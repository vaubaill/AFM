        !COMPILER-GENERATED INTERFACE MODULE: Tue May 29 09:28:11 2018
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE PMETH__genmod
          INTERFACE 
            SUBROUTINE PMETH(WM1,WM2,EA,R1,EB1,DD1,R2,EB2,DD2,ERR)
              REAL(KIND=8) :: WM1(6)
              REAL(KIND=8) :: WM2(6)
              REAL(KIND=8) :: EA(6)
              REAL(KIND=8) :: R1(6)
              REAL(KIND=8) :: EB1(5)
              REAL(KIND=8) :: DD1
              REAL(KIND=8) :: R2(6)
              REAL(KIND=8) :: EB2(5)
              REAL(KIND=8) :: DD2
              INTEGER(KIND=4) :: ERR
            END SUBROUTINE PMETH
          END INTERFACE 
        END MODULE PMETH__genmod
