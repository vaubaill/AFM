MODULE CHAROPS

 USE VARGLOBAL

 CONTAINS

!#+INIT#############################################################
!#
!#+FUNC ItoC
!#
!###################################################################
!#
!#+CALL itoc (x)
!#+CATE Utilitaire
!#+TITL Conversion des entiers en chaine de caractere
!#+AUTH (C) 2005, J. Berthier & F.Vachier - IMCCE
!#+PROJ Eproc
!#+VERS 4.0
!#
!# Derniere modification
!#+MODI 2002-10-06 : creation JBE
!#
!###################################################################
!#
!# Variables d'entree/sortie : 
!#
!#+VINP  x         IN     nombre entier a convertir
!#+VOUT  ItoC      CH(10) chaine correspondante
!#
!###################################################################

function ItoC (x)

   implicit none
   integer, intent(in)              :: x
   character (len=10)               :: ItoC

   character (len=1), dimension(10) :: Chiffre
   integer                          :: Ix, dd, cpt, i, j

   Chiffre = ' '
   if (x /= 0) then
      Ix = x
      dd = 1000000000
      cpt = 0
      do while (char(int(real(Ix,DP)/real(dd,DP))+48) == '0')
         cpt = cpt + 1
         dd = int(real(dd,DP)/10.d0)
      end do
      j = 1
      do i = cpt, 9
         Chiffre (j) = char(int(real(Ix,DP)/real(dd,DP))+48)
         j = j + 1
         Ix = Ix - dd*int(real(Ix,DP)/real(dd,DP))
         dd = int(real(dd,DP)/10.d0)
      end do
   else
      Chiffre(1) = '0'
      Chiffre(2:10) = ' '
   end if
   ItoC = Chiffre(1)//Chiffre(2)//Chiffre(3)//Chiffre(4)//Chiffre(5)// &
          Chiffre(6)//Chiffre(7)//Chiffre(8)//Chiffre(9)//Chiffre(10)

   return
end function ItoC
!#+INIT#############################################################
!#
!#+FUNC CtoI
!#
!###################################################################
!#
!#+CALL ctoi (chaine)
!#+CATE Utilitaire
!#+TITL Conversion d'une chaine de caracteres en entier
!#+AUTH (C) 2005, J. Berthier & F.Vachier - IMCCE
!#+PROJ Eproc
!#+VERS 4.0
!#
!# Derniere modification
!#+MODI 2002-10-06 : creation JBE
!#
!###################################################################
!#
!# Variables d'entree/sortie : 
!#
!#+VINP  chaine    CH(*) chaine a convertir
!#+VOUT  CtoI      IN    nombre entier correspondant 
!#
!###################################################################

function CtoI (chaine)

   implicit none
   character(len=*), intent(in) :: chaine
   integer                      :: CtoI

   read(chaine,*) CtoI

   return
end function CtoI
!#+INIT#############################################################
!#
!#+FUNC CtoR
!#
!###################################################################
!#
!#+CALL ctor (chaine)
!#+CATE Utilitaire
!#+TITL Conversion d'une chaine de caracteres en reel 8
!#+AUTH (C) 2005, J. Berthier & F.Vachier - IMCCE
!#+PROJ Eproc
!#+VERS 4.0
!#
!# Derniere modification
!#+MODI 2002-10-06 : creation JBE
!#
!###################################################################
!#
!# Variables d'entree/sortie : 
!#
!#+VINP  chaine    CH(*) chaine a convertir
!#+VOUT  CtoR      DP    nombre reel correspondant 
!#
!###################################################################

function CtoR (chaine)
 USE PRECISION

   implicit none
   character(len=*), intent(in)  :: chaine
   real(kind=DP)                 :: CtoR

   read(chaine,*) CtoR

   return
end function CtoR
!=== FIN ======================================================================!
END MODULE CHAROPS
