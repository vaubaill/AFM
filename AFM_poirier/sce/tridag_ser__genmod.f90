        !COMPILER-GENERATED INTERFACE MODULE: Tue Nov  3 10:34:23 2020
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE TRIDAG_SER__genmod
          INTERFACE 
            SUBROUTINE TRIDAG_SER(A,B,C,R,U)
              REAL(KIND=4), INTENT(IN) :: A(:)
              REAL(KIND=4), INTENT(IN) :: B(:)
              REAL(KIND=4), INTENT(IN) :: C(:)
              REAL(KIND=4), INTENT(IN) :: R(:)
              REAL(KIND=4), INTENT(OUT) :: U(:)
            END SUBROUTINE TRIDAG_SER
          END INTERFACE 
        END MODULE TRIDAG_SER__genmod
