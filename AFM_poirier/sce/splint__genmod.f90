        !COMPILER-GENERATED INTERFACE MODULE: Tue Nov  3 10:34:22 2020
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE SPLINT__genmod
          INTERFACE 
            SUBROUTINE SPLINT(INDEX,X,VAL_OUT)
              USE VARGLOBAL, ONLY :                                     &
     &          ATM_N,                                                  &
     &          SPLINE_DATA,                                            &
     &          SPLINE_DATA_PP
              INTEGER(KIND=4), INTENT(IN) :: INDEX
              REAL(KIND=4), INTENT(IN) :: X
              REAL(KIND=4), INTENT(OUT) :: VAL_OUT
            END SUBROUTINE SPLINT
          END INTERFACE 
        END MODULE SPLINT__genmod
