        !COMPILER-GENERATED INTERFACE MODULE: Tue May 29 09:28:11 2018
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE APPROACH__genmod
          INTERFACE 
            SUBROUTINE APPROACH(TMIN,TMAX,EA,WM1,WM2)
              REAL(KIND=8) :: TMIN
              REAL(KIND=8) :: TMAX
              REAL(KIND=8) :: EA(6)
              REAL(KIND=8) :: WM1(6)
              REAL(KIND=8) :: WM2(6)
            END SUBROUTINE APPROACH
          END INTERFACE 
        END MODULE APPROACH__genmod
