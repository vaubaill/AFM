SUBROUTINE tridag_ser(a,b,c,r,u)
  
  USE PRECISION
  USE nrutil, ONLY : assert_eq,nrerror
  
  IMPLICIT NONE
  
  REAL(SP), DIMENSION(:), INTENT(IN) :: a,b,c,r
  REAL(SP), DIMENSION(:), INTENT(OUT) :: u
  REAL(SP), DIMENSION(SIZE(b)) :: gam
  
  INTEGER(kind=DP) :: n,j
	
  REAL(SP) :: bet
	
  n=assert_eq((/SIZE(a)+1,SIZE(b),SIZE(c)+1,SIZE(r),SIZE(u)/),'tridag_ser')
  bet=b(1)
  
  IF (bet == 0.0) CALL nrerror('tridag_ser: Error at code stage 1')
	
  u(1)=r(1)/bet
  
  DO j=2,n
     gam(j)=c(j-1)/bet
     bet=b(j)-a(j-1)*gam(j)

     IF (bet == 0.0) CALL nrerror('tridag_ser: Error at code stage 2')

     u(j)=(r(j)-a(j-1)*u(j-1))/bet
  END DO

  DO j=n-1,1,-1
     u(j)=u(j)-gam(j+1)*u(j+1)
  END DO
	
END SUBROUTINE tridag_ser

RECURSIVE SUBROUTINE tridag_par(a,b,c,r,u)
  
  USE PRECISION
  USE nrutil, ONLY : assert_eq,nrerror
  USE nrutil, ONLY : assert_eq,nrerror
  USE nr, ONLY : tridag_ser
  
  IMPLICIT NONE
  
  REAL(SP), DIMENSION(:), INTENT(IN) :: a,b,c,r
  REAL(SP), DIMENSION(:), INTENT(OUT) :: u
  
  INTEGER(kind=DP), PARAMETER :: NPAR_TRIDAG=4
  INTEGER(kind=DP) :: n,n2,nm,nx
  
  REAL(SP), DIMENSION(SIZE(b)/2) :: yint,q,piva
  REAL(SP), DIMENSION(SIZE(b)/2-1) :: x,z
  REAL(SP), DIMENSION(SIZE(a)/2) :: pivc
  
  n=assert_eq((/SIZE(a)+1,SIZE(b),SIZE(c)+1,SIZE(r),SIZE(u)/),'tridag_par')
  
  IF (n < NPAR_TRIDAG) THEN
     CALL tridag_ser(a,b,c,r,u)
  ELSE
     IF (MAXVAL(ABS(b(1:n))) == 0.0) CALL nrerror('tridag_par: possible singular matrix')
     
     n2=SIZE(yint)
     nm=SIZE(pivc)
     nx=SIZE(x)
     piva = a(1:n-1:2)/b(1:n-1:2)
     pivc = c(2:n-1:2)/b(3:n:2)
     yint(1:nm) = b(2:n-1:2)-piva(1:nm)*c(1:n-2:2)-pivc*a(2:n-1:2)
     q(1:nm) = r(2:n-1:2)-piva(1:nm)*r(1:n-2:2)-pivc*r(3:n:2)
     
     IF (nm < n2) THEN
        yint(n2) = b(n)-piva(n2)*c(n-1)
        q(n2) = r(n)-piva(n2)*r(n-1)
     END IF
     
     x = -piva(2:n2)*a(2:n-2:2)
     z = -pivc(1:nx)*c(3:n-1:2)
     
     CALL tridag_par(x,yint,z,q,u(2:n:2))
     
     u(1) = (r(1)-c(1)*u(2))/b(1)
     u(3:n-1:2) = (r(3:n-1:2)-a(2:n-2:2)*u(2:n-2:2) &
          -c(3:n-1:2)*u(4:n:2))/b(3:n-1:2)
     IF (nm == n2) u(n)=(r(n)-a(n-1)*u(n-1))/b(n)
     
  END IF
  
END SUBROUTINE tridag_par
