!##############################################################################
 
!==============================================================================!
! Reads multiple data in a character
!                                                                              !
! Input : charin: character containing multiple chars, e.g: 1.0,2.0,3.0       !
!                                                                              !
! Output : arrout: array of numbers, e.g. (/ 1.0, 2.0, 3.0/)                   !
!                                                                              !
!==============================================================================!
! Librairie EPROC :                                                            !
!    Ephemerides, Predictions et Reductions pour les Corps Celestes.           !
! Conditions d'utilisation : eproc -licence                                    !
! Copyright (C) 2015, J. Vaubaillon, J. Berthier                               !
!==============================================================================!
 
!##############################################################################
MODULE MULTICHAR

 USE CHAROPS
 CONTAINS

subroutine multival(char,allval,n)

! use Eproc_Ftools, only : CtoR
 
 character (len=*), intent(in) :: char
 real(kind=DP), dimension(:), allocatable,intent(out) :: allval
 integer, intent(out) :: n
 character(len=len_trim(char))  :: charcopy
 integer :: poscoma,i
 integer, parameter :: maxnum=100

 if (.not. allocated (allval)) allocate(allval(maxnum))
 i=0
 charcopy=char
 do while(len_trim(charcopy) .ne. 0)
  poscoma = scan (charcopy, ',')                 ! position of the ',' character
  i=i+1
  if (poscoma .eq. 0) then
   allval(i) = ctor(charcopy)
   charcopy=''
  else
   allval(i) = ctor(charcopy(:poscoma-1))
   charcopy=charcopy(poscoma+1:)
  end if
 end do
 n=i

! return

end subroutine multival
END MODULE MULTICHAR
