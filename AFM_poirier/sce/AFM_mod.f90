 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE AFM_MOD

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                    DESCRIPTION OF THE A.F.M's MODEL                    ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE PARAM_FILE
 USE VARGLOBAL
 USE PARMODEL
 USE PHYSICS
 USE MATHS
 USE INTERPOLATION
 USE OUTIL_ATM
 USE lib_array

 CONTAINS

 !----------------------------------------------------------------------------!
 !--            Listing of subroutines available in this module!            --!
 !----------------------------------------------------------------------------!
 !--                                                                        --!
 !--  AFM_fra : management of the fragmentation phemonenon                  --!
 !--  AFM_abl : management of the ablation      phemonenon                  --!
 !--                                                                        --!
 !----------------------------------------------------------------------------!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##               MANAGEMENT OF THE FRAGMENTATION PHENOMENON               ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !** ROL : - computing of atmospheric pressure                              **!
 !**       - computing of tensile strength force                            **!
 !**       - application of a law of size distribution                      **!
 !**                                                                        **!
 !** INPUT :                                                                **!
 !**       - id_fra : particle id                                           **!
 !**       - yin  : state vector of a body before any fragmentation  ! m,v,h,o,l,X,Y[,T]       **!
 !**       - dyin : derivative of yin w.r.t time                            **!
!**                                                                         **!
 !** OUTPUT :                                                               **!
 !**       - nfra : number of residual fragments                            **!
 !**       - yfra : corresponding state table (for all fragments)           **!
 !**       - dyfra : derivative of yfra w.r.t time                          **!
 !**       - Ptsl : Tensile strength                                        **!
 !**       - Pdyn : Dynamic pressure                                        **!
 !**                                                                        **!
 !****************************************************************************!
 
 subroutine AFM_fragmentation(id_fra,yin,dyin,nfra,yfra,dyfra,Ptsl,Pdyn)

 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O                                            ! 1 2 3 4 5 6 7  8
 integer(kind=DP)                        , intent(IN)  :: id_fra
 real(kind=DP), dimension(nbeqn,1)       , intent(IN)  ::  yin ! m,v,h,o,l,X,Y[,T]
 real(kind=DP), dimension(nbeqn,1)       , intent(IN)  :: dyin ! dm/dt,dv/dt,dh/dt,do/dt,dl/dt,dX/dt,dY/dt[,dT/dt]
 integer(kind=DP)                        , intent(OUT) :: nfra
 real(kind=DP), dimension(nbeqn,maxdim), intent(OUT)   :: yfra
 real(kind=DP), dimension(nbeqn,maxdim), intent(OUT)   :: dyfra
 real(kind=DP)                         , intent(OUT)   :: Ptsl, Pdyn
 !DVA Variables
 !* state vector
 real(kind=DP)    :: mm,vm,hm,Om,Xm,Ym
 real(kind=DP)    :: rm,rstop 
 !* iteration
 integer(kind=SP) :: i,j,k
 !* 
 real(kind=DP) :: Cd
 real(kind=DP) :: x
 real(kind=DP), dimension(:), allocatable :: mass
 real(kind=DP) :: logmm,logP
 !*
 logical :: Lfra,bounds_error
 real(kind=DP) :: fill_value
 
 !*
 !-------------------------------- BEGINNING! --------------------------------!
 mm = yin(1,1) ! [kg]
 vm = yin(2,1) ! [m/s]
 hm = yin(3,1) ! [m]
 Om = yin(4,1) ! [rad]
 Xm = yin(5,1) ! [m]
 Ym = yin(6,1) ! [m]
 
 ! by default there is no fragmentation
 Lfra = .false.
 
 !* atmospheric pressure
 !- density [kg/m3]
 call atm_planet(hm,da,Pa,Ta)

 !* drag coefficient (spherical body) [dimensionless]
 Cd = 2.0_dp

 !* dynamic pressure [N/m2]
 Pdyn = (Cd/2._dp) * da * vm**2

 !* compute tensile strength
 select case(StrengthMod)
 case('user','Popova2011','Ferrier2012')
  Ptsl = tsl_str%Pref * (tsl_str%Mref/mm) ** tsl_str%coef
  if (Pdyn .ge. Ptsl) Lfra = .true.
 case('interp')
 ! computes the coefficients providing the tensile strength, from spline in logarithm space
  logmm=log10(mm)
  call    spline_custom(      tsl_str_intp%Mref(1:tsl_str_intp%n),tsl_str_intp%Pref(1:tsl_str_intp%n),tsl_str_intp%coef(1:tsl_str_intp%n,1),tsl_str_intp%coef(1:tsl_str_intp%n,2),tsl_str_intp%coef(1:tsl_str_intp%n,3) ,tsl_str_intp%n)
  logP = ispline(logmm,tsl_str_intp%Mref(1:tsl_str_intp%n),tsl_str_intp%Pref(1:tsl_str_intp%n),tsl_str_intp%coef(1:tsl_str_intp%n,1),tsl_str_intp%coef(1:tsl_str_intp%n,2),tsl_str_intp%coef(1:tsl_str_intp%n,3) ,tsl_str_intp%n)
  Ptsl = 10.0_dp**(logP)
  if (Pdyn .ge. Ptsl) Lfra = .true.
 case('manual')
  ! computes from tabulated values without spline
  bounds_error = .false.
  fill_value = tsl_str_intp%Pref(tsl_str_intp%n)
  logP = interp1d(tsl_str_intp%Mref(1:tsl_str_intp%n),tsl_str_intp%Pref(1:tsl_str_intp%n),log10(mm),bounds_error,fill_value) 
  Ptsl = 10.0_dp**(logP)
  if (Pdyn .ge. Ptsl) Lfra = .true.
 case('ObsAlt')
  Ptsl = tsl_str%Pref * (tsl_str%Mref/mm) ** tsl_str%coef
  ! if main fragment
  if (id_fra .eq. 1) then
   ! if altitude if less than observe altitude
   if (hm .lt. tsl_str_oalt%ObsAlt(tsl_str_oalt%i)) then
    Ptsl = Pdyn
    Lfra = .true.
    tsl_str_oalt%i = tsl_str_oalt%i+1
    if (tsl_str_oalt%i .gt. tsl_str_oalt%n) tsl_str_oalt%i = tsl_str_oalt%n
   else
    Lfra = .false.
   end if
  ! not main fragmant
  else
   if (Pdyn .ge. Ptsl) Lfra = .true.
   !Lfra = .false.
  end if
 case default
  if (mm<mstop) then
   print*,'tsl_str%Pref=',tsl_str%Pref
   print*,'tsl_str%Mref=',tsl_str%Mref
   print*,'mm=',mm
   print*,'tsl_str%coef=',tsl_str%coef
  end if
  Ptsl = tsl_str%Pref * (tsl_str%Mref/mm) ** tsl_str%coef
 end select
 
 
 !----------------------- check for breakup ----------
 !- Fragmentation occurs
 if (Lfra) then
   
   !- Number of fragments
   call fra_nbmet(id_fra,nfra)
   
   !- Mass/Radius of each fragment
   allocate(mass(1:nfra))
   call fra_distrib(nfra,mstop,mm,mass)

   !- ejection velocity of each fragment
   !   allocate(vej(1:nfra))
   !   call fra_vej(nfra,mass,vej)
   !- save the state vector
   do i = 1,nfra
     do j = 1,nbeqn
       select case(j)
       ! set mass and mass loss rate
       case(1)
         yfra(j,i) = mass(i)
        dyfra(j,i) = dyin(j,1) ! note: this quantity=dm/dt will be reevaluated at the entry of the integration loop, so it's ok if it doesn't match the mass
      ! ADD HERE TANGENTIAL EJECTION PROCESS
      ! case : add dX/dt, dY/dt from vej
       case default
         yfra(j,i) = yin(j,1)
        dyfra(j,i) = yin(j,1)
       end select
     end do
   end do
   deallocate(mass)
   !print'("Fragmentation: Parent=",i6," Nfra=",i6," Ptsl=",e11.3," Pdyn=",e11.3," mm=",e11.3," hm=",e11.3)',nBODY%icur,nfra,Ptsl,Pdyn,mm,hm 
   if (id_fra .eq. 1) print'("Fragmentation: Parent=",i6," Nfra=",i6," Ptsl=",e11.3," Pdyn=",e11.3," mm=",e11.3," hm=",e11.3)',id_fra,nfra,Ptsl,Pdyn,mm,hm 
 else
   !- No fragmentation
   nfra = 1
   yfra(1:nbeqn,1) = yin(1:nbeqn,1)
  dyfra(1:nbeqn,1) =dyin(1:nbeqn,1)
 end if
 end subroutine AFM_fragmentation

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                 DISTRIBUTION OF FRAGMENTS (Normal law)                 ##!
 !##                                                                        ##!
 !##  input: nfra: number of fragments                                      ##!
 !##         mstop: minimal mass to consider                                ##!
 !##         m0: total mass of framgments                                   ##!
 !##  output: mass: mass of each fragments, such as sum(mass)=m0            ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                   
 ! INPUT:
 !      nfra : number of fragments
 !      mstop : 
 subroutine fra_distrib(nfra,mstop,m0,mass)
 
 USE lib_array, ONLY : quicksort
 
 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O 
 integer(kind=DP), intent( IN) :: nfra
 real(kind=DP)   , intent( IN) :: mstop,m0
 real(kind=DP), dimension(1:nfra), intent(OUT) :: mass
 !DVA Variables
 !* random number
 real(kind=DP) :: y,alpha
 !* iteration
 integer(kind=SP) :: i,count
 !*
 integer, dimension(1:nfra) :: idxmass
 real(kind=DP) :: mtmp 
 real(kind=DP) :: tmp,tmp2 ,p,sumallM
 real(kind=DP),dimension(nfra-1) :: allP
 logical :: allgood

 !-------------------------------- BEGINNING! --------------------------------!


 allgood=.false.
 ! initialize random seed
 call system_clock (count)
 ! call random_seed(count)
 call random_seed()
 do while (.not. allgood) 
  mtmp = m0
  sumallM=0.0_dp
   ! generates random number for each fragment
  do i = 1,nfra-1
    call random_number(allP(i)) ! [0,1]
    allP(i)=allP(i)*0.9         ! [0,0.9] to avoid having negative mass
  end do
  
  allgood=.true.
  do i = 1,nfra
   if (i .eq. nfra) then
      mass(i) = m0-sumallM
   else
      mass(i) = -(mtmp/real(nfra,kind=DP)) * (log(1.0_dp-allP(i)))
      sumallM = sumallM+mass(i)
   end if
   
   ! double-check that the mass are coherent
   if (mass(i) .le. 0.0_sp) allgood=.false.
   if (mass(i) .gt. m0)     allgood=.false.
  end do
 end do ! end of do while (.not. allgood)
 
 ! sort mass
 call quicksort(mass)
 ! swap first and last mass
 tmp=mass(1)
 mass(1) = mass(nfra)
 mass(nfra) = tmp
 
 print*,'all fragments mass: ',mass
 
 
 end subroutine fra_distrib

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                   NUMBER OF FRAGMENTS (Geometric law)                  ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 ! INPUT:
 !      None
 ! OUTPUT:
 !       nfra : number of fragments
 ! 
 subroutine fra_nbmet(id_fra,nfra)

 USE VARGLOBAL

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 integer(kind=DP), intent(IN) :: id_fra
 integer(kind=DP), intent(OUT) :: nfra 
 !DVA Variables
 real(kind=DP) :: x, coef_x
 integer(kind=DP) :: count

 !-------------------------------- BEGINNING! --------------------------------! 
  if((id_fra .eq. 1) .and. (StrengthMod=='ObsAlt')) then
    nfra = tsl_str_oalt%ObsNum(tsl_str_oalt%k)
    tsl_str_oalt%k = tsl_str_oalt%k+1
    if (tsl_str_oalt%k .gt. tsl_str_oalt%n) tsl_str_oalt%k = tsl_str_oalt%n
  else
   ! initialize random seed
   call system_clock(count)
   !call random_seed(count)
   call random_seed()
   call random_number(x) ! 0<= x <  1
   x = 1._dp-x           ! 0<  x <= 1
   ! allow for a distribution or not
   if (inum_fra) then
    call random_number(coef_x)
    coef_x = 0.1_dp+coef_x*0.9_dp ! [0.1 ; 1.0]
   else
    coef_x=1.0_dp
   end if
   nfra = int(log(x)/log(coef_x*num_fra),kind=DP) + 2_sp
   print*,'id_fra=',id_fra,' nfra=',nfra
 end if

 end subroutine fra_nbmet

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                  MANAGEMENT OF THE ABLATION PHENOMENON                 ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine AFM_abl(y,yp,atm)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 real(kind=DP), dimension(1:nbeqn), intent(INOUT) :: y ! m,v,h,o,l,X,Y[,T]
 real(kind=DP), dimension(1:nbeqn), intent(OUT) :: yp   ! dm/dt,dv/dt,dh/dt,do/dt,dl/dt,dX/dt,dY/dt[,dT/dt]
 real(kind=DP), dimension(1:3), intent(OUT) :: atm

 !-------------------------------- BEGINNING! --------------------------------!

 select case(imodel)
 case(1) ! Auliffe
   call AFM_abl_Auliffe(y,yp,atm)
 case(2) ! M.D. Campbell-Brown
   call AFM_abl_Campbell(y,yp,atm)
 case(3) ! Borovicka
   call AFM_abl_Borovicka(y,yp,atm)
 case(4) ! Borovicka modified by Vaubaillon
   call AFM_abl_BoroVaub(y,yp,atm)
 case(5) ! M.D. Campbell-Brown modified by Coquerelle
   call AFM_abl_Campbell_VC(y,yp,atm)
 case(6) ! Auliffe modified by Coquerelle
   call AFM_abl_Auliffe_VC(y,yp,atm) 
 case(7) ! Borovicka modified by Vaubaillon edited by Coquerelle
   call AFM_abl_BoroVaub_VC(y,yp,atm)
 end select

 end subroutine AFM_abl
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                 MANAGEMENT OF THE ABLATION PHENOMENON                  ##!
 !##                           McAULIFFE (2012)                             ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !** ROL : - building of differential system y' = f(t,y)   (ablation)       **!
 !**                                                                        **!
 !** VAR :  - y                                                             **!
 !**        - yp : corresponding to y'                                      **!
 !****************************************************************************!
 
 subroutine AFM_abl_Auliffe(y,yp,atm)

 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O                              ! 1 2 3 4 5 6 7 8 
 real(kind=DP), dimension(1:9), intent( IN) :: y ! m,v,h,o,l,X,Y,T
 real(kind=DP), dimension(1:9), intent(OUT) :: yp
 real(kind=DP), dimension(1:3), intent(OUT) :: atm
 !DVA Variables 
 real(kind=DP) :: x,l,s
 !*
 real(kind=DP) :: mm,vm,hm,Om,lm,Ts
 !* provisoire
 real(kind=DP) :: Cd, Ch
 real(kind=DP) :: Ac, Acs
 real(kind=DP) :: Q
 real(kind=DP) :: diso,miso
 real(kind=DP) :: t
 !* 
 real(kind=DP) :: acc_g,cSOL
 !*
 real(kind=DP) :: Lref

 !-------------------------------- BEGINNING! --------------------------------!

 mm = y(1) ! [kg]
 vm = y(2) ! [m/s]
 hm = y(3) ! [m]
 Om = y(4) ! [rad]
 lm = y(5) ! [m]
 Ts = y(8) ! [K]
 !if (Ts .lt. 0._dp) STOP 'Ts < 0'

 !* Global mass --> Shell mass ; note: dm=meteoroid bulk density
 ro = ( mm/((4._dp/3._dp)*PI*dm) )**(1._dp/3._dp)
 !* inner radius meteoroid  [m] ! Tc: Thermal conductivity
 diso = 0.3_dp * Tc / (cSB * Ts**3)
 if (ro .gt. diso) then
   ri = ro - diso
 else
   ri = 0._dp
 end if
 ! note: dm=meteoroid bulk density
 miso = (4._dp/3._dp)*PI*dm*(ro**3-ri**3)

 !* atmospheric density, pressure and temperature at altitude hm
 !- Pa [N/m2]
 !- Ta [K]
 !- da [kg/m3]
 call atm_planet(hm,da,Pa,Ta)

 atm(1) = da
 atm(2) = Pa
 atm(3) = Ta

 !* Gravitational acceleration and Solar constant
 call param_physics(acc_g,cSOL)

 !* Heat of ablation
 if (Ts .lt. Tmelt) then
   Lref = Linf
 else
   Lref = Lsup
 end if
 
 !* thermal speed of the atmospheric molecules [m/s]
 vth = 2._dp*kB*Ts/mu
 if (vth .lt. 0.0) then
    print*,'ERREUR : une racine carree ne peut pas etre negative !'
    stop
 end if
 vth = sqrt(2._dp*kB*Ts/mu)
 
 
 !* cross-sectional area of the body [m2] 
 Ac  = PI * ro**2  

 !* Surface area of the meteoroid [m2]  
 Acs = 4._dp * Ac

 !* drag coefficient (spherical body) [dimensionless]  
 Cd = 2.0_dp

 !* vapour pressure [N/m2]
 Pvap = 10._dp**(A-B/Ts) !  -1._dp) 

 !* Heat of ablation
 if (Ts .lt. Tmelt) then
   Lref = Linf
 else
   Lref = Lsup
 end if

 ! 1 2 3 4 5 6 7 8 
 ! m,v,h,o,l,X,Y,T
 
 !* dm/dt [kg/s]
 yp(1) = -Acs * Pvap * sqrt(mu/(2._dp*PI*kB*Ts))
 if (yp(1) .gt. 0.0_dp) then
  print*,'dm/dt>0'
  stop
 end if
 
 !* dvm/dt [m/s2] 
 yp(2) = -(3._dp*Cd/8._dp) * (da/dm) * (vm**2)/ro + acc_g*cos(Om) 
 
 !* dhm/dt [m/s] 
 yp(3) = -vm * cos(Om)

 !* dOm/dt [rad/s] 
 yp(4) = -acc_g*sin(Om)/vm

 !* dlm/dt [m/s]
 yp(5) = vm * sin(Om)

 !* dX/dt = dlm/dt * sin(Om)
 yp(6) = yp(5) * sin(Om)

 !* dY/dt
 yp(7) = yp(7)

 !* dTs/dt [K/s] 
 s = vm/vth 
 x = 7._dp/5._dp
 !- accomodation coefficient
 l = 1._dp - 1._dp/(2._dp*s**2) * (x+1._dp)/(x-1._dp) * Ts/Ta 

 Esol = cSOL * Ac * (1._dp-am/100._dp) ! [W] solar irradiation
 Erad = Acs * cSB * (Ts**4 - Ta**4)    ! [W] radiative heat losses
 Eimp = (l/2._dp) * Ac * da * vm**3    ! [W] impact of atmospheric molecules
 Evap = -Lref * yp(1)                  ! [W] evaporation process
 yp(8) = (Eimp+Esol-Erad-Evap) / (miso*cm) ! dT/dt

 end subroutine AFM_abl_Auliffe
 
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                 MANAGEMENT OF THE ABLATION PHENOMENON                  ##!
 !##                           McAULIFFE (2012)                             ##!
 !##                       MODIFIED BY COQUERELLE                           ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !** ROL : - building of differential system y' = f(t,y)   (ablation)       **!
 !**                                                                        **!
 !** VAR :  - y                                                             **!
 !**        - yp : corresponding to y'                                      **!
 !****************************************************************************!
 
 subroutine AFM_abl_Auliffe_VC(y,yp,atm)

 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O                              ! 1 2 3 4 5 6 7 8   9
 real(kind=DP), dimension(1:10), intent( IN) :: y ! m,v,h,o,l,X,Y,Ts Tm
 real(kind=DP), dimension(1:10), intent(OUT) :: yp
 real(kind=DP), dimension(1:3), intent(OUT) :: atm
 !DVA Variables 
 real(kind=DP) :: x,l,s
 !*
 real(kind=DP) :: mm,vm,hm,Om,lm,Ts,Tm,Im
 !* provisoire
 real(kind=DP) :: Cd, Ch
 real(kind=DP) :: Ac, Acs
 real(kind=DP) :: Q
 real(kind=DP) :: diso,miso
 real(kind=DP) :: t
 !* 
 real(kind=DP) :: acc_g,cSOL
 !*
 real(kind=DP) :: Lref
 !*
 real(kind=Dp) :: tau, M


 !-------------------------------- BEGINNING! --------------------------------!
 
 mm = y(1) ! [kg]
 vm = y(2) ! [m/s]
 hm = y(3) ! [m]
 Om = y(4) ! [rad]
 lm = y(5) ! [m]
 Ts = y(8) ! [K]
 Tm = y(9) ! [K]
 Im = y(10) ! [W}
  
 !* Global mass --> Shell mass
 ro = ( mm/((4._dp/3._dp)*PI*dm) )**(1._dp/3._dp)
 !* inner radius meteoroid  [m] 
 diso = 0.3_dp * Tc / (cSB * Ts**3)
 if (ro .gt. diso) then
   ri = ro - diso
 else
   ri = 0._dp
 end if
 miso = (4._dp/3._dp)*PI*dm*(ro**3-ri**3)

 !* atmospheric density, pressure and temperature at altitude hm
 !- Pa [N/m2]
 !- Ta [K]
 !- da [kg/m3]
 call atm_planet(hm,da,Pa,Ta)

 atm(1) = da
 atm(2) = Pa
 atm(3) = Ta

 !* Gravitational acceleration and Solar constant
 call param_physics(acc_g,cSOL)

 !* thermal speed of the atmospheric molecules [m/s]
 vth = 2._dp*kB*Ts/mu
 if (vth .lt. 0.0) then
    print*,'ERREUR : une racine carree ne peut pas etre negative !'
    stop
 end if
 vth = sqrt(2._dp*kB*Ts/mu)
  
 !* cross-sectional area of the body [m2] 
 Ac  = PI * ro**2  

 !* Surface area of the meteoroid [m2]  
 Acs = 4._dp * Ac

 !* drag coefficient (spherical body) [dimensionless]  
 Cd = 2.0_dp

 !* vapour pressure [N/m2]
 if (Tm .ge. Tmelt) Tm=Tmelt
 Pvap = 10._dp**(A-B/Tm)
 
 !* Heat of ablation
 if (Ts .lt. Tmelt) then
   Lref = Linf
 else
   Lref = Lsup
 end if

 ! 1 2 3 4 5 6 7 8 
 ! m,v,h,o,l,X,Y,T
 
 !* dm/dt [kg/s]
 yp(1) = -Acs * Pvap * sqrt(mu/(2._dp*PI*kB*Tm))
  if (yp(1) .gt. 0.0_dp) then
  print*,'ERREUR : la masse augmente !'
  stop
 end if
 
 !* dvm/dt [m/s2] 
 yp(2) = -(3._dp*Cd/8._dp) * (da/dm) * (vm**2)/ro + acc_g*cos(Om) 
 
 !* dhm/dt [m/s] 
 yp(3) = -vm * cos(Om)

 !* dOm/dt [rad/s] 
 yp(4) = -acc_g*sin(Om)/vm

 !* dlm/dt [m/s]
 yp(5) = vm * sin(Om)
 
 !* dX/dt = dl sin(Om)
 yp(6) = yp(5) * sin(Om)

 !* dY/dt
 yp(7) = yp(7)

 !* dTs/dt [K/s] 
 s = vm/vth 
 x = 7._dp/5._dp
 !- accomodation coefficient
 if (s .lt. 15._dp) then
   s = 15._dp
   l = 1._dp - 1._dp/(2._dp*s**2) * (x+1._dp)/(x-1._dp) * Ts/Ta 
 else
   l = 1._dp - 1._dp/(2._dp*s**2) * (x+1._dp)/(x-1._dp) * Ts/Ta 
 end if

 Esol = cSOL * Ac * (1._dp-am/100._dp) ! [W] solar irradiation
 Erad = Acs * cSB * (Ts**4 - Ta**4)    ! [W] radiative heat losses
 Eimp = (l/2._dp) * Ac * da * vm**3    ! [W] impact of atmospheric molecules ATTENTION.
 Evap = Lref * yp(1)                  ! [W] evaporation process
 yp(8)= (Eimp+Esol-Erad-Evap) / (miso*cm)
 if (Ts .ge. Tmelt) then
   Ts = Tmelt
   yp(8) = 0.0d0
 end if
 yp(9)= (Eimp+Esol-Acs * csB * (Tm**4 - Ta**4)-Evap) / (miso*cm)
 if (Tm .ge. Tmelt) then
   Tm = Tmelt
   yp(9) = 0.0d0
 end if
 
 !* Intensite lumineuse
 tau = 2._dp * 10._dp**(-3)
 Im = -tau * yp(1) * (vm**2)/2._dp
  
 M = 24.3_dp - 2.5_dp * log10(Im * 10._dp**7) ! La magnitude 
       
 end subroutine AFM_abl_Auliffe_VC

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                 MANAGEMENT OF THE ABLATION PHENOMENON                  ##!
 !##                            BOROVICKA (2012)                            ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine AFM_abl_Borovicka(y,yp,atm)
 
 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O                               ! 1 2 3 4 5 6 7
 real(kind=DP), dimension(1:7), intent( IN) :: y  ! m,v,h,o,l,X,Y
 real(kind=DP), dimension(1:7), intent(OUT) :: yp ! dm/dt,dv/dt,dh/dt,do/dt,dl/dt,dX/dt,dY/dt
 real(kind=DP), dimension(1:3), intent(OUT) :: atm
 !DVA Variables 
 real(kind=DP) :: x,l,s
 !*
 real(kind=DP) :: mm,vm,hm,Om,lm,Xm,Ym,Ts
 !* provisoire
 real(kind=DP) :: Cd, Ch
 real(kind=DP) :: Ac, Acs
 real(kind=DP) :: Q
 real(kind=DP) :: diso
 real(kind=DP) :: t
 !* 
 real(kind=DP) :: acc_g,cSOL
 !*
 real(kind=DP) :: Qheat, Gamma, Lambda 

 !-------------------------------- BEGINNING! --------------------------------!
 mm = y(1) !* [kg]
 vm = y(2) !* [m/s]
 hm = y(3) !* [m]
 Om = y(4) !* [rad]
 lm = y(5) !* [m]
 Xm = y(6) !* [m]
 Ym = y(7) !* [m]

 !* atmospheric density, pressure and temperature at altitude hm
 !- Pa [N/m2]
 !- Ta [K]
 !- da [kg/m3]
 call atm_planet(hm,da,Pa,Ta)

 atm(1) = da
 atm(2) = Pa
 atm(3) = Ta

 !* Gravitational acceleration and Solar constant
 call param_physics(acc_g,cSOL)

 !*  outer radius meteoroid [m]
 ro = (mm/((4._dp/3._dp)*PI*dm)) ** (1._dp/3._dp)

 !* cross-sectional area of the body [m2] 
 Ac  = PI * ro**2  

 !* Surface area of the meteoroid [m2]  
 Acs = 4._dp * Ac

 !* drag coefficient (spherical body) [dimensionless]  
 Cd = 2.0_dp

 Gamma = 1.0_dp
 Lambda = 1.0_dp
 Qheat = Q_Boro  ! [J/kg] ; Borovicka et al 2007: Q=[2.5;6]x1.0E+06 [J/kg]

 ! 1 2 3 4 5 6 7 8 
 ! m,v,h,o,l,X,Y,T

 !* dm/dt [kg/s]
 yp(1) = -(Lambda * Ac * da * vm**3) / (2._dp*Qheat)

 !* dvm/dt [m/s2] 
 yp(2) = -(3._dp*Cd/8._dp) * (da/dm) * (vm**2)/ro + acc_g*cos(Om)

 !* dhm/dt [m/s] 
 yp(3) = -vm * cos(Om)

 !* dOm/dt [rad/s]
 yp(4) = -acc_g * sin(Om) / vm

 !* dlm/dt [m/s]
 yp(5) = vm * sin(Om)

 !* dX/dt = dl sin(Om)
 yp(6) = yp(5) * sin(Om)

 !* dY/dt
 yp(7) = yp(7)

!write(*,*)"y = ",y
!write(*,*)"dy = ",yp
!write(*,*)"da = ",da

!stop

 end subroutine AFM_abl_Borovicka


 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                 MANAGEMENT OF THE ABLATION PHENOMENON                  ##!
 !##                 BOROVICKA (2012) MODIFIED BY VAUBAILLON 2014           ##!
 !##                                                                        ##!
 !## takes into account the thermal heating of the particle                 ##!
 !## in order to comply with CNES demands, and in order to reproduce        ##!
 !## the number of 10^0-10^2 kg fragments for e.g Chelyabinsk (i.e.=0)      ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine AFM_abl_BoroVaub(y,yp,atm)
 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O 
 real(kind=DP), dimension(1:8), intent(INOUT) :: y ! m,v,h,o,l,X,Y
 real(kind=DP), dimension(1:8), intent(OUT) :: yp
 real(kind=DP), dimension(1:3), intent(OUT) :: atm
 !DVA Variables 
 real(kind=DP) :: x,l,s
 !*
 real(kind=DP) :: mm,vm,hm,Om,lm,Xm,Ym,Ts
 !* provisoire
 real(kind=DP) :: Cd, Ch
 real(kind=DP) :: Ac, Acs
 real(kind=DP) :: Q
 real(kind=DP) :: diso
 real(kind=DP) :: t
 real(kind=DP) :: Q_therm, rho_sl, Vc, coef_therm
 !* 
 real(kind=DP) :: acc_g,cSOL
 !*
 real(kind=DP) :: Qheat, Gamma , Lambda
 
 integer :: k
 
 !-------------------------------- BEGINNING! --------------------------------!

 mm = y(1) !* [kg]
 vm = y(2) !* [m/s]
 hm = y(3) !* [m]
 Om = y(4) !* [rad]
 lm = y(5) !* [m]
 Xm = y(6) !* [m]
 Ym = y(7) !* [m]


 !* atmospheric density, pressure and temperature at altitude hm
 !- Pa [N/m2]
 !- Ta [K]
 !- da [kg/m3]
 call atm_planet(hm,da,Pa,Ta)

 atm(1) = da
 atm(2) = Pa
 atm(3) = Ta

 !* Gravitational acceleration and Solar constant
 call param_physics(acc_g,cSOL)

 !*  outer radius meteoroid [m]
 ro = (mm/((4._dp/3._dp)*PI*dm)) ** (1._dp/3._dp)

 !* cross-sectional area of the body [m2] 
 Ac  = PI * ro**2  

 !* Surface area of the meteoroid [m2]  
 Acs = 4._dp * Ac

 !* drag coefficient (spherical body) [dimensionless]  
 Cd = 2.0_dp

 Gamma = 1.0_dp
 Lambda = 1.0_dp
 ! thermal flux computed with: Detra, Kemp et Riddel
 rho_sl = 1.225  ! [kg/m^3]
 Vc     =7802.88 ! [m/s]
 Q_therm=11028.5E+04 / sqrt(ro) * sqrt(da / rho_sl) * (vm / Vc)**3.15_dp
 coef_therm=0.9_dp
 
 Qheat = 2.0E+10_dp ! [J/kg]
 !* dm/dt [kg/s]
 yp(1) = -(Lambda * Ac * da * vm**3) / (2._dp*Qheat) - coef_therm*Q_therm/Qheat

 !* dvm/dt [m/s2] 
 yp(2) = -(3._dp*Cd/8._dp) * (da/dm) * (vm**2)/ro + acc_g*cos(Om)

 !* dhm/dt [m/s] 
 yp(3) = -vm * cos(Om)

 !* dOm/dt [rad/s]
 yp(4) = -acc_g * sin(Om) / vm

 !* dlm/dt [m/s]
 yp(5) = vm * sin(Om)

 !* dX/dt = dl sin(Om)
 yp(6) = yp(5) * sin(Om)

 !* dY/dt
 yp(7) = yp(7)


 if (mm .le. mstop) then
  y(1)=0.9_dp*mstop
  yp(1:nbeqn)=-1.0E-10
 endif

 !* Test for NaN (Not a Number)
 do k = 1,nbeqn
   if (isnan(y(k))) then
     print*,'FATAL ERROR: y is NaN !!!'
     print*,'y=',y(1:nbeqn)
     print*,'yp=',yp(1:nbeqn)
     stop
   end if
 end do
 do k = 1,nbeqn
   if (isnan(yp(k))) then
     print*,'FATAL ERROR: yp is NaN !!!'
     print*,'y=',y
     print*,'yp=',yp
     print*,'Q_therm=',Q_therm
     print*,'Q_therm/Qheat=',Q_therm/Qheat
     print*,'Boro=',-(gamma * Ac * da * vm**3) / (2._dp*Qheat)
     stop
   end if
 end do

 end subroutine AFM_abl_BoroVaub


 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                 MANAGEMENT OF THE ABLATION PHENOMENON                  ##!
 !##                       M.D. CAMPBELL-BROWN (2004)                       ##!
 !##     				                                                    ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine AFM_abl_Campbell(y,yp,atm)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O 
 real(kind=DP), dimension(1:9), intent( IN) :: y ! m,v,h,o,l,X,Y,T
 real(kind=DP), dimension(1:9), intent(OUT) :: yp !=dy/dt
 real(kind=DP), dimension(1:3), intent(OUT) :: atm
 !DVA Variables 
 real(kind=DP) :: x,l,s
 !*
 real(kind=DP) :: t,mm,vm,hm,Om,lm,Xm,Ym,Tm
 !* provisoire
 real(kind=DP) :: Cd, Ch
 real(kind=DP) :: Ac, Acs  
 real(kind=DP) :: ps,pv,Pspall
 !* 
 real(kind=DP) :: acc_g,cSOL
 !*
 real(kind=DP) :: Lref,denom

 !-------------------------------- BEGINNING! --------------------------------!

 mm = y(1) ! [kg]
 vm = y(2) ! [m/s]
 hm = y(3) ! [m]
 Om = y(4) ! [rad]
 lm = y(5) ! [m]
 Xm = y(6) ! [m]
 Ym = y(7) ! [m]
 Tm = y(8) ! [K]

 !* atmospheric density, pressure and temperature at altitude hm
 !- Pa [N/m2]
 !- Ta [K]
 !- da [kg/m3]
 call atm_planet(hm,da,Pa,Ta)

 atm(1) = da
 atm(2) = Pa
 atm(3) = Ta

 !* Gravitational acceleration and Solar constant
 call param_physics(acc_g,cSOL)

 !*  outer radius meteoroid [m]
 ro = (mm/((4._dp/3._dp)*PI*dm)) ** (1._dp/3._dp)

 !* cross-sectional area of the body [m2] 
 Ac  = PI * ro**2  

 !* Surface area of the meteoroid [m2]  
 Acs = 4._dp * Ac

 !* drag coefficient (spherical body) [dimensionless]  
 Cd = 2.0_dp

 !* Heat of ablation
 if (Tm .lt. Tmelt) then
   Lref = Linf
 else
   Lref = Lsup
   Tm=Tmelt
 end if
 
 ! 1 2 3 4 5 6 7 8
 ! m,v,h,o,l,X,Y,T
 
 !* dm/dt [kg/s]
 ps = Pa0 * exp((Lref*mu)/(kB*Tmelt)) * exp(-(Lref*mu)/(kB*Tm))
 pv = 0._dp
 denom = sqrt((2._dp*PI*kB*Tm)/mu)
 Pspall = atan(0.04_dp * (Tm-Tmelt) / PI) + 0.5_dp 
 if (Pspall .gt. 1.0_dp) then
   print*,'***FATAL ERROR: Pspall .gt. 1'
   print*,'Tmelt=',Tmelt,'  Tm=',Tm,'  dTm/dt=',yp(9)
   stop
 end if
 yp(1) = -Acs * psi * (ps-pv) / denom * Pspall
 if (yp(1) .gt. 0.0_dp) yp(1)=0.0d0

 !* dvm/dt [m/s2]
 yp(2) = -Ac * (Cd/2._dp) * (da * vm**2/mm) * (mm/dm)**(2._dp/3._dp) + acc_g*cos(Om)

 !* dhm/dt [m/s] 
 yp(3) = -vm * cos(Om)

 !* dOm/dt [rad/s]
 yp(4) = acc_g * sin(Om) / vm

 !* dlm/dt [m/s]
 yp(5) = vm * sin(Om)

 !* dX/dt = dl sin(Om)
 yp(6) = yp(5) * sin(Om)

 !* dY/dt
 yp(7) = yp(7)

 !* dTs/dt [K/s] 
 Erad = Acs * cSB * (Tm**4 - Ta**4) ! note: in Campbell-Brown & Koschny, the term A(m/rho_m)^2/3 is the cross sectional area, ie Acs here
 Eimp = (1._dp/2._dp) * Ac * da * vm**3 
 Evap = -Lref * yp(2)
 yp(8) = (Eimp-Erad-Evap) / (cm*mm)
 if (Tm .ge. Tmelt) then
  Tm=Tmelt
  yp(8) = 0.0d0
 end if

 end subroutine AFM_abl_Campbell

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                 MANAGEMENT OF THE ABLATION PHENOMENON                  ##!
 !##                       M.D. CAMPBELL-BROWN (2004)                       ##!
 !##                         MODIFIED BY COQUERELLE                         ##!
 !##     				                                                    ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine AFM_abl_Campbell_VC(y,yp,atm)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O 
 real(kind=DP), dimension(1:9), intent( IN) :: y ! m,v,h,o,l,X,Y,T
 real(kind=DP), dimension(1:9), intent(OUT) :: yp !=dy/dt
 real(kind=DP), dimension(1:3), intent(OUT) :: atm
 !DVA Variables 
 real(kind=DP) :: x,l,s
 !*
 real(kind=DP) :: t,mm,vm,hm,Om,lm,Xm,Ym,Tm,Im
 !* provisoire
 real(kind=DP) :: Cd, Ch
 real(kind=DP) :: Ac, Acs  
 real(kind=DP) :: ps,pv,Pspall
 !* 
 real(kind=DP) :: acc_g,cSOL
 !*
 real(kind=DP) :: Lref,denom
 !*
 real(kind=DP) :: tau, M

 !-------------------------------- BEGINNING! --------------------------------!

 mm = y(1) ! [kg]
 vm = y(2) ! [m/s]
 hm = y(3) ! [m]
 Om = y(4) ! [rad]
 lm = y(5) ! [m]
 Xm = y(6) ! [m]
 Ym = y(7) ! [m]
 Tm = y(8) ! [K]
 Im = y(9) ! [W]

 !* atmospheric density, pressure and temperature at altitude hm
 !- Pa [N/m2]
 !- Ta [K]
 !- da [kg/m3]
 call atm_planet(hm,da,Pa,Ta)

 atm(1) = da
 atm(2) = Pa
 atm(3) = Ta

 !* Gravitational acceleration and Solar constant
 call param_physics(acc_g,cSOL)

 !*  outer radius meteoroid [m]
 ro = (mm/((4._dp/3._dp)*PI*dm)) ** (1._dp/3._dp)

 !* cross-sectional area of the body [m2] 
 Ac  = 1.21 * (mm/dm)**(2._dp/3._dp)

 !* Surface area of the meteoroid [m2]  
 Acs = 4._dp * Ac

 !* drag coefficient (spherical body) [dimensionless]  
 Cd = 2.0_dp

 !* Heat of ablation
 if (Tm .lt. Tmelt) then
   Lref = Linf
 else
   Lref = Lsup
   Tm=Tmelt
 end if
 
 ! 1 2 3 4 5 6 7 8
 ! m,v,h,o,l,X,Y,T
 
 !* dm/dt [kg/s]
 ps = Pa0 * exp((Lref*mu)/(kB*Tmelt)) * exp(-(Lref*mu)/(kB*Tmelt))
 pv = 0._dp
 denom = sqrt((2._dp*PI*kB*Tm)/mu)
 Pspall = atan(0.04_dp * (Tm-Tmelt)) / PI + 0.5_dp 
 if (Pspall .gt. 1.0_dp) then
   print*,'ERREUR : P_spall ne peut pas etre > 1 !'
   stop
 end if
 yp(1) = -Ac * psi * (ps-pv) / denom * Pspall
 if (yp(1) .gt. 0.0_dp) then
   print*,'ERREUR : la masse augmente !'
   stop
 end if 

 !* dvm/dt [m/s2]
 yp(2) = -(Cd/2._dp) * (da * vm**2/mm) * Ac +acc_g*cos(Om)
 
 !* dhm/dt [m/s] 
 yp(3) = -vm * cos(Om)
 if (Om .gt. PI/2.1_dp) then
   Om=PI/2.1_dp
   yp(3) = -vm*cos(Om)
    if (y(2) .lt. 20.0_dp) stop
 end if
 
 !* dOm/dt [rad/s]
 yp(4) = -acc_g * sin(Om) / vm

 !* dlm/dt [m/s]
 yp(5) = vm * sin(Om)

 !* dX/dt = dl sin(Om)
 yp(6) = yp(5) * sin(Om)

 !* dY/dt
 yp(7) = yp(7)

 !* dTs/dt [K/s] 
 Erad = Acs * cSB * 0.9_dp * (Tm**4 - Ta**4)
 ! note: in Campbell-Brown & Koschny, the term A(m/rho_m)^2/3 is the cross sectional area, ie Acs here
 Eimp = (1._dp/2._dp) * 0.5_dp * Ac * da * vm**3 
 Evap = Lref * yp(1)
 yp(8) = (Eimp-Erad-Evap) / (cm*mm)
 if (Tm .ge. Tmelt) then
  Tm=Tmelt
  yp(8) = 0.0d0
 end if
 
 !* Intensite lumineuse
 tau = 2._dp * 10._dp**(-3)
 Im = -tau * yp(1) * (vm**2)/2._dp
  
 M = 6.8_dp - 1.086 * log(Im) ! La magnitude
 end subroutine AFM_abl_Campbell_VC

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                 MANAGEMENT OF THE ABLATION PHENOMENON                  ##!
 !##            BOROVICKA (2012) MODIFIED BY VAUBAILLON 2014                ##!
 !##     EDITED BY COQUERELLE  in order to consider the temperature.        ##!
 !##                                                                        ##!
 !## takes into account the thermal heating of the particle                 ##!
 !## in order to comply with CNES demands, and in order to reproduce        ##!
 !## the number of 10^0-10^2 kg fragments for e.g Chelyabinsk (i.e.=0)      ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine AFM_abl_BoroVaub_VC(y,yp,atm)
 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O 
 real(kind=DP), dimension(1:9), intent(INOUT) :: y ! m,v,h,o,l,X,Y,I
 real(kind=DP), dimension(1:9), intent(OUT) :: yp
 real(kind=DP), dimension(1:3), intent(OUT) :: atm
 !DVA Variables 
 real(kind=DP) :: x,l,s
 !*
 real(kind=DP) :: mm,vm,hm,Om,lm,Xm,Ym,Ts,Tm,Im
 !* provisoire
 real(kind=DP) :: Cd, Ch
 real(kind=DP) :: Ac, Acs
 real(kind=DP) :: Q
 real(kind=DP) :: diso
 real(kind=DP) :: t
 real(kind=DP) :: Q_therm, rho_sl, Vc, coef_therm
 !* 
 real(kind=DP) :: acc_g,cSOL
 !*
 real(kind=DP) :: Qheat, Gamma , Lambda
 !*
 real(kind=Dp) :: Lref,tau,M
 !*
 real(kind=DP) :: Kappa,eta
 
 integer :: k
 
 !-------------------------------- BEGINNING! --------------------------------!

 mm = y(1) !* [kg]
 vm = y(2) !* [m/s]
 hm = y(3) !* [m]
 Om = y(4) !* [rad]
 lm = y(5) !* [m]
 Xm = y(6) !* [m]
 Ym = y(7) !* [m]
 Tm = y(8) ! [K]
 Im = y(9) ! [W]


 !* atmospheric density, pressure and temperature at altitude hm
 !- Pa [N/m2]
 !- Ta [K]
 !- da [kg/m3]
 call atm_planet(hm,da,Pa,Ta)

 atm(1) = da
 atm(2) = Pa
 atm(3) = Ta

 !* Gravitational acceleration and Solar constant
 call param_physics(acc_g,cSOL)

 !*  outer radius meteoroid [m]
 ro = (mm/((4._dp/3._dp)*PI*dm)) ** (1._dp/3._dp)

 !* cross-sectional area of the body [m2] 
 Ac  = PI * ro**2  

 !* Surface area of the meteoroid [m2]  
 Acs = 4._dp * Ac

 !* drag coefficient (spherical body) [dimensionless]  
 Cd = 2.0_dp
 
 if (Tm .lt. Tmelt) then
   Lref = Linf
 else
   Lref = Lsup
   Tm=Tmelt
 end if

 Gamma = 1.0_dp
 Lambda = 1.0_dp
 ! thermal flux computed with: Detra, Kemp et Riddel
 rho_sl = 1.225  ! [kg/m^3]
 Vc     =7802.88 ! [m/s]
 Q_therm=11028.5E+04 / sqrt(ro) * sqrt(da / rho_sl) * (vm / Vc)**3.15_dp
 coef_therm=0.9_dp
 
 Qheat = 2.0E+10_dp ! [J/kg]
 !* dm/dt [kg/s]
 Kappa = 5.8_dp * 10._dp**(-3)
 eta = 0.5_dp
 yp(1) = -(Lambda * Ac * da * vm**3) / (2._dp*Qheat) - coef_therm*Q_therm/Qheat !- Kappa * eta * (mm**(2._dp/3._dp)) * dm * vm**3

 !* dvm/dt [m/s2] 
 yp(2) = -(3._dp*Cd/8._dp) * (da/dm) * (vm**2)/ro + acc_g*cos(Om)

 !* dhm/dt [m/s] 
 yp(3) = -vm * cos(Om)

 !* dOm/dt [rad/s]
 yp(4) = -acc_g * sin(Om) / vm

 !* dlm/dt [m/s]
 yp(5) = vm * sin(Om)

 !* dX/dt = dl sin(Om)
 yp(6) = yp(5) * sin(Om)

 !* dY/dt
 yp(7) = yp(7)
 
 !* dTs/dt [K/s] 
 Erad = Acs * cSB * 0.9_dp * (Tm**4 - Ta**4) ! note: in Campbell-Brown & Koschny, the term A(m/rho_m)^2/3 is the cross sectional area, ie Acs here
 Eimp = (1._dp/2._dp) * 0.5_dp * Ac * da * vm**3 
 Evap = Lref * yp(1)
 yp(8) = (Eimp-Erad-Evap) / (cm*mm)
 if (Tm .ge. Tmelt) then
  Tm=Tmelt
  yp(8) = 0.0d0
 end if
 
  !* Intensite lumineuse
 tau = 5._dp * 10._dp**(-6)
 Im = -tau * yp(1) * (vm**2)/2._dp
  
 M = 2.5_dp * log(Im) ! La magnitude

 if (mm .le. mstop) then
  y(1)=0.9_dp*mstop
  yp(1:nbeqn)=-1.0E-10
 endif

 !* Test for NaN (Not a Number)
 do k = 1,nbeqn
   if (isnan(y(k))) then
     print*,'FATAL ERROR: y is NaN !!!'
     print*,'y=',y(1:nbeqn)
     print*,'yp=',yp(1:nbeqn)
     stop
   end if
 end do
 do k = 1,nbeqn
   if (isnan(yp(k))) then
     print*,'FATAL ERROR: yp is NaN !!!'
     print*,'y=',y
     print*,'yp=',yp
     print*,'Q_therm=',Q_therm
     print*,'Q_therm/Qheat=',Q_therm/Qheat
     print*,'Boro=',-(gamma * Ac * da * vm**3) / (2._dp*Qheat)
     stop
   end if
 end do

 end subroutine AFM_abl_BoroVaub_VC

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE AFM_MOD
