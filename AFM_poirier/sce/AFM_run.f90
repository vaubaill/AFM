 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

Program AFM_RUN 

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                              MEAN PROGRAM                              ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~! 
 
 USE PRECISION
 USE ARRAY
 USE PARAM_FILE
 USE VARGLOBAL
 USE MANAGE_FILE
 USE AFM_PAR
 USE MATHS 
 USE CONVERSION
 USE TEMPS
 USE INPUT
 USE USER_PAR
 USE OUTIL_AFM
 !USE MPI ! parallelization librairy
 USE CHAROPS
 
 !----------------------------------------------------------------------------!
 !--           Listing of the subroutines available in this module          --!
 !----------------------------------------------------------------------------!
 !--                                                                        --!
 !--  AFM_run        : mean program of the software                         --!
 !--  initialization : management of input files                            --!
 !--                                                                        --!
 !----------------------------------------------------------------------------!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !** ROL : - running A.F.M. software                                        **! 
 !**       - output files management                                        **!
 !**       - running time computing                                         **!
 !**                                                                        **!
 !****************************************************************************!

 !FON Declaration of local variables
 implicit none 
 !* File management
 integer(kind=SP) :: ios 
 !* Running computing time 
 real(kind=DP)    :: tdeb,tfin,tifin,tideb
 !* Iteration
 integer(kind=SP) :: idbody, i
 !*
 character(len=80) :: commen(1:80)
 character(len=128) :: arg
 !*
 integer(kind=SP) :: nsrv,ncon,nste,npbm,nsat,nesc,nint
 integer(kind=SP) :: ndata
 !*
 integer(kind=SP) :: nrdis,nvdis
 integer(kind=SP) :: igcpt
 integer(kind=DP) :: icpt
 !* time
 real(kind=DP)    :: time_s,time,time_save
 integer(kind=SP) :: time_d, time_h, time_m
 real(kind=DP)    :: coef_x
 !* file handling
 integer(kind=SP), dimension(1:10) :: list_file
 !* file existence
 logical :: ex
 !* Parallelization 
 integer(kind=SP) :: code
! integer(kind=SP), dimension(MPI_STATUS_SIZE) :: statut
 character(len=12) :: proc

 !-------------------------------- BEGINNING! --------------------------------!
 !* Initialization of MPI
! call MPI_INIT(code)
 !* Number of available processus
! call MPI_COMM_SIZE(MPI_COMM_WORLD,n_p,code) 
! call MPI_COMM_RANK(MPI_COMM_WORLD,t_p,code)
!  print*,'processor ',t_p,'/',n_p,' is been used'
n_p=1 ! comment for parallel version
t_p=0 ! comment for parallel version
 proc =  trim(adjustL(ItoC(t_p)))    ! in parallel version
 
 !* -- Read program argument, if any
 i=1
 arg = 'in'
 do while (arg /= ' ') 
   call getarg (i,arg)
   select case (arg)
   case ('-i','-I','-conf','-CONF')
    call getarg (i+1,arg)
    if (arg /= '') then
    loc%file(0)= trim(arg) ! read configuration name
    print*,'loc%file(0)=',loc%file(0)
    i = i+1
    end if
   case ('-user','-usr','-data','-datain','-in')
    call getarg (i+1,arg)
    if (arg /= '') then
    loc%file(5)= trim(arg) ! read user input data file name
    print*,'loc%file(5)=',loc%file(5)
    i = i+1
    end if
   end select
   i = i + 1
 end do

 ! initialization: default output directory: Note: this variable may be re-defined in the configuration file
 defoutdir='../user/out/'

 !* --  Initialization - reads configuration file
 write(*,'(A4,i4,A43)')"t_p=",t_p," --> Reading USER'S FILE/MODEL'S FILE DATA "
 call initialization()

! changing the name of output files if needed (in parallel version)
 if (t_p==0) write(*,'(A20,A128)') "(AFM_run) defoutdir=",defoutdir
 do i=10,20
  loc%file(i)=trim(adjustL(defoutdir))//trim(adjustL(loc%file(i)))//proc  ! of type: ../user/out/tables/INT/STEPS_fra.dat0
 end do
 ! check if the output directory exists. If not create it + the subdirectories
 inquire(DIRECTORY=defoutdir,exist=ex)
 if (.not. ex) then
  call system("mkdir -p "//trim(adjustL(defoutdir))//"graphics")
  call system("mkdir -p "//trim(adjustL(defoutdir))//"tables")
 end if
 if (t_p==0) call system("cp "//trim(adjustL(loc%file(0)))//" "//trim(adjustL(defoutdir))//"/")
 
 ! Allocate the memory
 allocate(y100(1:nbeqn))
 allocate(data(0:9,maxdim))

 !* -- Data input file
 if (t_p==0) write(*,'(A56)')"AFM_run: Reading/Converting/Writing all data in DATABASE"
 call input_all(ndata)
 if (t_p==0) write(*,'(A28,I6)')"AFM_run:  OK  - Data read : ",ndata
 !* End of input data
 if (nBODY%irestrict .eq. 0) then
  nBODY%ibeg=1
  nBODY%iend=ndata
 else
  if ((nBODY%ibeg .lt. 1) .or. (nBODY%iend .gt. ndata)) then
   write(*,*)"AFM_run: *** FATAL ERROR: pb of boundaries with nBODY%ibeg or nBODY%iend. FYI, ndata=",ndata
   stop
  end if
  if (nBODY%ibeg .gt. nBODY%iend) then
   print*,'AFM_run: FATAL ERROR: nBODY%ibeg .gt. nBODY%iend'
   stop
  end if
 end if
 
 
 
 call chunck_MPI(nBODY%ibeg,nBODY%iend) ! distribute the particle number into the /= processors
 
 
 !* -- Numerical integration
 list_file(1:7) = (/10,11,14,15,16,19,20/)
 call open_file('w',7,list_file)
 if (t_p==0) write(*,'(A41)')"AFM_run: --> Numerical integration starts"
 call CPU_time(tdeb)
 igcpt = 0_sp
 nrbnd = 0_sp ! number of rebounds=0

 ! reprise if multiple bodies
 if ((ibodies .eq. 2) .and. (iresume .eq. 1)) then
  ! reads *cur.dat* file
  open(unit=num%file(17),file=loc%file(17),iostat=ios)
  read(num%file(17),fmt%file(17),iostat=ios) nBODY%icur
  close(unit=num%file(17))
  if (t_p==0) print*,'AFM_run: reprise multi bodies ok. nBODY%icur=',nBODY%icur
 else 
  nBODY%icur=-1
 end if

 print*,'AFM_run: proc=',t_p,' now Integrating from particle number: ',cd_p(t_p),' to:',cf_p(t_p)
 do idbody= cd_p(t_p), cf_p(t_p)
   if (idbody .lt. nBODY%icur) then
    cycle
   else
    nBODY%icur=idbody
   end if
   if (ibodies .eq. 2) then ! if multiple bodies, write down current body id in file(17)
    open(unit=num%file(17),file=loc%file(17),iostat=ios)
    write(num%file(17),fmt%file(17),iostat=ios) nBODY%icur
    close(unit=num%file(17))
   end if
   nsrv = 0_sp
   ncon = 0_sp
   npbm = 0_sp
   nste = 0_sp
   nsat = 0_sp
   nesc = 0_sp
   nint = 0_sp
   sSAVE(1:8) = 0.0
   print*,'AFM_run: proc=',t_p,' === Now integrating object #: ',nBODY%icur
   call CPU_time(tideb)
   !hmax = data(4,idbody)
   hmax = 130000_dp ! [m]
   time = data(0,idbody)                                                      ! 0 1 2 3 4 5 6 7 8  9
   y100(1:nbeqn) = (/data(1,idbody),data(3:nbeqn+1,idbody)/) ! because data is: t,m,r,v,h,o,l,X,Y[,T]: do not inculde t nor r
   print*,'AFM_run:      ! m,v,h,o,l,X,Y[,T] '
   print*,'AFM_run: y100=',y100(1:nbeqn)   ! m,v,h,o,l,X,Y[,T]
   call AFMcontrol(idbody,time,y100,ncon,npbm,nste,nsrv,nsat,nesc,nint,icpt)
   call CPU_time(tifin)

   if (npbm .ne. 0) write(*,'(A39)')"    NO  - Error : integration aborted !"
   write(*,'(A42,I9)')"AFM_run:  number of rebounded bodies     =",nrbnd
   write(*,'(A42,I9)')"AFM_run:  number of consumed bodies      =",ncon
   write(*,'(A42,I9)')"AFM_run:  number of fragmentation steps  =",nste
   write(*,'(A42,I9)')"AFM_run:  number of meteoroids at ground =",nsrv
   write(*,'(A42,I9)')"AFM_run:  number of orbited bodies       =",nsat
   write(*,'(A42,I9)')"AFM_run:  number of escaped bodies       =",nesc

   call conv_dhms(tifin-tideb,time_d,time_h,time_m,time_s)
!   write(*,'(A10,I6,A1,I6,A4,3(I2,A1),F6.3,A1,I9,A11)') "    OK  - ",i,"/", nBODY%iend-nBODY%ibeg+1  ,"  - ",time_d,"d",time_h,"h",time_m,"m",time_s,"s",icpt," iterations"  
   print*,"AFM_run:  Particle id ",idbody,' integration done. Time:', time_d,"d",time_h,"h",time_m,"m",time_s,"s",icpt," iterations" 
   igcpt = igcpt+icpt  
 end do

 !call close_file(7,(/10,11,14,15,16/))
close(10)
close(11)
close(14)
close(15)
close(16)

 write(*,*)"AFM_run:     OK  - number of iterations           =",igcpt
 !* End of numerical integration

 call clean_close()

 
 ! now launch the graphic creation tools
 
 end program AFM_RUN
 
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                             INITIALIZATION                             ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !** ROL : - (a) - reading of user file                                     **!
 !**       - (b) - data conversion into S.I. units                          **!
 !**       - (c) - check of user file's parameters                          **!
 !**       - (d) - reading of A.F.M. model's parameters                     **!  
 !**       - (e) - check of A.F.M. model's parameters                       **! 
 !**       - (f) - input files management                                   **!   
 !**                                                                        **!
 !****************************************************************************!
 
 subroutine initialization()

 USE PRECISION
 USE VARGLOBAL
 USE INPUT
 USE USER_PAR
 USE AFM_PAR

 !--------------------------------- BEGINNING --------------------------------!

 !* (a) Read user file
 call read_par() 

 !* (b) Data conversion into S.I. units
 call conv_par() 
 write(*,'(A52)')"    OK  - User File      : Conversion data        --"

 !* (c) Control of user file's parameters 
 !call check_par()

 !* (d) Reading of A.F.M. model's parameters
 call read_mod()

 !* (e) Control of A.F.M. model's parameters
 !call check_mod()

 !* (f) Input files management
 !call data_conv()

 end subroutine initialization

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
