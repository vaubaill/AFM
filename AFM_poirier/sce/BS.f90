MODULE BS

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                  BURLISH-STOER NUMERICAL INTEGRATION                   ~~! 
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE VARGLOBAL
 USE AFM_MOD

 CONTAINS

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                      NUMERICAL INTEGRATION CONTROL                     ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !# INPUT:
 !#     t : time
 !#     topt : optimized time step
 !#     y : array of Y
 !#     dy : dy/dt
 !#     atm : variable containing the state of the atmosphere

 subroutine BScontrol(t,topt,y,dy,atm)
 
 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 real(kind=DP), intent(INOUT) :: t,topt
 real(kind=DP), dimension(1:nbeqn), intent(INOUT) ::  y ! m,v,h,o,l,X,Y[,T]
 real(kind=DP), dimension(1:nbeqn), intent(INOUT) :: dy ! dm/dt,dv/dt,dh/dt,do/dt,dl/dt,dX/dt,dY/dt[,dT/dt]
 real(kind=DP), dimension(1:3), intent(  OUT) :: atm
 !DVA Variables
 !real(kind=DP),dimension(1:nbeqn) :: s 
 logical :: fin

 !-------------------------------- BEGINNING! --------------------------------!

 fin = .false.
 !s = 0._dp
 call BS1966(t,topt,y,dy,epstol,10_sp,1_sp,fin,atm)

 end subroutine BScontrol

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                      NUMERICAL INTEGRATION (BASIC)                     ##!
 !##                                                                        ##!
 !############################################################################!
 !#
 !# INPUT:
 !#     t : time
 !#     h : time step
 !#     y : array of Y
 !#     dy : dy/dt
 !#     eps : epsilon: precision control
 !#     extrap : maximum nnumber of extrapolation
 !#     automa : automated step control
 !#     fin : Logical end of integration
 !#     atm : variable containing the state of the atmosphere
 !# 
 subroutine BS1966(t,h0,y,dy,eps,extrap,automa,fin,atm)
 
 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O   
 integer(kind=SP), intent(   IN) :: extrap, automa
 real (kind=DP)  , intent(INOUT) :: t
 real (kind=DP)  , intent(INOUT) :: h0
 real (kind=DP)  , intent(   IN) :: eps
 real (kind=DP)  , dimension(1:nbeqn), intent(INOUT) ::  y ! t,m,v,h,o,l,X,Y[,T]
 real(kind=DP)   , dimension(1:nbeqn), intent(INOUT) :: dy ! dm/dt,dv/dt,dh/dt,do/dt,dl/dt,dX/dt,dY/dt[,dT/dt]
 !real (kind=DP)  , dimension(1:nbeqn), intent(INOUT) :: s
 logical         , intent(INOUT) :: fin
 real(kind=DP)   , dimension(1:3), intent(OUT) :: atm 
 !DVA Variables
 logical :: konv, bo, bh
 integer(kind=SP) :: Passe, M, R, SR, JJP1, JP1, LP1, KP1, i, k, KK
 real(kind=DP)    :: A, FC, U, XDKG, V, B1, TA, DK, DM, G, B, Cc
 real(kind=DP), dimension(1:nbeqn) :: dz, ya, yl, ym
 real(kind=DP), dimension(10) :: D
 real(kind=DP), dimension(1:nbeqn,7) :: dt
 real(kind=DP), dimension(8,1:nbeqn) :: yg, yh  

 
 ! *** Explication des noms de certaines variables ****
 ! JP1 : nombre d'extrapolations successives = 1 à extrap
 ! B  = h = h / n (cf G)
 ! G = h/2 = h / 2n, avec n pair (n=2m ou 2M)  (cf B et M)
 ! M = n/2  (cf G)
 ! DM = M converti en double

! print*,'========= entering BS1966 ============'
! print*,'t=',t
! print*,'h0=',h0
! print*,'y=',y
! print*,'dy=',dy
! print*,'eps=',eps
! print*,'extrap=',extrap
! print*,'automa=',automa
! print*,'fin=',fin
! print*,'atm=',atm
 
 !-------------------------------- BEGINNING! --------------------------------!
 dt = 0._dp
 Passe = 0
! print*,'First call to AFM_abl'
 call AFM_abl(y,dy,atm)
 dz(1:nbeqn) = dy(1:nbeqn)
 Passe = 1
 bh  = .false.
 fin = .false.
 ya = y           ! en remplacement d'une boucle DO
 konv = .false.
 LOOP1: do while (.not. konv)
   A = h0 + t
   ! h est un pas "global". le sous-programme calcule y(t+h) avec plusieurs
   ! pas  de calcul reels h (plus petits que h), puis estime la meilleure
   ! valeur de y(t+h) en fonction des differentes valeurs trouvees. Le
   ! nombre de pas h essayes (ils forment une suite decroissante) est
   ! inferieur ou egal a extrap. Le pas h ne peut changer automatiquement
   ! que si automa=1. Si automa=0, le pas est consqtant.
   M = 1
   R = 2
   SR = 3
   FC = 1.5_dp
   bo = .false.
   JJP1 = 0
   ! Extrapolations successives
   LOOP2: do JP1 = 1,extrap
     if (bo) then
       D(2) = 16._dp/9._dp
       D(4) = 64._dp/9._dp
       D(6) = 256._dp/9._dp
     else
       D(2) = 2.25_dp
       D(4) = 9._dp
       D(6) = 36._dp
     end if
     konv = .false.
     if (JP1 .gt. 3) konv = .true.
     if (JP1 .gt. 7) then
       LP1 = 7
       D(7) = 64._dp
       FC = 0.6_dp*FC
     else
       LP1 = JP1
       D(LP1) = M**2
     end if
     M = 2*M
     DM = (M)
     G = h0/DM
     B = G*2._dp
     if (.not. bh .or. JP1 .ge. 9) then
       KK = (M-2)/2
       ! Calcul de T(H)
       M = M-1
       do i = 1,nbeqn
         YL(i) = YA(i)
         YM(i) = YA(i)+G*DZ(i)
       end do
       do k = 1,M
         DK = dble(k)
         XDKG = t + DK*G    ! t+h
!         print*,'Second call to AFM_abl. t=',XDKG
         call AFM_abl(YM,dy,atm)
 
         do i = 1,nbeqn
           U = YL(i) + B*dy(i)
           YL(i) = YM(i)
           YM(i) = U
           U = abs(U)
           if (U .gt. sSAVE(i)) sSAVE(i) = U
         end do
         if (k .ne. KK .or. k .eq. 2) cycle
         JJP1 = 1 + JJP1
         do i = 1,nbeqn
           YH(JJP1,i) = YM(i)
           YG(JJP1,i) = YL(i)
         end do
       end do
     else
       do i = 1,nbeqn
         YM(i) = YH(JP1,i)
         YL(i) = YG(JP1,i)
       end do
     end if
!     print*,'Third call to AFM_abl'
     call AFM_abl(YM,dy,atm)
!     print*,'BS1966: y=',YM,' dy=',dy ! A VIRER !!!
     
    ! Calcul de la valeur extrapolee T(0)
     do i = 1, nbeqn
       V = DT(i,1)
       TA = (YM(i)+YL(i)+G*dy(i))/2._dp
       Cc = TA
       DT(i,1) = TA
       do KP1 = 2, LP1
         B1 = D(KP1)*V
         B = B1-Cc
         U = V
         if (B /= 0._dp) then
           B = (Cc-V)/B
           U = Cc*B
           Cc = B1*B
         end if
         V = DT(i,KP1)
         DT(i,KP1) = U
         TA = U+TA
       end do
       if (abs(y(i)-TA) .gt. eps*sSAVE(i)) then
           konv = .false.
       end if
       y(i) = TA
     end do
     if (konv) exit LOOP2
     D(3) = 4._dp
     D(5) = 16._dp
     BO = .not. bo
     M = R
     R = SR
     SR = M*2
   end do LOOP2
   ! Plus de extrap etapes d'extrapolation etaient necessaires.
   ! Si AUTOMA=1 (changement de pas automatique), on recommence avec un pas
   ! "global" (par opposition au pas de calcul), deux fois plus petit.
   ! N.B. Extrap doit etre plus petit ou egal a 10. (GB)
   ! N.B. Le calcul est optimise pour un nombre d'extrapolation voiqsin, en
   ! moyenne, de 5. (AB)
   if (automa .eq. 0) exit LOOP1
   BH = .not.BH
   fin = .true.
   h0 = h0/2._dp
 end do LOOP1
 if (automa .eq. 0) FC = 1._dp
 h0 = FC*h0
 t = A
! print*,'BS: t=',t
! print*,'BS: h0=',h0
! print*,'BS: y=',y
! print*,'BS: dy=',dy
! print*,'========= Exit BS1966 ============'
 
 end subroutine BS1966

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE BS
