!##############################################################################

!==============================================================================!
!                                                                              !
! R�partition des paquets de particules dans les processeurs                   !
!                                                                              !
! ENTREE : i1, i2   = indices des particules � integrer                              !
!                                                                              !
! SORTIE : variables de DCL_MPI.h : cd_p, cf_p                                 !
!                                                                              !
!==============================================================================!
!                                                                              !
! Composition : CINES; adaptation et arrangement : J. VAUBAILLON, IMCCE - CNES !
!                                                                              !
!==============================================================================!
! Copyright (C) CINES                                                          !
! Projet PINTEM : 2002, J. VAUBAILLON, IMCCE - CNES - CINES                    !
!==============================================================================!

!##############################################################################

subroutine chunck_MPI(i1, i2)

!   use mpi
   USE VARGLOBAL

   implicit none

   integer, intent(in) :: i1, i2
   
   integer :: nelem_p                         ! nbre d elements a repartir
   integer :: nchunck_p                       ! nbre d elements par morceau
   integer :: nreste_p                        ! reste a affecter parmi les n_p noeuds
   integer ::  j_p,  i

!-------------------------------------------------------------------

   ! Reinitialiser n_p, s'il a chang� lors de la precedente integration
!   call MPI_COMM_SIZE( MPI_COMM_WORLD, n_p, i )

    nelem_p= i2 - i1 + 1
    nchunck_p=nelem_p / n_p
    nreste_p = nelem_p - nchunck_p * n_p
    cd_p(0) = i1
    
   
   
    if (nchunck_p .eq. 0) then 
        nchunck_p = 1
        cf_p(0) = nchunck_p
        do j_p = 1, nreste_p - 1
          cd_p(j_p) = cf_p(j_p -1) + 1
          cf_p(j_p) = cd_p(j_p) + nchunck_p - 1
        enddo
        n_p = nreste_p
    else
        cf_p(0) = i1+nchunck_p-1
        do j_p = 1, n_p - nreste_p - 1
          cd_p(j_p) = cf_p(j_p -1) + 1
          cf_p(j_p) = cd_p(j_p) + nchunck_p - 1
        enddo
        do j_p = n_p - nreste_p , n_p -1
          cd_p(j_p) = cf_p(j_p -1) + 1
          cf_p(j_p) = cd_p(j_p) + nchunck_p 
        enddo
    endif

if (t_p==0) then
 print'("chunck_MPI: n_p = ", i6)', n_p
 print'("chunck_MPI: nelem_p = ", i6)', nelem_p
 print'("chunck_MPI: nchunck_p = ", i6)', nchunck_p
 print'("chunck_MPI: nreste_p = ", i6)', nreste_p
 do j_p = 0,n_p-1
  print*,'chunck_MPI: t_p = ',j_p,' cd_p(j_p)=',cd_p(j_p),' cf_p(j_p)=',cf_p(j_p)
 end do
end if


return

end subroutine chunck_MPI

