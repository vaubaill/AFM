SUBROUTINE splint(index, x, val_out)
  
!  USE param
  USE nrutil, ONLY : assert_eq,nrerror
  USE nr, ONLY: locate
  USE nrtype
  USE VARGLOBAL, only : atm_n,spline_data,spline_data_pp

  
  IMPLICIT NONE
  
  INTEGER(I4B) :: khi,klo,n

  INTEGER(I4B), INTENT(IN) :: index

  REAL(SP), INTENT(IN) :: x
  REAL(SP), INTENT(OUT) :: val_out
  REAL(SP), DIMENSION(atm_n) :: xa, ya, y2a
  REAL(SP) :: a,b,h

!  IF (diag_ctrl >= 1 ) THEN
!     WRITE(30,*) 'SPLINT.F90 entered.'
!     WRITE(30,*) ' '
!  ENDIF

  xa(1:atm_n) = spline_data(1:atm_n,1)

  IF ( index == 1 ) THEN
     ya(1:atm_n) = spline_data(1:atm_n,2)
     y2a(1:atm_n) = spline_data_pp(1:atm_n,1)
  ELSE IF ( index == 2 ) THEN
     ya = spline_data(:,3)
     y2a = spline_data_pp(:,2)     
  END IF
	
  n=assert_eq(SIZE(xa),SIZE(ya),SIZE(y2a),'splint')
  
  klo=MAX(MIN(locate(xa,x),n-1),1)
  
  khi=klo+1
  h=xa(khi)-xa(klo)

  IF (h == 0.0) CALL nrerror('bad xa input in splint')
  
  a=(xa(khi)-x)/h
  b=(x-xa(klo))/h
  
  val_out=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.0_sp

!  IF (diag_ctrl >= 1 ) THEN
!     WRITE(30,*) 'SPLINT.F90 exited.'
!     WRITE(30,*) ' '
!  ENDIF

END SUBROUTINE splint
