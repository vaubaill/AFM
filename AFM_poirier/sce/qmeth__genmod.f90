        !COMPILER-GENERATED INTERFACE MODULE: Tue May 29 09:28:11 2018
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE QMETH__genmod
          INTERFACE 
            SUBROUTINE QMETH(TMIN,TMAX,EA,R1,EB1,DD1,R2,EB2,DD2,ERR)
              REAL(KIND=4) :: TMIN
              REAL(KIND=4) :: TMAX
              REAL(KIND=4) :: EA(6)
              REAL(KIND=4) :: R1(6)
              REAL(KIND=4) :: EB1(5)
              REAL(KIND=4) :: DD1
              REAL(KIND=4) :: R2(6)
              REAL(KIND=4) :: EB2(5)
              REAL(KIND=4) :: DD2
              INTEGER(KIND=4) :: ERR
            END SUBROUTINE QMETH
          END INTERFACE 
        END MODULE QMETH__genmod
