!#+INIT#############################################################
!#
!#+FUNC CtoI
!#
!###################################################################
!#
!#+CALL ctoi (chaine)
!#+CATE Utilitaire
!#+TITL Conversion d'une chaine de caracteres en entier
!#+AUTH (C) 2005, J. Berthier & F.Vachier - IMCCE
!#+PROJ Eproc
!#+VERS 4.0
!#
!# Derniere modification
!#+MODI 2002-10-06 : creation JBE
!#
!###################################################################
!#
!# Variables d'entree/sortie : 
!#
!#+VINP  chaine    CH(*) chaine a convertir
!#+VOUT  CtoI      IN    nombre entier correspondant 
!#
!###################################################################

function CtoI (chaine)

   implicit none
   character(len=*), intent(in) :: chaine
   integer                      :: CtoI

   read(chaine,*) CtoI

   return
end function CtoI

!=== FIN ======================================================================!
