        !COMPILER-GENERATED INTERFACE MODULE: Tue Nov  3 10:34:24 2020
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE SPLINE_JMA__genmod
          INTERFACE 
            SUBROUTINE SPLINE_JMA(XINT,YINT,YP1,YPN,Y2)
              USE VARGLOBAL, ONLY :                                     &
     &          ATM_N
              REAL(KIND=4), INTENT(IN) :: XINT(ATM_N)
              REAL(KIND=4), INTENT(IN) :: YINT(ATM_N)
              REAL(KIND=4), INTENT(IN) :: YP1
              REAL(KIND=4), INTENT(IN) :: YPN
              REAL(KIND=4), INTENT(OUT) :: Y2(ATM_N)
            END SUBROUTINE SPLINE_JMA
          END INTERFACE 
        END MODULE SPLINE_JMA__genmod
