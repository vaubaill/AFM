 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE PHYSICS

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                           PHYSICAL CONSTANTS                           ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE VARGLOBAL

 !FON Declaration of global parameters
 implicit none 

 !* Atmospheric density at sea level [kg/m3]
 real(kind=DP), parameter :: da0 = +1.22500E+00_dp

 !* Atmospheric temperature at sea level [K]
 real(kind=DP), parameter :: Ta0 = +2.88150E+02_dp

 !* Atmospheric pressure at sea level [Pa] = [N/m2]
 real(kind=DP), parameter :: Pa0 = +1.01325E+05_dp

 !* Stefan-Boltzmann constant [W/m2/K4]
 real(kind=DP)  :: cSB = 5.67E-08_dp

 !* Boltzmann constant [m2.kg/s2/K]
 real(kind=DP) :: kB = 1.3806488E-23_dp 

 !* U.A. [m]
 real(kind=DP) :: ua2m = 149597871000._dp

 CONTAINS

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                                                                        ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 ! INPUT:
 !      NONE
 !
 ! OUTPUT:
 !      acc_g : acceleration of gravity [m/s2]
 !      cSOL : Solar constant [W/m2]]
 !
 subroutine param_physics(acc_g,cSOL)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 real(kind=DP), intent(OUT) :: acc_g
 real(kind=DP), intent(OUT) :: cSOL
 !DVA Variables

 !-------------------------------- BEGINNING! --------------------------------!

 select case(iplanet)
 case(1) ! Earth
   acc_g = 9.80665_dp ! [m/s2]
   cSOL  = 1360.8_dp  ! [W/m2]
 case(2) ! Mars
   acc_g = 3.7_dp     ! [m/s2]
   cSOL  = 600._dp    ! [W/m2] 
 end select

 end subroutine param_physics

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE PHYSICS
