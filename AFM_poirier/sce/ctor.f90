!#+INIT#############################################################
!#
!#+FUNC CtoR
!#
!###################################################################
!#
!#+CALL ctor (chaine)
!#+CATE Utilitaire
!#+TITL Conversion d'une chaine de caracteres en reel 8
!#+AUTH (C) 2005, J. Berthier & F.Vachier - IMCCE
!#+PROJ Eproc
!#+VERS 4.0
!#
!# Derniere modification
!#+MODI 2002-10-06 : creation JBE
!#
!###################################################################
!#
!# Variables d'entree/sortie : 
!#
!#+VINP  chaine    CH(*) chaine a convertir
!#+VOUT  CtoR      DP    nombre reel correspondant 
!#
!###################################################################

function CtoR (chaine)
 USE PRECISION

   implicit none
   character(len=*), intent(in)  :: chaine
   real(kind=DP)                 :: CtoR

   read(chaine,*) CtoR

   return
end function CtoR

!=== FIN ======================================================================!
