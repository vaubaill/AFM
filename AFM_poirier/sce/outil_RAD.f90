 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE OUTIL_RAD

 USE PRECISION
 USE PARAM_FILE
 USE TEMPS

 CONTAINS

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!#############################################################################!
!##                                                                         ##!
!##                           RADIANT COMPUTATION                           ##!
!##                                                                         ##!
!#############################################################################!

 !****************************************************************************!
 !** Variables en entree : **                                               **!
 !***************************                                               **!
 !**                                                                        **!
 !**  methode : methode de calcul du radiant                                **!
 !**    Q : Q-adjustment                                    (Asegawa, 1990) **!
 !**    W : Rotation of the line of apsides        (Steel & Baggaley, 1985) **!
 !**    H : Omega-adjustment                               (Hasegawa, 1990) **!
 !**    B : Variation of perihelion distance and excentricity (Svoren,1993) **!
 !**    A : Rotation around the lines of apsides       (Svoren et al.,1993) **!
 !**    P : Porter's method                                   (Porter,1952) **!
 !**                                                                        **!
 !** Variables en sortie : **                                               **!
 !***************************                                               **!
 !**                                                                        **!
 !** rad  : radiant                                                         **!
 !**                                                                        **!
 !****************************************************************************! 
 subroutine RADnum(jj,orbkep,ADm,DECm,vm)

 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O
 real(kind=DP), intent( IN) :: jj
 real(kind=DP), dimension(1:6), intent( IN) :: orbkep
 real(kind=DP), intent(OUT) :: ADm,DECm
 real(kind=DP), intent(OUT) :: vm
 !* Data type
 real(kind=DP) :: a,e,i,GOM,pom,M
 !* 
 real(kind=DP) :: Tmin,Tmax
 real(kind=DP), dimension(6) :: EA
 real(kind=DP), dimension(6) :: R1,R2
 real(kind=DP), dimension(5) :: EB1,EB2
 real(kind=DP) :: DD1,DD2
 integer(kind=SP) :: ERR

 !****************************************************************************!

 a   = orbkep(1)
 e   = orbkep(2)
 i   = orbkep(3)
 GOM = orbkep(4)
 pom = orbkep(5)
 M   = orbkep(6)

 !****************************************************************************!
 !**   ## Description des variables :                                       **!
 !**       EA(1) : distance perihelique                                [UA] **!
 !**       EA(2) : excentricite                                             **!
 !**       EA(3) : argument du periastre                              [deg] **!
 !**       EA(4) : longitude du noeud ascendant                       [deg] **!
 !**       EA(5) : inclinaison (ecliptique)                           [deg] **!
 !**       EA(6) : temps en 'Julian Centuries' depuis JD = 2451545.0  [deg] **!
 !****************************************************************************!
 EA(1) = a*(1._dp-e)  
 EA(2) = e
 EA(3) = pom
 EA(4) = GOM
 EA(5) = i
 EA(6) = (jj-JD2000)/36525.0_dp

 Tmin = (jj-365.25_dp/2._dp-JD2000)/36525.0_dp
 Tmax = (jj+365.25_dp/2._dp-JD2000)/36525.0_dp

 !****************************************************************************!
 !**   ## Description des variables :                                       **!
 !**    R1(2)  : ascension droite du radiant (predit)                 [deg] **!
 !**    R1(3)  : declinaison du radiant      (predit)                 [deg] **!
 !**    R1(4)  : vitesse geocentrique        (predit)                [km/s] **!
 !****************************************************************************!

 call QMETH(Tmin,Tmax,EA,R1,EB1,DD1,R2,EB2,DD2,ERR)
 if (ERR .eq. 1) then
   call WMETH(Tmin,Tmax,EA,R1,EB1,DD1,R2,EB2,DD2,ERR)
 end if
 if (ERR .eq. 1) then
   call HMETH(Tmin,Tmax,EA,R1,EB1,DD1,R2,EB2,DD2,ERR)
 end if
 if (ERR .eq. 1) then
   call BMETH(Tmin,Tmax,EA,R1,EB1,DD1,R2,EB2,DD2,ERR)
 end if
 if (ERR .eq. 1) then
   call AMETH(Tmin,Tmax,EA,R1,EB1,DD1,R2,EB2,DD2,ERR)
 end if
 if (ERR .eq. 1) then
   call PMETH(Tmin,Tmax,EA,R1,EB1,DD1,R2,EB2,DD2,ERR)
 end if

 if (ERR .eq. 1) then 
   STOP 'No Methods, for computing Radiant, works'
 else
   ADm  = R1(2) ! [deg]
   DECm = R1(3) ! [deg]
   vm   = R1(4) ! [km/s]
 end if

 end subroutine RADnum

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

END MODULE OUTIL_RAD
