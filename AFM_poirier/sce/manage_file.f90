 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE MANAGE_FILE

 USE PRECISION
 USE VARGLOBAL
 USE PARAM_FILE 
 
 CONTAINS

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                        PROCESS OF OPENING FILE                         ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !* rw = 'r' : read
 !* rw = 'w' : write

 subroutine open_file(rw,nlist,list)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 character(len=1), intent( IN) :: rw
 integer(kind=SP), intent( IN) :: nlist
 integer(kind=SP), dimension(1:nlist), intent( IN) :: list
 !DVA Variables
 !* iteration
 integer(kind=SP)  :: i
 !* file
 integer(kind=SP)  :: ios
 character(len=128) :: locfile
 integer(kind=SP)  :: numfile
 logical :: ex

 !-------------------------------- BEGINNING! --------------------------------!
 
 do i = 1,nlist
   numfile = list(i)
   locfile = loc%file(numfile)
 print*,"proc=",t_p," now opening file=",locfile
   
   ! if open in read mode, check if file exists
   if (rw .eq. "r") then
    inquire(file=locfile, exist=ex)
    if (.not. ex) then
      print*,'*** FATAL ERROR: file ',locfile,' DOES NOT EXIST!!!'
      stop
    end if
   end if
  
   if ((iappend .eq. 1) .AND. (numfile .ge. 10)) then
    open(numfile,file=locfile,iostat=ios,POSITION='APPEND')
   else
    open(numfile,file=locfile,iostat=ios)
   end if

   !* Test opening file 
   if (ios .ne. 0) then
     write(*,'(A18,1x,A60,i6)')"Error opening file",locfile,ios
     call clean_close()
     STOP
   end if

   if (rw .eq. "w") then
    if (iappend==0) then 
    if (t_p==0) then
     select case(numfile)
     case(10)
       write(numfile,'(A126)') "##############################################################################################################################"
       write(numfile,'(A126)') "## ParentID ## External pressure [Pa] ## Tensile strength [Pa] ## mass       [kg] ## altitude   [km] ## number of fragments ##"
       write(numfile,'(A126)') "##          ##                        ##                       ##                 ##                 ##                     ##"
     case(12)
       write(numfile,'(A68)') "####################################################################"
       write(numfile,'(A68)') "## number of iterations ## step size of numerical integration [s] ##"
       write(numfile,'(A68)') "##                      ##                                        ##"
     case(4,11,14,15,16,18,19,20)
       write(numfile,'(A373)') "#####################################################################################################################################################################################################################################################################################################################################################################################"
       write(numfile,'(A373)') "## Id     ##  time[s]      ## mass       [kg] ## radius      [m] ## velocity [km/s] ## altitude   [km] ## Zen. Ang. [deg] ## Dis. traj. [km]  ## Xm [m]         ##  Ym [m]         ## Temperature [K] ## dm/dt [kg/s]    ## dr/dt    [m/s]   ## dv/dt [km/s/s]  ## dh/dt   [km/s]  ## do/dt [deg/s]   ## dl/dt [km/s]    ## dXm/dt [m/s]    ## dYm/dt [m/s]    ## dT/dt   [K/s]      "
       write(numfile,'(A373)') "##        ##               ##                 ##                 ##                 ##                 ##                 ##                  ##                ##                 ##                 ##                 ##                  ##                 ##                 ##                 ##                 ##                 ##                 ##                    "
     end select
    end if
    end if
   else if (rw .eq. "r") then
     select case(numfile)
     case(4,10,11,12,14,15,16,19,20)
       read(numfile,*)
       read(numfile,*)
       read(numfile,*)
     end select
   end if
 end do 

 end subroutine open_file

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                        PROCESS OF CLOSING FILE                         ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

 subroutine close_file(nlist,list)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 integer(kind=SP), intent( IN) :: nlist
 integer(kind=SP), dimension(1:nlist), intent( IN) :: list
 !DVA Variables
 !* iteration
 integer(kind=SP) :: i
 integer(kind=SP)  :: numfile

 !-------------------------------- BEGINNING! --------------------------------!
 
 do i = 1,nlist    
   numfile = list(i)  
   close(numfile)
 end do

 end subroutine close_file

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                         CLEAN CLOSURE OF FILES                         ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine clean_close()

 !FON Declaration of local variables
 implicit none
 !MPI Variables
 integer :: err
 
 !-------------------------------- BEGINNING! --------------------------------!

 deallocate(data)
 deallocate(y100)
 
 ! end MPI
! call MPI_FINALIZE(err)
 
 print*,'End of AFM program'

 end subroutine clean_close

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE MANAGE_FILE
