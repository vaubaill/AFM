####################################################################
#                                                                  #
# PROGRAMME DE CONCATENATION DES FICHIERS DE RESULTATS DE AFMp     #
#                                                                  #
# entree : root : root of the files to concatenate                 #
#                                                                  #
####################################################################
# COPYRIGHT: J. VAUBAILLON, R. DECOSTA, IMCCE - CNES, 2014         #
####################################################################
prog="AFMp_concatene.sh"
syntax="$prog root"
ext=".dat"

#pushd ./
#cd ../user/out/tables/

allroots=(STEPS_int STEPS_con STEPS_err STEPS_fra STEPS_res STEPS_cur STEPS_lst)
for root in ${allroots[@]}
do
 outputfile="$root$ext"
 list=`ls $root.dat*`
 n=${#list[@]}
# if ("$n" -gt "0") then
  cat $list > $outputfile
  rm $list
# else echo "no file of type $root.dat*"
# fi
done

#popd
echo "$prog done"
