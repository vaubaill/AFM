#!/usr/bin/perl

use File::Copy;

####################################################################
#                                                                  #
# PROGRAMME DE SOUMISSION DE FICHIERS afm POUR CERFEUIL            #
#                                                                  #
# entree : root : configuration file root (with path)              #
#          OPTION: afm_script_model : model script for submission  #
#                                                                  #
####################################################################
# COPYRIGHT: J. VAUBAILLON, R. DECOSTA, IMCCE - CNES, 2014         #
####################################################################
$prog="submitconf ";
$syntax="$prog root [afm_script_model] ";
$AFM_script_model="AFM_script.sh";
$root="";

$execdir="$ENV{'$SCRATCHDIR'}";

if (@ARGV) {
     $nbarg=int(@ARGV);
     if ($nbarg == 0) { die "FATAL ERROR: syntax is: $syntax \n";}
     if ($nbarg gt 2) { die "FATAL ERROR: syntax is: $syntax \n";}
     $root=$ARGV[0];
     if ($nbarg == 2) {$AFM_script_model=$ARGV[1];}
     }
else {die "FATAL ERROR: syntax is: $syntax \n";}

$fichtmp="junk";
$cnt=0;

# copy user configuration file into execdir (scratch)
############ CHANGE THIS !!!!! ###########################3
#copy("sourcefile","destinationfile") or die "Copy failed: $!";

@FILE= <$root*>; 
foreach $file (@FILE) {
 open (MOD, $AFM_script_model) or die "FATAL ERROR: IMPOSSIBLE TO OPEN FILE $AFM_script_model \n";
 open (TMP, ">$fichtmp")       or die "FATAL ERROR: IMPOSSIBLE TO OPEN TEMPORARY FILE $fichtmp\n";
  while ($line=<MOD>){

    $line=" mpiexec AFMp.x $file";
 print "$line \n";
   } # end of if ($line =~ /mpiexec/)
   print TMP <<AFFICHE;
$line
AFFICHE
 } # end of while ($line=<MOD>)
 close (MOD);
 close (TMP);
 $cmd="qsub $fichtmp";
 print "$cmd \n";
 system("$cmd");
 $cnt++;
} # end of foreach $file (@FILE)
system("qstat");
print "($prog) $cnt jobs were submitted.\n";
print "$prog done \n";
