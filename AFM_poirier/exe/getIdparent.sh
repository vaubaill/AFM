# get te Id of the AFM parent bodies that were successfully integrated

alloutputfile=(out_1_48 out_49_96 out_97_144 out_145_192 out_193_240  out_241_288  out_289_336  out_337_360)

outputfile="Idparent.dat"

# get first generation:
#grep iteration AFM_script.sh.o1891* | grep d | awk '{print$4}' | sed 's/\///' > $outputfile
# get second generation of bodies:
#grep done *.o* | awk '{print $3}' > $outputfile


# for occigen
(test -e $outputfile) && rm $outputfile
for file in ${alloutputfile[*]}
do
 echo "now treating $file"
 grep "integration done. Time" $file | awk '{print $2}' | sort >> $outputfile
done

#chmod u-w $outputfile

echo "Idparent saved in: $outputfile"
