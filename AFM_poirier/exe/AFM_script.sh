# ##################################################
# soumission d'un job sur le frontal
# ##################################################
# Si c'est la premiere utilisation lancer les commandes suivantes
# depuis le frontal de soumission des jobs :
# 
# chmod u+x AFM_script.sh
# qsub AFM_script.sh

# Le script suivant est de type shell 
#$ -S /bin/sh

# ##################################################
# La file d'attente que nous voulons
# ##################################################
# short : < 30mn | medium : < 1 jour | long : 4 semaines
# -----          ------               ---- 
#$ -q temps_moyen
#$ -p -100

# request the given resources: DO NOT CHANGE FOR CERFEUIL
#$ -l ccj=1

# ##################################################
#  Adresse mail pour notification etat du job
# ##################################################
#$ -M vaubaill@imcce.fr

# ##################################################
# notification par mail :
# notification debut de job (b)egin 
# notification erreur de job (a)bort
# notification fin de job (e)nd 
# ##################################################
#$ -m bae

# ##################################################
# On travaille depuis le repertoire ou le pg se trouve
# ##################################################
#$ -cwd

# ##################################################
# Travail dans l\u2019environnement parallele "mpich2"
# Pour lister les environnements // dispos : qconf -spl depuis le maitre
# 
# La valeur 2 indique le nb de slots (processus) necessaire
# ##################################################
#$ -pe mpich2 5

# ##################################################
## Sortie standard et sortie erreur sont redirigees dans un meme fichier
# ##################################################
#$ -j y

# Conserver variables environnement
#$ -V

echo "*****************************************************"
echo "               DETAIL RUNNING PROGRAM                "
echo "*****************************************************"
echo "nb processeurs attribues par sge : $NSLOTS (slots)"
echo "exec host: $HOSTNAME"
echo "job_id: $JOB_ID"
echo "PE_HOSTFILE: $PE_HOSTFILE"
echo "*****************************************************"
echo ""
echo "*****************************************************"

echo "*****************************************************"
echo "               OUTPUT RUNNING PROGRAM                "
echo "*****************************************************"

# Demarrage programme 
mpiexec AFMp.x ../user/in/userfile/BIGPARA/para_all_body_11_20
