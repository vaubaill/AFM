#!/usr/bin/perl

use File::Copy;
use File::Basename;

####################################################################
#                                                                  #
# PROGRAM THAT CREATES CONFIGURATION FILES FOR AFM AS WELL AS      #
# SCRIPTS FOR THE OCCIGEN PARALLEL MACHINE                         #
#                                                                  #
# input                                                            #
#          config_model  : model of configuration file for AFM     #
#          n_beg       : id of first particle to integrate         #
#          n_end       : id of last  particle to integrate         #
#          n_part      : numbre of particles per file: it HAS to   #
#                        be a multiple of 24 for occigen machine   #
#          afm_script_model : AFM_script model file                #
#          nproc      : number of processors                       #
#                                                                  #
# output                                                           #
#        1-[(n_end-n_beg)/n_part] configuration files for AFM      #
#          labelled $config_model_$ni_$ne where $ni and $ne are the #
#          beginning and ending particle number label for each     #
#          configuration file                                      #
#        2-same number of afm_script_model files to be queued in   #
#          occigen machine queue                                   # 
#        3-the file AFMjustdoit.sh contains all the command lines  #
#          needed to submit all the jobs on occigen machine        #
#                                                                  #
####################################################################
# COPYRIGHT: J. VAUBAILLON, IMCCE - PROJECT INSIGHT, 2015          #
####################################################################
$prog="createAFMconf-para.sh ";
$AFM_script_model="AFM_script.slurm";
$syntax="$prog config_model n_beg n_end n_part afm_script_model nproc";

if (@ARGV) {
     $nbarg=int(@ARGV);
     unless ($nbarg == 6) { die "$prog *** FATAL ERROR: syntax is: $syntax \n";}
     $fichconf=$ARGV[0];
     $nbeg=$ARGV[1];
     $nend=$ARGV[2];
     $npart=$ARGV[3];
     $AFM_script_model=$ARGV[4];
     $nproc=$ARGV[5];
     }
else {die "$prog *** FATAL ERROR: syntax is: $syntax \n";}
# double check the arguments
$check=$npart-24;      if ($check lt 0)    {die "$prog *** FATAL ERROR: npart HAS to be multiple of 24 since nodes have 24 procs \n";}
$check=$npart-$nproc;  if ($check lt 0)    {die "$prog *** FATAL ERROR: npart HAS to be greater or equal to nproc \n";}
$residual=$nproc % 24; if ($residual ne 0) {die "$prog ***  FATAL ERROR: nbnode HAS to be a multiple of 24";}
$nbnode=int($nproc / 24);
$ntaskspernode=24;



# initializations
$scratchAFMdir="$ENV{'SCRATCHDIR'}/AFM/";
$execdir="$scratchAFMdir/exe/";
$AFMx="$execdir/AFMp.x";
$userconfdir="user/in/userfile/";
$homeCONFdir="$ENV{'HOME'}/AFM/$userconfdir/";
$scratchCONFdir="$ENV{'SCRATCHDIR'}/AFM/$userconfdir/";


$justdoitfile="AFMjustdoit.sh";
open (AFM, ">$justdoitfile") or die "$prog *** IMPOSSIBLE D'OUVRIR LE FICHIER $justdoitfile\n";

$cnt=0;
for ($i=$nbeg-1; $i <= $nend; $i=$i+$npart) {
 $nbegtmp=$i+1;
 $nendtmp=$i+$npart;
 if ($nbegtmp > $nend) {next}
 if ($nendtmp > $nend) {$nendtmp=$nend}
 $sfix="\_$nbegtmp\_$nendtmp";
 $fichtmp="$fichconf$sfix";
 open (TMP, ">$fichtmp") or die "$prog *** IMPOSSIBLE D'OUVRIR LE FICHIER TEMPORAIRE\n";
 open (CONF, $fichconf)  or die "$prog *** IMPOSSIBLE DE LIRE LE FICHIER DE CONFIGURATION $fichconf \n $!";
 while ($line=<CONF>){
 $reco='defoutdir      : ../user/out/';
  if ($line =~ /$reco(\w+)/ ) {
   $outputdir="$1$sfix";
   $line="defoutdir      : ../user/out/$outputdir/           # default output directory        -OK-";
 }
  if ($line =~ /iappend/) {
   if ($cnt == 0) {
   $line="iappend        : 0                          # Append results after existing ones (0/1)           -OK-";
  } else {
   $line="iappend        : 1                          # Append results after existing ones (0/1)           -OK-";
  }
  }
  if ($line =~ /restrict/) {
   $line="irestrict      : 1  # Restrict the bodies id (0/1)                            -OK-";
  }
  if ($line =~ /ibeg/) {
   $line="ibeg           : $nbegtmp # First body id to simulate                         -OK-";
  }
  if ($line =~ /iend/) {
   $line="iend           : $nendtmp # Last  body id to simulate                         -OK-";
  }
  chop $line;
  print TMP <<AFFICHE;
$line
AFFICHE
  } # end of while ($line=<CONF>)
 close (CONF);
 close (TMP);

 # copy the  configuration file into the $SCRATCHDIR directory
 $fichtmpbase=basename($fichtmp);
 $scratchfichtmp="$scratchCONFdir/$fichtmpbase";
 copy("$fichtmp","$scratchfichtmp") or die "$prog *** FATAL ERROR: Copy of $fichtmp into $scratchfichtmp failed: $!";

 # sets the variable for the slurm file
 $cmdafm="$AFMx -conf $scratchfichtmp";
 $outputfile="$ENV{'SCRATCHDIR'}/AFM/output/$outputdir";
 $slurmname="$outputdir";
 $trvtsk=0; $trvnod=0 ; $trvexc=0;
 
 # now changes the AFM_script slurm file and submit it to the queue
 $AFM_script_tmp="$AFM_script_model\_$nbegtmp\_$nendtmp.sh";
 open (MOD, $AFM_script_model)  or die "$prog *** FATAL ERROR: IMPOSSIBLE TO OPEN FILE $AFM_script_model \n";
 open (TMP, ">$AFM_script_tmp") or die "$prog *** FATAL ERROR: IMPOSSIBLE TO OPEN TEMPORARY FILE $AFM_script_tmp\n";
  while ($line=<MOD>){
   chop $line;
   if ($line =~ /SBATCH -J/)       {$line="#SBATCH -J $slurmname";}
   if ($line =~ /output/)          {$line="#SBATCH --output $outputfile";}
   if ($line =~ /ntasks=/)         {$line="#SBATCH --ntasks=$nproc";$trvtsk=1;}
   if ($line =~ /ntasks-per-node/) {$line="#SBATCH --ntasks-per-node=$ntaskspernode";}
   if ($line =~ /nodes/)           {$line="#SBATCH --nodes=$nbnode";$trvnod=1;}
   if ($line =~ /srun/)            {$line="srun --mpi=pmi2 -K1 --resv-ports -n \$SLURM_NTASKS  $cmdafm \n";$trvexc=1;}
   print TMP <<AFMSCRIPT;
$line
AFMSCRIPT
 } # end of while ($line=<MOD>)
 close (MOD);
 close (TMP);
 # double check
 unless ($trvtsk) {die "$prog *** FATAL ERROR: ntasks not found in slurm file $AFM_script_model\n";}
 unless ($trvnod) {die "$prog *** FATAL ERROR: trvnod not found in slurm file $AFM_script_model\n";}
 unless ($trvexc) {die "$prog *** FATAL ERROR: trvexc not found in slurm file $AFM_script_model\n";}


 $cmd="sbatch $AFM_script_tmp";
 print AFM <<JDI;
$cmd
JDI
 system("chmod u+x $AFM_script_tmp");

 $cnt++;
} # end of for ($i=$nbeg-1; $i <= $nend; $i=$i+$npart)
print AFM <<TOOL;
TOOL
close (AFM);

system("chmod u+x $justdoitfile");
print "($prog) $cnt files were created.\n";
print "($prog) Now you can run the command: \n";
print "($prog) $justdoitfile \n";
print "($prog) END\n";
