####################################################################
#                                                                  #
# PROGRAMME DE CONCATENATION DES FICHIERS DE RESULTATS DE AFMp     #
# Obtenus dans divers repertoires de travail                       #
#                                                                  #
####################################################################
# COPYRIGHT: J. VAUBAILLON, R. DECOSTA, IMCCE - CNES, 2014         #
####################################################################
prog="AFMp_concatene_multi.sh"
syntax="$prog root"
ext=".dat"

pushd ./
cd /scratch/cnt0026/pss7341/vaubaillon/AFM/user/


alldirs=(out_1_48 out_49_96 out_97_144 out_145_192 out_193_240 out_241_288  out_289_336  out_337_360)

allroots=(STEPS_int STEPS_con STEPS_err STEPS_fra STEPS_res STEPS_cur STEPS_lst)

outdir="out_all"
outsubdir="/tables/"
ext=".dat"
# clean the output and temporary directories
(test -e $outdir)   && rm -r $outdir
mkdir -p $outdir$outsubdir
i=1

for root in ${allroots[@]}
do
 outputfile="$root$ext"
 for element in ${alldirs[@]}
 do
    list=`find $element -name $outputfile* -print | sort`
    for file in ${list[@]}
    do
     cat $file >> $outdir$outsubdir$outputfile
    done
 done
 echo "data saved in $outdir$outsubdir$outputfile"
done


popd
echo "$prog done"
