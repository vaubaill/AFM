"""Create AFM configuration files


"""
import os
import numpy as np

AFMconf_model = '../user/in/userfile/templateBoro.in'
outdir_root = '../user/out/4AE/'
outdir_root = '/home/vaubaill/PROJECTS/PODET/PODET-MET/FRIPON/Data/Fakeor/AFM_simul/AFM_poirier/'
#outdir_root = '/media/sf_Volumes/Elements/AFM/4AE/'
outfile_name = '4AE.cf'

angle_rng =[0.0,85.0]
angle_stp = 5.0
mass_rng_log = [-2,1]
mass_stp_log = 0.2
velocity_rng = [12.0,71.0]
velocity_stp = 2.0
#velocity_rng = [36.0,71.0]
#velocity_stp = 100.0
for velocity in np.arange(velocity_rng[0],velocity_rng[1],velocity_stp):
    for mass_log in np.arange(mass_rng_log[0],mass_rng_log[1],mass_stp_log):
        for angle in np.arange(angle_rng[0],angle_rng[1],angle_stp):
            outdir_subroot = '{0:.0f}'.format(velocity) + '_kms_E' + '{0:.1f}'.format(mass_log) + '_kg_'+ '{0:.0f}'.format(angle)+'_deg'
            defoutdir = outdir_root + outdir_subroot + '/'
            if not os.path.isdir(defoutdir):
                os.mkdir(defoutdir)
                os.mkdir(defoutdir+'/tables')
                os.mkdir(defoutdir+'/graphics')
            print(defoutdir)
            outfile_name = outdir_subroot + '.cf'
            outfile = defoutdir + outfile_name
            print(outfile)
            mass = 10.0**mass_log
            with open(AFMconf_model,'r') as fin:
                lines = fin.readlines()
            with open(outfile,'w') as out:
                for line in lines:
                    line = line[0:-1]
                    if ('defoutdir') in line:
                        line = 'defoutdir      : '+defoutdir+'          # default output directory        -OK'
                    if 'vm0' in line:
                        line = 'vm0            : '+'{:+.5e}'.format(velocity)+' # initial value of speed                 [km/s] -OK-'
                    if 'Om0' in line:
                        line = 'Om0            : '+'{:+.5e}'.format(angle)+' # initial value for zenithal angle        [deg] -OK-'
                    if 'mm0' in line:
                        line = 'mm0            : '+'{:+.5e}'.format(mass)+' # initial value of mass                    [kg] -OK-'
                    print(line)
                    out.write(line+'\n')
            print('configuration file saved in '+outfile)
            
            # now submit command
            cmd = 'AFM.x -i '+outfile
            print(cmd)
            os.system(cmd)
            print('Data saved under: '+outdir_root+outdir_subroot)
