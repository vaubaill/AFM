#!/bin/bash
#SBATCH -J Mars_481_528
#SBATCH --mail-type=END
#SBATCH --mail-user=vaubaill@imcce.fr
#SBATCH --nodes=2
#SBATCH --ntasks=48
#SBATCH --ntasks-per-node=24
#SBATCH --threads-per-core=1
#SBATCH --time=01:00:00
#SBATCH --output /scratch/cnt0026/pss7341/vaubaillon/AFM/output/Mars_481_528

module purge
module load intel/15.0.0.090
module load bullxmpi/1.2.8.3

cd /scratch/cnt0026/pss7341/vaubaillon/AFM/exe/

srun --mpi=pmi2 -K1 --resv-ports -n $SLURM_NTASKS  /scratch/cnt0026/pss7341/vaubaillon/AFM//exe//AFMp.x -conf /scratch/cnt0026/pss7341/vaubaillon/AFM/user/in/userfile///MarsMulti_481_528 


echo "(afm.slurm) AFM program done."

