"""histogram of Chelyabinsk observed mass hitogram

From Borovicka et al 2013, extended data table d

"""

import numpy as np
from matplotlib import pyplot as plt
import astropy.units as u

# data from Borovicka et al 2013
mass = np.array([15.,450.,30.,20.,3.,1.,2.,0.5,5.,1.5,0.2,0.5,5.,0.3])*u.kg


fileout='Chelyabinsk-MassHist.png'
fig = plt.figure()
n, bins, patches = plt.hist(np.log10(mass.to('kg').value))
plt.title('Histogram of Chelyabinsk picked up mass')
plt.xlabel('log(Mass) [kg]')
plt.ylabel('N')
plt.savefig(fileout,dpi=300)
plt.close(fig)
print('Figure saved in '+fileout)



mtmp = 1.0E+06
nfra=1000
allP =np.random.random(nfra)
mass = -(mtmp/nfra) * (np.log10(1.0-allP))
fig = plt.figure()
n, bins, patches = plt.hist(np.log10(mass))
plt.title('Framgnetation mass distribution')
plt.xlabel('log(Mass) [kg]')
plt.ylabel('N')
plt.show()
