# uncompyle6 version 3.2.3
# Python bytecode 2.7 (62211)
# Decompiled from: Python 2.7.13 (default, Nov 24 2017, 17:33:09) 
# [GCC 6.3.0 20170516]
# Embedded file name: initialize.py
# Compiled at: 2018-05-04 11:48:51
"""
05/05/2018
Author : Jeremie Vaubaillon
Copyright : Observatoire de Paris
Free license

Purpose: 
 Initializes fundamental data for the AFM software

"""
import os, sys, logging, numpy as np, astropy.units as u, numpy as np
from astropy.time import Time
home = os.environ['HOME']

class AFM(object):
    afm_path = home + '/PROJECTS/PODET/PODET-MET/ADMIRE/AFM/outil26_AFM/CINES/AFM/'
    afm_exe_path = afm_path + 'exe/'
    afm_dataout = afm_path + 'user/out/'
    log_dir = afm_path + 'LOG/'
    log_dft_file = log_dir + 'AFM-' + Time.now().isot.replace('-', '').replace(':', '').split('.')[0] + '.log'
    log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
    log_name = 'AFM'
    log = logging.getLogger(log_name)
    log.setLevel(logging.DEBUG)
    log.propagate = True
    sdlr = logging.StreamHandler()
    sdlr.setLevel(logging.INFO)
    sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
    log.addHandler(sdlr)
    hdlr = logging.FileHandler(log_dft_file)
    hdlr.setLevel(logging.DEBUG)
    hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
    log.addHandler(hdlr)
    log.info('Default log file is: ' + log_dft_file)
