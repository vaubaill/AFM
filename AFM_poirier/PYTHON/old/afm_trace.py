import sys
import glob
import os
import logging
import math
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
#matplotlib.use('Agg')
from astropy.table import QTable
import astropy.units as u
from astropy.time import Time as Time
from astropy.time import TimeDelta as TimeDelta
from astropy.utils.iers import IERS_A, IERS_A_URL
from astropy.coordinates import cartesian_to_spherical

from afm_init import AFM


def intgraph(SimName=''):
    """Plot the results of an integration with AFM, without fragmentation.
    
    Parameters
    ----------
    SimName : string
        Name of the output directory containing all the data.
        Ex: 'TESTnofragBoro'
    
    Returns
    -------
    None. 
    
    """
    
    if (SimName==''):
        msg='*** FATAL ERROR: Please specify the name of the simulation to treat.'
        AFM.log.error(msg)
        raise ValueError(msg)
    # set the name of the file to read
    INTfile=AFM.afm_dataout+SimName+AFM.afm_table_dir+AFM.afm_int_file
    # output directory
    out_dir = AFM.afm_dataout+SimName+AFM.afm_graph_dir
    # output file root
    out_root = INTfile.split('.')[0].replace(AFM.afm_table_dir,AFM.afm_graph_dir)
    
    # preparation: set name of columns in file to read
    names=['Id','Time','Mass','Radius','Velocity','Height','ZenithAngle','LDist','X','Y','T',
                'dm/dt','dr/dt','dv/dt','dh/dt','dz/dt','dl/dt','dX/dt','dY/dt','dT/dt']
    # read the file
    AFM.log.info('now reading file '+INTfile)
    with open(INTfile,'r') as filein:
        data=QTable.read(filein,data_start=4,names=names,format='ascii.basic')
    # set data units
    data['Time'].unit = 's'
    data['Mass'].unit='kg'
    data['Radius'].unit='m'
    data['Velocity'].unit='km/s'
    data['Height'].unit='km'
    data['ZenithAngle'].unit='deg'
    data['LDist'].unit='km'
    data['X'].unit='km'
    data['Y'].unit='km'
    data['T'].unit='K'
    data['dm/dt'].unit='kg/s'
    data['dr/dt'].unit='m/s'
    data['dv/dt'].unit='km/s2'
    data['dh/dt'].unit='km/s'
    data['dz/dt'].unit='deg/s'
    data['dl/dt'].unit='km/s'
    data['dX/dt'].unit='km'
    data['dY/dt'].unit='km'
    data['dT/dt'].unit='K/s'
    
    # trace mass=f(time)
    fileout=out_root+'-TimeMass.png'
    fig = plt.figure()
    plt.plot(data['Time'].to('s').value,data['Mass'].to('kg').value,'bo')
    plt.title('Mass vs Time')
    plt.xlabel('Time [s]')
    plt.ylabel('Mass [kg]')
    plt.yscale('log')
    #plt.show()
    plt.savefig(fileout)
    plt.close(fig)
    AFM.log.info('Figure saved in '+fileout)
    
    # trace velocity=f(time)
    fileout=out_root+'-TimeVelocity.png'
    fig = plt.figure()
    plt.plot(data['Time'].to('s').value,data['Velocity'].to('m/s').value,'bo')
    plt.title('Velocity vs Time')
    plt.xlabel('Time [s]')
    plt.ylabel('Velocity [m/s]')
    #plt.show()
    plt.savefig(fileout)
    plt.close(fig)
    AFM.log.info('Figure saved in '+fileout)
    
    # trace mass=f(height)
    fileout=out_root+'-HeightMass.png'
    fig = plt.figure()
    plt.plot(data['Height'].to('km').value,data['Mass'].to('kg').value,'bo')
    plt.title('Mass vs Height')
    plt.xlabel('Height [km]')
    plt.ylabel('Mass [kg]')
    plt.yscale('log')
    #plt.show()
    plt.savefig(fileout)
    plt.close(fig)
    AFM.log.info('Figure saved in '+fileout)
    
    # trace velocity=f(time)
    fileout=out_root+'-HeightVelocity.png'
    fig = plt.figure()
    plt.plot(data['Height'].to('km').value,data['Velocity'].to('m/s').value,'bo')
    plt.title('Velocity vs Time')
    plt.xlabel('Height [km]')
    plt.ylabel('Velocity [m/s]')
    #plt.show()
    plt.savefig(fileout)
    plt.close(fig)
    AFM.log.info('Figure saved in '+fileout)
    
    AFM.log.info('done')

