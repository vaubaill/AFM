"""
05/05/2018
Author : Jeremie Vaubaillon
Copyright : Observatoire de Paris
Free license

Purpose: 
 Initializes fundamental data for the AFM software

"""
import os
import sys
import logging
import numpy as np
import astropy.units as u
import numpy as np
from astropy.time import Time

home=os.environ['HOME']

class AFM(object):
    afm_path=home+'/PROJECTS/PODET/PODET-MET/ADMIRE/AFM/AFM_poirier/'
    afm_exe_path=afm_path+'exe/'
    afm_dataout=afm_path+'user/out/'
    afm_table_dir='/tables/'
    afm_graph_dir='/graphics/'
    afm_int_file='STEPS_int.dat0'
    afm_con_file='STEPS_con.dat0'
    afm_err_file='STEPS_err.dat0'
    afm_fra_file='STEPS_fra.dat0'
    afm_lst_file='STEPS_lst.dat0'
    afm_res_file='STEPS_res.dat0'
    
    # logging directory
    log_dir = afm_path + 'LOG/'
    # default log file
    #log_dft_file = log_dir + 'AFM-' + Time.now().isot.replace('-','').replace(':','').split('.')[0] + '.log' 
    log_dft_file = log_dir + 'AFM.log' 
    # log format
    log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
    # logger name
    log_name='AFM'
    # create logger
    log = logging.getLogger(log_name)
    # set logger level
    log.setLevel(logging.DEBUG)
    log.propagate = True
    # create StreamHandler
    sdlr = logging.StreamHandler()
    # set StreamHandler options
    sdlr.setLevel(logging.INFO)
    sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
    # add StreamHandler into logger
    log.addHandler(sdlr)
    # creates a default file handler
    hdlr = logging.FileHandler(log_dft_file)
    # set file handler options
    hdlr.setLevel(logging.DEBUG)
    hdlr.setFormatter(logging.Formatter(fmt=log_fmt))
    # adds file handler to logger
    log.addHandler(hdlr)
    log.info('Default log file is: '+log_dft_file)
