"""AFM_plots

Author
------
J. Vaubaillon, 2020

Version
-------
1.0

"""
# import all necessary tools
import os
import argparse
import logging
from configparser import ConfigParser, ExtendedInterpolation
# astropy
import astropy.units as u
from astropy.table import QTable
from matplotlib import pyplot as plt

log = logging.getLogger(__name__)
log = logging.getLogger('afm_plots')
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
log.setLevel(logging.DEBUG)
log.propagate = True
sdlr = logging.StreamHandler()
sdlr.setLevel(logging.INFO)
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)

def read_conf(config_file):
    """Read configuration file.
    
    Parameters
    ----------
    config_file : string
        AFM configuration file name.
    
    Returns
    -------
    config : configparser.ConfigParser Object
        ConfigParser.
    
    """
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(config_file)
    config['USER']['home'] = os.getenv('HOME')
    # updates some variables
    config['SIM']['dir_out'] = config['AFM']['user_out']+'/'+config['SIM']['name']+'/'
    config['SIM']['graph_dir'] = config['SIM']['dir_out']+'/'+config['SIM']['graph_dir']+'/'
    config['SIM']['tab_dir'] = config['SIM']['dir_out']+'/'+config['SIM']['tab_dir']+'/'
    config['SIM']['int_file'] = config['SIM']['tab_dir']+'/'+config['SIM']['int_file']
    config['SIM']['frag_file'] = config['SIM']['tab_dir']+'/'+config['SIM']['frag_file']
    config['SIM']['res_file'] = config['SIM']['tab_dir']+'/'+config['SIM']['res_file']
    config['SIM']['con_file'] = config['SIM']['tab_dir']+'/'+config['SIM']['con_file']
    return config

def read_data(data_file,names,unts):
    """Read data in file.
    
    Parameters
    ----------
    data_file : string
        Data file name.
    names : array of strings
        Column names.
    units : array of astropy.unit.Unit object.
        Units for each column.
    
    Returns
    -------
    data : astropy.table.QTable object
        data in data_file.
    
    """
    if not os.path.exists(data_file):
        raise IOError(data_file+' does not exist.')
    log.info('Now reading: '+data_file)
    data = QTable.read(data_file,data_start=0,names=names,format='ascii.basic')
    for (name,unt) in zip(names,unts):
        if unt:
            data[name].unit = unt
    log.info('There are '+str(len(data))+' data in '+data_file)
    return data


def read_frag(config):
    """Read Fragmentation data.
    
    Parameters
    ----------
    config : configparser.ConfigParser Object
        ConfigParser.
    
    Returns
    -------
    data : astropy.table.QTable object.
        data in the fragmentation file.
    
    """
    # read fragmentation output file
    data_file = config['SIM']['frag_file']
    names=['ParentID','P','TS','Mass','Height','Nfrag']
    unts = ['',u.Pa,u.Pa,u.kg,u.km,'']
    data = read_data(data_file,names,unts)
    return data

def plot_frag(config,data):
    """Plot fragmentation events.
    
     Parameters
    ----------
    config : configparser.ConfigParser Object
        ConfigParser.
    data : astropy.table.QTable object.
        data in the fragmentation file.
    
    Returns
    -------
    None.

    """
    # trace mass=f(Tensile_Strength)
    fileout=config['SIM']['graph_dir']+config['SIM']['name']+'-fraMassHeight.png'
    fig = plt.figure()
    plt.plot(data['Mass'].to('kg').value,data['Height'].to('km').value,'bo')
    plt.title('Mass vs Height')
    plt.xlabel('Mass [kg]')
    plt.ylabel('THeight [km]')
    plt.xscale('log')
    plt.savefig(fileout,dpi=300)
    plt.close(fig)
    log.info('Figure saved in '+fileout)
    # trace mass=f(TAltitude)
    fileout=config['SIM']['graph_dir']+config['SIM']['name']+'-fraMassTS.png'
    fig = plt.figure()
    plt.plot(data['Mass'].to('kg').value,data['TS'].to('Pa').value,'bo')
    plt.title('Mass vs Tensile Strength')
    plt.xlabel('Mass [kg]')
    plt.ylabel('Tensile Strength [Pa]')
    plt.xscale('log')
    plt.yscale('log')
    plt.savefig(fileout,dpi=300)
    plt.close(fig)
    log.info('Figure saved in '+fileout)
    return

def read_res(config):
    """Read Fragmentation survival data.
    
    Parameters
    ----------
    config : configparser.ConfigParser Object
        ConfigParser.
    
    Returns
    -------
    data : astropy.table.QTable object.
        data in the survival result file.
    
    """
    data_file = config['SIM']['res_file']   
    names=['ParentID','time','Mass','rad','V','H','z','L','X','Y','T',
           'dm','dr','dv','dh','do','dl','dX','dY','dT']
    unts = ['','s','kg','m','km/s','km','deg','km','m','m','K',
            'kg/s','m/s','km/s2','km/s','deg/s','km/s','m/s','m/s','K/s']
    data = read_data(data_file,names,unts)
    return data

def plot_res(config,data):
    """Plot surviving fragments results.
    
     Parameters
    ----------
    config : configparser.ConfigParser Object
        ConfigParser.
    data : astropy.table.QTable object.
        data in the survival result file.
    
    Returns
    -------
    None.

    """
    # trace mass=f(time)
    fileout=config['SIM']['graph_dir']+config['SIM']['name']+'-resMassDist.png'
    fig = plt.figure()
    plt.plot(data['Mass'].to('kg').value,data['L'].to('km').value,'bo')
    plt.title('Surviving Mass vs Distance')
    plt.xlabel('Mass [kg]')
    plt.ylabel('distance [km]')
    plt.xscale('log')
    plt.savefig(fileout,dpi=300)
    plt.close(fig)
    log.info('Figure saved in '+fileout)
    
    # trace mass histogram
    fileout=config['SIM']['graph_dir']+config['SIM']['name']+'-resMassHist.png'
    fig = plt.figure()
    n, bins, patches = plt.hist(np.log10(data['Mass'].to('kg').value))
    plt.title('Histogram of surviving mass')
    plt.xlabel('log(Mass) [kg]')
    plt.ylabel('N')
    plt.savefig(fileout,dpi=300)
    plt.close(fig)
    log.info('Figure saved in '+fileout)
    
    
    return


def read_con(config):
    """Read consumed fragments data.
    
    Parameters
    ----------
    config : configparser.ConfigParser Object
        ConfigParser.
    
    Returns
    -------
    data : astropy.table.QTable object.
        data in the consumed fragments file.
    
    """
    # read fragmentation output file
    data_file = config['SIM']['con_file']   
    names=['ParentID','time','Mass','rad','V','H','z','L','X','Y','T',
           'dm','dr','dv','dh','do','dl','dX','dY','dT']
    unts = ['','s','kg','m','km/s','km','deg','km','m','m','K',
            'kg/s','m/s','km/s2','km/s','deg/s','km/s','m/s','m/s','K/s']
    data = read_data(data_file,names,unts)
    return data

def plot_con(config,data):
    """Plot surviving fragments results.
    
     Parameters
    ----------
    config : configparser.ConfigParser Object
        ConfigParser.
    data : astropy.table.QTable object.
        data in the consumed file.
    
    Returns
    -------
    None.
    
    """
    # trace mass=f(time)
    fileout=config['SIM']['graph_dir']+config['SIM']['name']+'-conMassAlt.png'
    fig = plt.figure()
    plt.plot(data['Mass'].to('kg').value,data['H'].to('km').value,'bo')
    plt.title('Consumed Mass vs Altitude')
    plt.xlabel('Mass [kg]')
    plt.ylabel('Altitude [km]')
    plt.xscale('log')
    plt.savefig(fileout,dpi=300)
    plt.close(fig)
    log.info('Figure saved in '+fileout)
    return


if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='afm_plots arguments.')
    parser.add_argument('-conf',default='afm.config',help='configuration file. Default is: ./afm.config')
    args = parser.parse_args()
    
    # set configuration file
    config_file = args.conf
    
    # read config
    config = read_conf(config_file)
    
    # plot fragmentation events
    data = read_frag(config)
    plot_frag(config,data)
    # plot surviving fragments 
    data = read_res(config)
    plot_res(config,data)
    # plot consumed fragments 
    data = read_con(config)
    plot_con(config,data)
    
    # plot integration 
    
    
else:
    log.debug('successfully imported')

