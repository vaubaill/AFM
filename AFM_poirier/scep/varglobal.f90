 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE VARGLOBAL

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                     DECLARATION OF GLOBAL VARIABLES                    ~~|
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
! USE MPI ! parallelization librairy
 
 implicit none

  !real(kind=DP), dimension(:), allocatable :: s


 !* Parameters for parallelization
 integer, parameter   :: procs_max_p=60          ! nombre Max de processeurs maxi de la machine
 integer, parameter   :: nombre_message_p=15     ! nombre Max de message en attente de reception
 integer              :: n_p                     ! nombre de noeud alloues
 integer              :: t_p                     ! numero du noeud (de 0 a  N_P -1)
 integer, dimension(0:procs_max_p -1)   :: cd_p  ! cd_p(t_p) debut du morceau a traiter par le noeud t_p
 integer, dimension(0:procs_max_p -1)   :: cf_p  ! cf_p(t_p) fin   du morceau a traiter par le noeud t_p
 integer, dimension(nombre_message_p)   :: Req_p ! Req_p  utilise en mode non bloquant
! integer, dimension(mpi_status_size,nombre_message_p)  :: Status_Req_p

 !****************************************************************************!
 !**                               Parameters                               **!
 !****************************************************************************!

! integer(kind=SP) :: maxdim = 6000000_sp
 integer(kind=SP) :: maxdim = 10000_sp

 real(kind=DP), dimension(:), allocatable :: y100

 real(kind=DP), dimension(:,:), allocatable ::  data

 !****************************************************************************!
 !**                   User file '../user/in/user_in.dat'                   **!
 !****************************************************************************!

 !* Default output directory 
 character(len=128) :: defoutdir
 real(kind=DP)      :: outfps  ! data output frequency [/s]
 integer(kind=SP)   :: iappend
 integer(kind=SP)   :: iresume

 !* Numerical integration Runge-Kutta-Fehlberg 4(5)  
 integer(kind=SP)  :: imodel
 integer(kind=DP)  :: nbeqn
 integer(kind=DP)  :: nbfra
 real(kind=DP)     :: Q_Boro ! Value of Q in Borovicka model. Default is: 2.0E+07
                             ! but authors specify Q=[2.5;6]x1.0E+06 [J/kg]

 real(kind=DP)     :: tpas   ! integration step size
 real(kind=DP)     :: epstol ! tolerance error for appropriate step size
 real(kind=DP)     :: t_init ! initial value of time

 real(kind=DP)     :: hstop  ! Altitud cut off limit
 real(kind=DP)     :: mstop  ! Mass cut off limit
 real(kind=DP)     :: tstop  ! Step size cut off limit
 real(kind=DP)     :: vstop  ! Velocity cut off limit

 !* Input data
 integer(kind=DP)  :: irm,imm

 real(kind=DP)     :: mm0    ! initial value of mass
 real(kind=DP)     :: rm0    ! initial value of radius
 real(kind=DP)     :: vm0    ! initial value of speed
 real(kind=DP)     :: hm0    ! initial value of altitud
 real(kind=DP)     :: Om0    ! initial value of zenithal angle
 real(kind=DP)     :: lm0    ! initial value of distance along trajectory
 real(kind=DP)     :: Ts0    ! initial value of temperature
 real(kind=DP)     :: Xm0    ! initial shift in X in km
 real(kind=DP)     :: Ym0    ! initial shift in Y in km 

 real(kind=DP) :: hmax 

 integer(kind=SP)  :: iintegrator ! integrator choice
 integer(kind=SP)  :: nrbnd       ! numbre of rebound

 type body_one
   !integer(kind=SP) :: i
   integer(kind=SP) :: dis
 end type body_one
 type (body_one) oBODY 

 type body_many
   integer(kind=SP) :: ifile
   integer(kind=SP) :: dis
   real(kind=DP)    :: epsv
   real(kind=DP)    :: epsr 
   integer(kind=DP) :: irestrict ! 0/1 restrict the number of bodies to integrate (yes/no)
   integer(kind=DP) :: ibeg      ! id number of the first body to simulate
   integer(kind=DP) :: iend      ! id number of the last  body to simulate
   integer(kind=DP) :: icur      ! id number of the current body being simulated
   real(kind=DP),dimension(:),allocatable :: coef  ! coefficient for the tensile strength
 end type body_many
 type (body_many) nBODY

 integer(kind=DP) :: ibodies

 !* Material composition of the meteoroid 
 integer(kind=SP)  :: imaterial
! character(len=14) :: nmaterial

 !* Form of the meteoroid
 integer(kind=SP)  :: ishape
 character(len=14) :: nshape

 !* Attractive planet
 integer(kind=SP)  :: iplanet
! character(len=14) :: nplanet 

 !* Atmospheric parameters
 integer(kind=SP)  :: iatmos
 character(len=14) :: natmos 

 !* Pysical modelling
 type ablation
   integer(kind=SP) :: i
 end type ablation
 type (ablation) abl

 type fragmentation
   integer(kind=SP) :: i
   integer(kind=SP) :: tab
   integer(kind=SP) :: ste
 end type fragmentation
 type (fragmentation) fra

 real(kind=DP) :: size_eps
 
 !- Number of fragments
 !- (1) parameter of the geometric law
 real(kind=DP)     :: num_fra  ! number of fragments parameter in the geometric law
 integer(kind=SP)  :: inum_fra ! if =1 then a distribution is computed

 ! Strength model. Choice of Popova2011, Ferrier2012, user
 character(len=6) :: StrengthMod
 ! impose or random method
 type tensile_strength
  real(kind=DP) :: Mref
  real(kind=DP) :: Pref
  real(kind=DP) :: coef
 end type tensile_strength
 type (tensile_strength) tsl_str

 ! interpolate from data
 integer, parameter :: maxdimsmallarray=10             ! maximum number of dimension of small arrays
 type tensile_strength_interp
  integer(kind=SP) :: n                             ! number of tabulated points
  real(kind=DP),dimension(:),allocatable   :: Mref  ! tabulated mass [kg] IN LOGARITHM SPACE
  real(kind=DP),dimension(:),allocatable   :: Pref  ! tabulated strength [Pa] IN LOGARITHM SPACE
  real(kind=DP),dimension(:,:),allocatable :: coef  ! coefficients of the spline function
 end type tensile_strength_interp
 type (tensile_strength_interp) tsl_str_intp


 !* Histogram
 type histogram
   real(kind=DP) :: rad
   real(kind=DP) :: vel  
 end type histogram
 type (histogram) eps

 real(kind=DP) :: rmin,rmax
 real(kind=DP) :: vmin,vmax

 !* 
 real(kind=DP), dimension(1:8) :: sSAVE

 !****************************************************************************!
 !**                Model parameters file '../data/model.dat'               **!
 !****************************************************************************!

 real(kind=DP)     :: am    ! albedo
 real(kind=DP)     :: cm    ! specific heat 
 real(kind=DP)     :: em    ! emissivity 
 real(kind=DP)     :: Tmelt ! Tmelt

 real(kind=DP)     :: Linf  ! latent heat (< Tmelt)
 real(kind=DP)     :: Lsup  ! latent heat (> Tmelt)
 real(kind=DP)     :: mu    ! mass unit
 real(kind=DP)     :: Tc    ! thermal conductivity
 real(kind=DP)     :: A     ! A coefficient for Pvap 
 real(kind=DP)     :: B     ! B coefficient for Pvap

 !****************************************************************************!
 !**                       AFM model '../sce/AFM.f90'                       **! 
 !****************************************************************************!
 
 real(kind=DP)     :: dm    ! meteoroid density
 real(kind=DP)     :: vm    ! meteoroid volume
 real(kind=DP)     :: ri,ro ! meteoroid radius (inner,outer)
 real(kind=DP)     :: Om    ! zenithal angle

 !* Energy
 real(kind=DP)     :: Esol  ! solar irradiation
 real(kind=DP)     :: Eint  ! internal energy
 real(kind=DP)     :: Erad  ! radiative heat losses
 real(kind=DP)     :: Eimp  ! impact of atmospheric molecules
 real(kind=DP)     :: Evap  ! evaporation process

 !* Atmospheric parameters
 real(kind=DP)     :: da    ! atmospheric density
 real(kind=DP)     :: Ta    ! atmospheric temperature 
 real(kind=DP)     :: Pa    ! atmospheric pressure
 real(kind=DP)     :: psi   ! condensation coefficient
 CHARACTER ( len = 100 ) :: atmos_data_file ! file containing data for atmospheric model
 integer           :: data_points   ! No. of atmospheric data points in ATMOS_DATA_FILE.
 REAL(SP), DIMENSION(:,:), ALLOCATABLE :: spline_data    ! atmospheric heights, densities & temperatures,
 REAL(SP), DIMENSION(:,:), ALLOCATABLE :: spline_data_pp ! second derivatives for the cubic spline interpolator



 real(kind=DP)     :: vth, Pvap

 integer(kind=SP) :: istat
 real(kind=DP)    :: epsv,epsr

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE VARGLOBAL
