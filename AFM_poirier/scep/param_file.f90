 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE PARAM_FILE

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                           PARAMETERS OF FILES                          ~~!
 !~~                (number, writing format and localisation)               ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE VARGLOBAL

 !FON Declaration of global variables
 implicit none
 !DVA Variables 

 type file_num
 !****************************************************************************!
 !**                          Data file (00 to 09)                          **!
 !****************************************************************************!
   integer(kind=SP), dimension(0:20) :: file = (/   &
       0_sp                                       , &
       1_sp                                       , &
       2_sp                                       , & 
       3_sp                                       , &
       4_sp                                       , &
       5_sp                                       , &
       6_sp                                       , &
       7_sp                                       , &
       8_sp                                       , &
       9_sp                                       , & 
 !**
      10_sp                                       , & 
      11_sp                                       , &
      12_sp                                       , & 
      13_sp                                       , &  
      14_sp                                       , & 
      15_sp                                       , & 
      16_sp                                       , & 
 !**
      17_sp                                       , &
      18_sp                                       , &
      19_sp                                       , &
      20_sp                                         /)
 !****************************************************************************!
 end type file_num 
 type (file_num) num


 type file_loc
 !****************************************************************************!
 !**                          Data file (00 to 09)                          **!
 !****************************************************************************!
   character(len=128), dimension(0:20) :: file = (/  &
      '../user/in/userfile/test_PARA             ', &  ! 0
      '../data/model.dat                         ', &  ! 1
      '../data/NEObrut.dat                       ', &  ! 2
      '../data/BROsconvsmallTenStrengthVatm.dat  ', &  ! 3
      '../data/database.dat                      ', &  ! 4
      '../data/MarsEntry.dat                     ', &  ! 5 ! may be reset in config file or program options
      '../data/Earth_atmos.txt                   ', &  ! 6
      '                                          ', &  ! 7
      '                                          ', &  ! 8
      '                                          ', &  ! 9
 !* 
      'tables/STEPS_fra.dat                      ', &  ! 10 ! note:L the default output directory will be added in front of the file name
      'tables/STEPS_int.dat                      ', &  ! 11 ! etapes d'integrations (repli seulement si un seul body)
      'tables/STEPS_topt.dat                     ', &  ! 12 ! pas d'integrations !!! OBSOLETE
      'tables/STEPS_atm.dat                      ', &  ! 13 ! caracteristiques de l'atmosphere rencontree
      'tables/STEPS_res.dat                      ', &  ! 14
      'tables/STEPS_con.dat                      ', &  ! 15
      'tables/STEPS_err.dat                      ', &  ! 16 ! errors during the integration
      'tables/STEPS_cur.dat                      ', &  ! 17
      'tables/STEPS_lst.dat                      ', &  ! 18 ! latest state of the particles
      '                                          ', &  ! 19
      '                                          '/)   ! 20
 !****************************************************************************! 
 end type file_loc
 type (file_loc) loc

 type file_fmt
 !****************************************************************************!
 !**                          Data file (00 to 09)                          **!
 !****************************************************************************!
   character(len=80), dimension(0:20) :: file = (/  &
      '                                          ', &  ! 0                
      '(A35,3(3x,E12.5))                         ', &  ! 1
      '(I7,2x,2(1x,F5.3),5(2x,F5.1),3x,F9.1)     ', &  ! 2
      '(3x,E15.8,8(4x,E15.8))                    ', &  ! 3
      '(I8,3x,E15.8,8(4x,E15.8))                 ', &  ! 4
      '(3x,E15.8,8(4x,E15.8))                    ', &  ! 5
      '                                          ', &  ! 6
      '                                          ', &  ! 7
      '                                          ', &  ! 8
      '                                          ', &  ! 9
 !**
      '(I8,3x,E15.8,11x,E15.8,11x,E15.8,5x,E15.8,4x,I19)  ', &  ! 10
      '(I8,3x,E15.8,18(4x,E15.8))                ', &  ! 11
      '(I12,15x,E15.8)                           ', &  ! 12
      '(4(E15.8,2x))                             ', &  ! 13
      '(I8,3x,E15.8,18(4x,E15.8))                ', &  ! 14
      '(I8,3x,E15.8,18(4x,E15.8))                ', &  ! 15
      '(I8,3x,E15.8,18(4x,E15.8),A2)             ', &  ! 16
      '(I8)                                      ', &  ! 17
      '(I8,3x,E15.8,18(4x,E15.8))                ', &  ! 18
      '                                          ', &  ! 19
      '                                          '/)   ! 20
 !****************************************************************************!
 end type file_fmt
 type (file_fmt) fmt 

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                         ~~!
 !~~                                                                         ~~!
 !~~                                                                         ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE PARAM_FILE
