 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE INPUT

 USE PRECISION 
 USE ARRAY
 USE PARAM_FILE
 USE MANAGE_FILE
 USE VARGLOBAL
 USE MATHS
 USE OUTIL_RAD

 CONTAINS

 !----------------------------------------------------------------------------!
 !--           Listing of the subroutines available in this module          --!
 !----------------------------------------------------------------------------!
 !--                                                                        --! 
 !--  input_all :                                                           --!
 !--  input_neo :                                                           --!
 !--                                                                        --!
 !----------------------------------------------------------------------------!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                           OPENING OUTPUT FILE                          ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 ! INPUT:
 !      None
 ! OUTPUT:
 !      ndata : 
 !
 !
 subroutine input_all(ndata)
 
 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 integer(kind=SP), intent(OUT) :: ndata
 !DVA Variables
 !* Iteration
 integer(kind=SP) :: k,i
 integer(kind=SP) :: nneo
 !*
 real(kind=DP) :: time
 ! 0 1 2 3 4 5 6 7 8 9
 ! t m r v h o l x y T
 !--------------------------------- BEGINNING --------------------------------!

 ndata = 0_sp
 if (ibodies .eq. 1) then
   
   !----- One body  
   if (irm .eq. 1) mm0 = dm*((4._dp/3._dp)*PI* rm0**3) ! [kg]
   if (imm .eq. 1) rm0 = (mm0/((4._dp/3._dp)*PI*dm))**(1._dp/3._dp) ! [m]
   ! m,v,h,o,l,x,y,[T]
   data(0,1) = 0.0d0
   data(1,1) = mm0            ! [kg]
   data(2,1) = rm0            ! [m]
   data(3,1) = vm0 * 1000._dp ! [m/s]
   data(4,1) = hm0 * 1000._dp ! [  m]
   data(5,1) = Om0 * DEG2RAD  ! [rad]
   data(6,1) = lm0 * 1000._dp ! [  m]
   data(7,1) = 0.0_dp         ! X [m]
   data(8,1) = 0.0_dp         ! Y [m]
   if (nbeqn==8) then
       data(9,1) = Ts0
   else
       data(9,1) = 0.0_dp
   end if
   ndata = 1
   
 else
 
   !----- Many bodies
   select case (nBODY%ifile)
   case(1) !* Near-Earth objects: see the param_file.f90 routine, fmt%file(2)
    call input_neo(ndata)
   case(2) !* Brown-type distribution objects: see the param_file.f90 routine, fmt%file(3)
    call input_bro(ndata)
   case default !* User provided file that complies with the format of fmt%file(5) in the param_file.f90 routine
                ! Example: ../data/BROsconvsmallTenStrengthVatm.dat
                ! Example: ./data/MarsEntry.dat (default)
    call input_user(ndata)
   end select
 end if

! if (rang .eq.0)
 if (iappend .eq. 0) then ! useless if we just read the data
 do i = 1,ndata
   select case(nbeqn)
   case(7)
     write(num%file(4),fmt%file(4)) i,data(0:1,i),data(3:4,i)/1000._dp,data(5,i)*RAD2DEG,data(6,i)/1000._dp,data(7,i),data(8,i),data(9,i)
   case(8)
     write(num%file(4),fmt%file(4)) i,data(0:1,i),data(3:4,i)/1000._dp,data(5,i)*RAD2DEG,data(6,i)/1000._dp,data(7,i),data(8,i),data(9,i)
   end select
 end do
 end if

 end subroutine input_all

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                          NEOs DATA CONVERSION                          ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !**                                                                        **!
 !**  ##  Meteoroid trajectory :                                            **!
 !**                                                                        **!
 !** a   : 1/2 grand-axe                                             [u.a.] **!
 !** exc : excentricité                                                     **!
 !** inc : inclinaison                                                [deg] **!
 !** GOM : longitude du noeud ascendant                               [deg] **!
 !** pom : argument du périastre                                      [deg] **!
 !** M   : anomalie moyenne                                           [deg] **!
 !** H   : magnitude absolue                                                **!
 !**                                                                        **!
 !**  ##  Meteoroid parameters :                                            **!
 !**                                                                        **!
 !** mm  : masse du meteoroide                                         [kg] **!
 !** vm  : vitesse du meteoroide                                      [m/s] **!
 !** hm  : altitude du meteoroide                                       [m] **!
 !** Om  : angle zenithal du meteoroide                               [rad] **!
 !** lm  : distance le long de la trajectoire                           [m] **!
 !** Ts  : temperature de surface du meteoroide                         [K] **!
 !**                                                                        **!
 !****************************************************************************!

 subroutine input_neo(ndata)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 integer(kind=SP), intent(INOUT) :: ndata
 !DVA Variables
 !* file
 integer(kind=SP) :: ios
 character(len=80) :: commen
 !* input data 
 real(kind=DP), dimension(1:6) :: kepler
 integer(kind=SP) :: id 
 real(kind=DP)    :: H
 !*
 real(kind=DP) :: tm,mm,rm,vm,hm,Om,lm,Xm,Ym,Ts
 real(kind=DP) :: Im
 !* radiant
 character(len=1) :: methode
 real(kind=DP)    :: ADm,DECm
 real(kind=DP)    :: date
 !* iteration
 integer(kind=SP) :: k,i,count
 !*
 ! 0 1 2 3 4 5 6 7 8 9
 ! t m r v h o l x y T

 !-------------------------------- BEGINNING! --------------------------------!

 call open_file('r',1,(/2/))
 
 !* header   
 do k = 1,19
   read(num%file(02),*)
 end do
 !* date
 read(num%file(02),'(A12,F9.1)') commen(1:12), date

 do k = 1,5
   read(num%file(02),*)
 end do

 ! initialize random seed
 call system_clock (count)
 call random_seed(count)

 ndata=0
 ios = 0
 do while (ios .eq. 0) 
   !* Reading data
   read(num%file(02),fmt%file(02),iostat=ios) id,kepler(1:6),H
   if (ios .eq. 0) then
      ndata=ndata+1
      !* Data conversion
      !- Radius [m]
      rm = (664.5_dp/sqrt(am/100._dp)) * 10._dp ** (-0.2_dp*H+3)     
      !- Mass [kg]
      mm = dm * (4._dp/3._dp) * PI * rm**3
      !- Altitude [km]
      hm = 120.0_dp
      !- Zenithal angle (via Impact angle)
      call random_number(Im) ! [0,1[
      Im = 1._dp-Im          ! ]0,1]   
      Om = PI/2._dp-acos(Im) ! [rad]
      !- Distance along trajectory [km]
      lm = 0._dp 
      !- Surface temperature [K]
      Ts = 200._dp
      !* Velocity [m/s]   
      call RADnum(date,kepler(1:6),ADm,DECm,vm)   
      
      ! t,m,r,v,h,o,l,x,y,[T]
      data(0,ndata) = 0.0d0
      data(1,ndata) = mm            ! [kg]
      data(2,ndata) = rm            ! [m]
      data(3,ndata) = vm * 1000._dp ! [m/s]
      data(4,ndata) = hm * 1000._dp ! [  m]
      data(5,ndata) = Om            ! [rad]
      data(6,ndata) = lm * 1000._dp ! [  m]
      data(7,ndata) = 0.0_dp         ! X [m]
      data(8,ndata) = 0.0_dp         ! Y [m]
      if (nbeqn==8) then
         data(9,ndata) = Ts0
      else
         data(9,ndata) = 0.0_dp
      end if
   end if
 end do 
 call close_file(1,(/2/))

 end subroutine input_neo

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                      Brown et al. DATA CONVERSION                      ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine input_bro(ndata)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 integer(kind=SP), intent(INOUT) :: ndata
 !DVA Variables
 !* file
 integer(kind=SP) :: ios
 !*
 real(kind=DP) :: tm,mm,rm,vm,hm,Om,lm,Ts,coef
 !* iteration
 integer(kind=SP) :: k
 !*
 real(kind=DP), dimension(1:10) :: y0

 !-------------------------------- BEGINNING! --------------------------------!

 call open_file('r',1,(/3/))
 
 !* header   
 do k = 1,3
   read(num%file(3),*)
 end do
 
 ndata=0
 ios = 0
 allocate(nBODY%coef(maxdim))
 do while (ios .eq. 0)
   !* Reading data
   read(num%file(03),fmt%file(03),iostat=ios) tm,mm,rm,vm,hm,Om,lm,Ts,coef

   if (ios .eq. 0) then
    ndata=ndata+1
    ! t,m,r,v,h,o,l,x,y,[T]
    data(0,ndata) = tm
    data(1,ndata) = mm            ! [kg]
    data(2,ndata) = rm            ! [m]
    data(3,ndata) = vm * 1000._dp ! [m/s]
    data(4,ndata) = hm * 1000._dp ! [  m]
    data(5,ndata) = Om * DEG2RAD  ! [rad]
    data(6,ndata) = lm * 1000._dp ! [  m]
    data(7,ndata) = 0.0_dp         ! X [m]
    data(8,ndata) = 0.0_dp         ! Y [m]
    if (nbeqn==8) then
        data(9,ndata) = Ts0
    else
        data(9,ndata) = 0.0_dp
    end if
    nBODY%coef(ndata)     = coef
   end if   
 end do 

 call close_file(1,(/3/))

 end subroutine input_bro
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                      Brown et al. DATA CONVERSION                      ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine input_user(ndata)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 integer(kind=SP), intent(INOUT) :: ndata
 !DVA Variables
 !* file
 integer(kind=SP) :: ios
 !*
 real(kind=DP) :: tm,mm,rm,vm,hm,Om,lm,Xm,Ym,Ts,coef
 !* iteration
 integer(kind=SP) :: k
 !*
 real(kind=DP), dimension(1:10) :: y0

 !-------------------------------- BEGINNING! --------------------------------!

 call open_file('r',1,(/5/))
 
 !* header   
 do k = 1,3
   read(num%file(5),*)
 end do
 
 ndata=0
 ios = 0
 allocate(nBODY%coef(maxdim))
 do while (ios .eq. 0)
   !* Reading data
   read(num%file(5),fmt%file(5),iostat=ios) tm,mm,rm,vm,hm,Om,lm,Ts,coef
   if (ios .eq. 0) then
    ! t,m,r,v,h,o,l,x,y,[T]
    data(0,ndata) = tm
    data(1,ndata) = mm            ! [kg]
    data(2,ndata) = rm            ! [m]
    data(3,ndata) = vm * 1000._dp ! [m/s]
    data(4,ndata) = hm * 1000._dp ! [  m]
    data(5,ndata) = Om * DEG2RAD  ! [rad]
    data(6,ndata) = lm * 1000._dp ! [  m]
    data(7,ndata) = 0.0_dp         ! X [m]
    data(8,ndata) = 0.0_dp         ! Y [m]
    if (nbeqn==8) then
        data(9,ndata) = Ts0
    else
        data(9,ndata) = 0.0_dp
    end if
    nBODY%coef(ndata)     = coef
   end if
 end do 

 call close_file(1,(/3/))

 end subroutine input_user
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                      RESUME WORK AFTER A BREAK                         ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine input_resume(time,y,dy,nfrag)
 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 real(kind=DP), intent(out) :: time
 real(kind=DP),dimension(:,:),allocatable,intent(out) :: y,dy
! real(kind=DP),dimension(:,:),intent(inout) :: y,dy
 integer(kind=DP), intent(inout) :: nfrag
 !DVA Variables
 !* file
 integer(kind=SP) :: ios
 !*
 real(kind=DP) :: tt,mm,rm,vm,hm,Om,lm,Xm,Ym,Tm,dmm,drm,dvm,dhm,dOm,dlm,dXm,dYm,dTm
 integer(kind=DP) :: id
 !* iteration
 integer(kind=DP) :: ndata
 !*
 real(kind=DP), dimension(1:8) :: y0
 real(kind=DP), dimension(1:8) :: dy0

 if (.not. allocated(y)) allocate(  y(1:nbeqn,1:maxdim)) ! double check maxdim!!!
 if (.not. allocated(dy)) allocate(dy(1:nbeqn,1:maxdim)) ! double check maxdim!!!

 print*,'opening file=',loc%file(18)
 open(unit=num%file(18),file=loc%file(18),iostat=ios)
 ios = 0
 ndata = 0
 do while (ios .eq. 0)
   !* Reading data
   read(num%file(18),fmt%file(18),iostat=ios), id,tt,mm,rm,vm,hm,Om,lm,Xm,Ym,Tm,dmm,drm,dvm,dhm,dOm,dlm,dXm,dYm,dTm
   if (ios .eq. 0) then
    ndata = ndata+1
    time = tt
    y(1,id) = mm
    y(2,id) = vm * 1000._dp ! [m/s]
    y(3,id) = hm * 1000._dp ! [  m]
    y(4,id) = Om * DEG2RAD  ! [rad]
    y(5,id) = lm * 1000._dp ! [  m]
    y(6,id) = Xm * 1000._dp ! [  m]
    y(7,id) = Ym * 1000._dp ! [  m]
    y(8,id) = Tm            ! [  K]
   dy(1,id) = dmm            ! [ kg]
   dy(2,id) = dvm * 1000._dp ! [m/s]
   dy(3,id) = dhm * 1000._dp ! [  m]
   dy(4,id) = dOm * DEG2RAD  ! [rad]
   dy(5,id) = dlm * 1000._dp ! [  m]
   dy(6,id) = dXm * 1000._dp ! [  m]
   dy(7,id) = dYm * 1000._dp ! [  m]
   dy(8,id) = dTm            ! [  K]
    if (ndata .eq. 1)  nfrag=id
   end if
 end do
 close(unit=num%file(18))
 
 
 end subroutine input_resume
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE INPUT
