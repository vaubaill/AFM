 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE RKF

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~        OPTIMIZED RUNGE-KUTTA-FEHLBERG 4(5) NUMERICAL INTEGRATION       ~~! 
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE VARGLOBAL
 USE AFM_MOD

 !FON Declaration of global variables 
 implicit none

 !* Coefficients Runge-Kutta 4 (function)
 real(kind=DP), dimension(8,6), parameter :: cRK = &
 (/              0._dp,     1._dp/4._dp    ,     3._dp/32._dp   , & 
  &  1932._dp/2197._dp,   439._dp/216._dp  ,    -8._dp/27._dp   , &
  &    25._dp/216._dp ,    16._dp/135._dp  ,               0._dp, &
  &              0._dp,     9._dp/32._dp   , -7200._dp/2197._dp , &
  &             -8._dp,               2._dp,               0._dp, &
  &              0._dp,               0._dp,               0._dp, &
  &              0._dp,  7296._dp/2197._dp ,  3680._dp/513._dp  , &
  & -3544._dp/2565._dp,  1408._dp/2565._dp ,  6656._dp/12825._dp, &
  &              0._dp,               0._dp,               0._dp, &
  &              0._dp,  -845._dp/4104._dp ,  1859._dp/4104._dp , &
  &  2197._dp/4104._dp, 28561._dp/56430._dp,               0._dp, &
  &              0._dp,               0._dp,               0._dp, &
  &              0._dp,   -11._dp/40._dp   ,    -1._dp/5._dp    , &
  &   -9._dp/50._dp   ,               0._dp,               0._dp, &
  &              0._dp,               0._dp,               0._dp, &
  &              0._dp,               0._dp,     2._dp/55._dp     /)

 !* Coefficients Runge-Kutta 4 (time)
 real(kind=DP), dimension(6), parameter   :: cT = &
 (/ 0._dp, 1._dp/4._dp, 3._dp/8._dp, 12._dp/13._dp, 1._dp, 1._dp/2._dp /)

 !FON End of global variables declaration

 CONTAINS

 !----------------------------------------------------------------------------!
 !--            Listing of subroutines available in this modules            --!
 !----------------------------------------------------------------------------!
 !--                                                                        --!
 !--  RKFcontrol                                                            --!
 !--  RKF45                                                                 --!
 !--                                                                        --!
 !----------------------------------------------------------------------------!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                NUMERICAL INTEGRATION (WITH STEP CONTROL)               ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !**                                                                        **!
 !** VAR : - topt : optimized step control                              [s] **!
 !**       - y    : optimized state vector                                  **!
 !**                                                                        **!
 !****************************************************************************!

 subroutine RKFcontrol(topt,y,atm)

 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O
 real(kind=DP), intent(INOUT) :: topt
 real(kind=DP), dimension(0:nbeqn), intent(INOUT) :: y
 real(kind=DP), dimension(1:3), intent(  OUT) :: atm
 !DVA Variables
 !* test
 real(kind=DP) :: eps 
 real(kind=DP) :: d 
 !* 
 real(kind=DP) :: h
 real(kind=DP), dimension(0:nbeqn) :: yref 

 !-------------------------------- BEGINNING! --------------------------------!

 yref(0:nbeqn) = y(0:nbeqn)

 h = topt
 call RKF45(h,y(0:nbeqn),eps,atm)

 do while (eps .gt. epstol) 
   y(0:nbeqn) = yref(0:nbeqn)
   d = 0.840896_dp * (epstol/eps)**(1._dp/4._dp)
   h = d*h
   call RKF45(h,y(0:nbeqn),eps,atm)
 end do

 topt = h
 y(0) = y(0)+topt
 
 end subroutine RKFcontrol

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                      NUMERICAL INTEGRATION (BASIC)                     ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !**                                                                        **!
 !** VAR : - h   : integration step                                     [s] **!
 !**       - y   : state vector at t time                                   **!
 !**       - eps : error between 4th and 5th order approximation            **! 
 !**                                                                        **!
 !****************************************************************************!

 subroutine RKF45(h,y,eps,atm)

 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O
 real(kind=DP), intent( IN) :: h
 real(kind=DP), dimension(0:nbeqn), intent(INOUT) :: y
 real(kind=DP), intent(OUT) :: eps
 real(kind=DP), dimension(1:3), intent(OUT) :: atm 
 !DVA Variables
 real(kind=DP), dimension(1:nbeqn) :: z
 real(kind=DP), dimension(1:nbeqn,6) :: k 
 real(kind=DP), dimension(1:nbeqn) :: vecR 
 !* Intermediate variables
 real(kind=DP), dimension(0:nbeqn) :: ytmp
 real(kind=DP) :: ttmp
 
 !-------------------------------- BEGINNING! --------------------------------!

 ytmp(0)       = y(0)
 ytmp(1:nbeqn) = y(1:nbeqn) 
 call AFM_abl(ytmp(0:nbeqn),k(1:nbeqn,1),atm) 

 ytmp(0)       = y(0)+cT(2)*h
 ytmp(1:nbeqn) = y(1:nbeqn) + cRK(2,1)*h*k(1:nbeqn,1)
 call AFM_abl(ytmp(0:nbeqn),k(1:nbeqn,2),atm)

 ytmp(0)       = y(0)+cT(3)*h
 ytmp(1:nbeqn) = y(1:nbeqn) + cRK(3,1)*h*k(1:nbeqn,1) &
                            + cRK(3,2)*h*k(1:nbeqn,2)
 call AFM_abl(ytmp(0:nbeqn),k(1:nbeqn,3),atm)

 ytmp(0)       = y(0)+cT(4)*h
 ytmp(1:nbeqn) = y(1:nbeqn) + cRK(4,1)*h*k(1:nbeqn,1) &
                            + cRK(4,2)*h*k(1:nbeqn,2) &
                            + cRK(4,3)*h*k(1:nbeqn,3)
 call AFM_abl(ytmp(0:nbeqn),k(1:nbeqn,4),atm)

 ytmp(0)       = y(0)+h
 ytmp(1:nbeqn) = y(1:nbeqn) + cRK(5,1)*h*k(1:nbeqn,1) &
                            + cRK(5,2)*h*k(1:nbeqn,2) &
                            + cRK(5,3)*h*k(1:nbeqn,3) &
                            + cRK(5,4)*h*k(1:nbeqn,4) 
 call AFM_abl(ytmp(0:nbeqn),k(1:nbeqn,5),atm)

 ytmp(0)       = y(0)+cT(6)*h
 ytmp(1:nbeqn) = y(1:nbeqn) + cRK(6,1)*h*k(1:nbeqn,1) &
                            + cRK(6,2)*h*k(1:nbeqn,2) &
                            + cRK(6,3)*h*k(1:nbeqn,3) &
                            + cRK(6,4)*h*k(1:nbeqn,4) &
                            + cRK(6,5)*h*k(1:nbeqn,5)
 call AFM_abl(ytmp(0:nbeqn),k(1:nbeqn,6),atm) 

 !* Runge-Kutta 5th and 4th order
 z(1:nbeqn)    = y(1:nbeqn) + cRK(8,1)*h*k(1:nbeqn,1) &
                            + cRK(8,3)*h*k(1:nbeqn,3) &
                            + cRK(8,4)*h*k(1:nbeqn,4) &
                            + cRK(8,5)*h*k(1:nbeqn,5) &
                            + cRK(8,6)*h*k(1:nbeqn,6)

 y(1:nbeqn)    = y(1:nbeqn) + cRK(7,1)*h*k(1:nbeqn,1) &
                            + cRK(7,3)*h*k(1:nbeqn,3) &
                            + cRK(7,4)*h*k(1:nbeqn,4) &
                            + cRK(7,5)*h*k(1:nbeqn,5)

 !* error
 vecR(1:nbeqn) = abs( z(1:nbeqn)-y(1:nbeqn) ) / h

 !* L2 norm
 eps = sqrt( dot_product( vecR(1:nbeqn),vecR(1:nbeqn) ) ) 
 
 end subroutine RKF45 

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE RKF
