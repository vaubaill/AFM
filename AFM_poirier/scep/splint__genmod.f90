        !COMPILER-GENERATED INTERFACE MODULE: Wed May  2 13:55:17 2018
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE SPLINT__genmod
          INTERFACE 
            SUBROUTINE SPLINT(INDEX,X,VAL_OUT)
              USE VARGLOBAL, ONLY :                                     &
     &          DATA_POINTS,                                            &
     &          SPLINE_DATA,                                            &
     &          SPLINE_DATA_PP
              INTEGER(KIND=4), INTENT(IN) :: INDEX
              REAL(KIND=4), INTENT(IN) :: X
              REAL(KIND=4), INTENT(OUT) :: VAL_OUT
            END SUBROUTINE SPLINT
          END INTERFACE 
        END MODULE SPLINT__genmod
