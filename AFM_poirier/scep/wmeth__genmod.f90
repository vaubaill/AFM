        !COMPILER-GENERATED INTERFACE MODULE: Fri May  4 10:30:56 2018
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE WMETH__genmod
          INTERFACE 
            SUBROUTINE WMETH(TMIN,TMAX,EA,R1,EB1,DD1,R2,EB2,DD2,ERR)
              REAL(KIND=8) :: TMIN
              REAL(KIND=8) :: TMAX
              REAL(KIND=8) :: EA(6)
              REAL(KIND=8) :: R1(6)
              REAL(KIND=8) :: EB1(5)
              REAL(KIND=8) :: DD1
              REAL(KIND=8) :: R2(6)
              REAL(KIND=8) :: EB2(5)
              REAL(KIND=8) :: DD2
              INTEGER(KIND=4) :: ERR
            END SUBROUTINE WMETH
          END INTERFACE 
        END MODULE WMETH__genmod
