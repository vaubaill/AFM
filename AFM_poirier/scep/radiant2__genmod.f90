        !COMPILER-GENERATED INTERFACE MODULE: Fri May  4 10:30:56 2018
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE RADIANT2__genmod
          INTERFACE 
            SUBROUTINE RADIANT2(EB,FB,EZ,FZ,RR,ERR)
              REAL(KIND=8) :: EB(5)
              REAL(KIND=8) :: FB
              REAL(KIND=8) :: EZ(5)
              REAL(KIND=8) :: FZ
              REAL(KIND=8) :: RR(6)
              INTEGER(KIND=4) :: ERR
            END SUBROUTINE RADIANT2
          END INTERFACE 
        END MODULE RADIANT2__genmod
