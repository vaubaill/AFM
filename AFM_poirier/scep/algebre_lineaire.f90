 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE ALGEBRE_LINEAIRE

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                         LINEAR ALGEBRA MODULES                         ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE VARGLOBAL
 USE MATHS

 CONTAINS

 !----------------------------------------------------------------------------!
 !--             Listing of subroutines available in this module            --!
 !----------------------------------------------------------------------------!
 !--                                                                        --!
 !--  tab_insert : insert one column by one/several column(s) in array      --!
 !--  tab_remove : remove of one column in an array                         --!
 !--                                                                        --!
 !----------------------------------------------------------------------------!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##         INSERT SOME COLUMN S IN ARRAY                                  ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************! 
 !**                                                                        **!
 !** VAR : - id_col  : column id to switch                                **!
 !**       - n_newcol : number of column to input                              **!
 !**       - newcol    : new column to input in A                               **!
 !**       - n_A  : number of column of the input/output table                **!
 !**       - A     : input/output table                                     **!
 !**                                                                        **!
 !****************************************************************************!
 !**                                         |0|         | 2  3|            **!
 !**  ##  example : switch the second column |0| by newcol = | 6  7|            **!
 !** --------------                          |0|         |10 11|            **!
 !**                                                                        **!
 !**                  | 1  0  4|                          | 1  2  3  4|     **!  
 !** (input)     Ai = | 5  0  8|   -->   (ouput)     Af = | 5  6  7  8|     **!
 !**                  | 9  0 12|                          | 9 10 11 12|     **!
 !**                                                                        **!
 !****************************************************************************!

 subroutine tab_insert(id_col,n_newcol,newcol,n_A,A)

 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O
 integer(kind=DP), intent( IN) :: id_col
 integer(kind=DP), intent( IN) :: n_newcol
 real(kind=DP)   , dimension(1:nbeqn,n_newcol)  , intent( IN) :: newcol
 integer(kind=DP), intent(INOUT) :: n_A
! real(kind=DP)   , dimension(1:nbeqn,1:maxdim), intent(INOUT) :: A
 real(kind=DP)   , dimension(:,:), allocatable, intent(INOUT) :: A
 !DVA Variables
 integer(kind=DP) :: ci, cf, szeqn,n_Aout
 real(kind=DP)   , dimension(:,:), allocatable :: Aout

 !--------------------------------- BEGINNING --------------------------------!

 if (id_col .le. 0 .and. id_col .gt. n_A) then 
   write(*,*)"Error : column number > matrix size"
   stop
 end if
 
 szeqn=size(A,1)
 n_Aout=n_A-1+n_newcol
 if (.not. allocated(Aout)) allocate( Aout(szeqn,1:n_Aout))

 !* parametres
 ci = id_col-1
 cf = id_col-1+n_newcol

 select case(ci)
 case(0)  ! ci=0 ; cf=n_newcol
  Aout(:,1:cf) = newcol(:,1:n_newcol)
  Aout(:,cf+1:n_A-1+n_newcol) = A(:,id_col+1:n_A)
 case default  ! ci=id_col-1 ; cf=ci+n_newcol
  Aout(:,1:ci) = A(:,1:ci)
  Aout(:,id_col:cf) = newcol(:,1:n_newcol)
  Aout(:,cf+1:n_A-1+n_newcol) = A(:,id_col+1:n_A)
 end select
 
 call MOVE_ALLOC(Aout,A) 
 
 end subroutine tab_insert

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                    REMOVE OF ONE COLUMN IN AN ARRAY                    ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !** VAR : - id_col : column id to delete                                     **!
 !**       - n_A : number of column of the input/output table              **!
 !**       - A    : input/output table                                      **!   
 !**                                                                        **!
 !****************************************************************************!
 !**                                         |0|                            **!
 !**  ##  example : remove the second column |0| (id_col = 2)                 **!
 !** --------------                          |0|                            **!
 !**                                                                        **!
 !**                  | 1  0  2|                            | 1  2|         **!  
 !** (input)     Ai = | 3  0  4|    -->    (ouput)     Af = | 3  4|         **!
 !**                  | 5  0  6|                            | 5  6|         **!
 !**                                                                        **!
 !**                                                                        **!
 !****************************************************************************!

 subroutine tab_remove(id_col,n_A,A,debug)

 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O
 integer(kind=DP), intent(   IN) :: id_col
 integer(kind=DP), intent(   IN) :: debug
 integer(kind=DP), intent(INOUT) :: n_A
! real(kind=DP)   , dimension(1:nbeqn,1:nbfra), intent(INOUT) :: A 
 real(kind=DP)   , dimension(:,:), allocatable :: A
 !DVA Variables 
 !* Column management
 integer(kind=DP) :: ci,cf,n_Af,szeqn,n_Aout
 !* Intermediate table 
 real(kind=DP)   , dimension(:,:), allocatable :: Aout

 !--------------------------------- BEGINNING --------------------------------!

 if (id_col .le. 0 .and. id_col .gt. n_A) then 
   write(*,*)"Error : column number > matrix size"
   stop
 end if

 szeqn=size(A,1)
 n_Aout=n_A-1
 if (.not. allocated(Aout)) allocate( Aout(szeqn,1:n_Aout))
 
 if (debug .eq. 1) then
    print*,'n_A=',n_A
    print*,'id_col=',id_col
    print*,'n_Aout=',n_Aout
 end if
 
 select case(n_A)
 case(1)
   print*,'*** Warning: impossible to remove a 1-element array'
 case(2)
   select case(id_col)
   case(1)
      Aout(:,1:1) = A(:,1:1)
   case(2)
      Aout(:,1:1) = A(:,2:2)
   end select
 case default
   select case(id_col)
   case(1)
      ci = 2
      cf = n_A
      Aout(:,1:n_Aout) = A(:,2:n_A)
   case default
      if (id_col .eq. n_A) then
         ci = id_col-1
         cf = n_A-1
         Aout(:,1:ci) = A(:,1:ci)
         Aout(:,id_col-1:n_Aout) = A(:,cf:n_A)
      else
         ci = id_col-1
         cf = id_col+1
         Aout(:,1:ci) = A(:,1:ci)
         Aout(:,id_col:n_Aout) = A(:,cf:n_A)
      end if
   end select
   if (debug==1) then
      print*,'A=',A
      print*,'id_col=',id_col
      print*,'A(:,id_col)=',A(:,id_col)
      print*,'ci=',ci
      print*,'cf=',cf
      print*,'Aout(:,1:',ci,' = A(:,1:',ci,')'
      print*,'Aout(:,',id_col,':',n_Aout,'=A(:,',cf,':',n_A,')'
      print*,'Aout=',Aout
      print*,'A=',A
   end if
 end select
 
 call MOVE_ALLOC(Aout,A)
 
 end subroutine tab_remove
 
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE ALGEBRE_LINEAIRE
