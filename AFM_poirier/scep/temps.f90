 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE TEMPS

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                              TIME SETTINGS                             ~~! 
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 implicit none

 real(kind=DP), parameter :: JD2000= 2451545.0_dp

 CONTAINS

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##            CONVERTING FROM ......,...s TO ..d..h..m..,...s             ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine conv_dhms(sec,d,h,m,s)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 real(kind=DP)   , intent( IN) :: sec
 integer(kind=SP), intent(OUT) :: d,h,m
 real(kind=DP)   , intent(OUT) :: s
 !DVA Variables
 real(kind=DP) :: r1,r2 

 !-------------------------------- BEGINNING! --------------------------------!

 !* days
 r1 = mod(sec,86400._dp)
 d  = int((sec-r1)/86400._dp,kind=SP) 

 !* hours
 r2 = mod(r1,3600._dp)
 h  = int((r1-r2)/3600._dp,kind=SP)

 !* seconds
 s  = mod(r2,60._dp)

 !* minutes
 m  = int((r2-s)/60._dp,kind=SP)

 end subroutine conv_dhms

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE TEMPS
