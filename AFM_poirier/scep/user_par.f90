 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE USER_PAR

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                  TAKE IN ACCOUNT OF USER'S PARAMETERS                  ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE PARAM_FILE
 USE PARMODEL
 USE VARGLOBAL
 USE MATHS
 USE MANAGE_FILE
 USE CHAROPS
 USE MULTICHAR

 CONTAINS

 !----------------------------------------------------------------------------!
 !--             Listing of subroutines availbale in this module            --!
 !----------------------------------------------------------------------------!
 !--                                                                        --!
 !--  read_par  : reading of user file                                      --!
 !--  conv_par  : data conversion into S.I. units                           --!
 !--  check_par : check of user file's parameters                           --!
 !--                                                                        --!
 !----------------------------------------------------------------------------!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                          READING OF USER FILE                          ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine read_par()

 !FON Declaration des variables locales
 implicit none
 !DVA Variables
 !* Iteration
 integer(kind=SP)  :: i,imax
 !* File management
 integer(kind=SP)  :: err
 !*
 integer(kind=SP)  :: itest
 character(len=14) :: ntest
 character(len=128) :: ident, ident_ref, line, value, param, comm
 integer :: poseg, posdp, pospi, finchaine
 real(kind=DP),dimension(:),allocatable :: tmpMref,tmpPref

 !--------------------------------- BEGINNING --------------------------------!
 ! initialisations
 defoutdir='../user/out/'   ! default output directory
 outfps=9999.0              ! data output frequency [/s] ; if =9999.0 then output is provided at every integration step
 iappend=0                  ! do not append results
 iresume=0                  ! do not resume the computation
 imodel=3                   ! Borovicka (2007)
 Q_Boro=2.0D+07             ! value of Qheat in Borovicka et al 2007.
 tpas=1.0D-03               ! step [s]
 epstol=1.00000E-06         ! Tolerance
 t_init=0.0d0               ! initial time [s]
 hstop=1.60000E+01          ! height cut off limit [km]
 mstop=1.00000E-04          ! mass cut off limit [kg]
 tstop=1.00000E-16          ! step size cut off limit [s]
 vstop=1.00000E-14          ! velocity cut off limit  [m/s]
 iintegrator=2              ! integrator type. 1=RK4 (do not use), 2=BS (recommended)
 ibodies=1                  ! One body by default. if=2: multiple bodies
 vm0=2.00000E+01            ! vm0 : initial value of speed [km/s] -OK-
 hm0=1.20000E+02            ! hm0 : initial value of altitude [km] -OK-
 Om0=4.50000E+01            ! Om0 : initial value for zenithal angle [deg] -OK-
 lm0=0.00000E+00            ! lm0 : distance along trajectory [km] -OK-
 Ts0=2.50000E+02            ! Ts0 : initial value of surface temperature [K] -OK-
 mm0=1.00000E+10            ! mm0 : initial value of mass [kg] -OK-
 imm=0                      ! mm0 radius is computed from mass (0/1)
 rm0=1.00000E-02            ! rm0 : initial value of radius [m] -OK-
 irm=0                      ! mass is computed from radius (0/1)
 nBODY%ifile=2             ! choice of file. 1=NEO 2=Brown et al
 ibodies = 1                ! number of bodies to consider
 nBODY%irestrict=0          ! restrict the body id (0/1, if multi-bodies)
 nBODY%ibeg=1_dp            ! body id to begin with (if multi-bodies)
 nBODY%iend=1_dp            ! body id to end with (if multi-bodies)
 imaterial=1                ! meteoroid material. 1=Rock, 2=Water Ice, 3=Iron
 ishape=1                   ! meteoroid shape. 1=spherical, 2=other - not taken into account yet -
 iplanet=1                  ! Planet: 1=Earthm 2=Mars
 iatmos=1                   ! Atmosphere model. 1=Hybrid, 2=exponential, 3=interpolation from tabulated values  4=interpolation from tabulated values provided by LMD
 fra%i=1                    ! tae into account the fragmentation (0/1)
 num_fra=0.60_dp            ! Fragmentation parameter ]0;1[
 inum_fra=0                 ! Distribution of fragmentation parameter (0/1)
 StrengthMod='Popova2011'          ! Fragmentation method
 
 
 print*,'Now reading configuration file: ',loc%file(0)
 open(unit=num%file(0),file=loc%file(0),iostat=err)
 do
  read (num%file(0), '(A)', iostat=err) line
  if (err== -1) exit
  if (err == 0 .and. line(1:1) /= '#') then
    posdp = scan (line, ':')                 ! position of the ':' character
    param = line(:posdp-1)                   ! parameter name
    pospi = scan (line, '#')                 ! position of the '#' character
    finchaine = len_trim(line)               ! finchaine=end of line (if not '#')
    if (pospi /= 0) finchaine = pospi - 1    ! position of the value of the parameter
    value = adjustL(line(posdp+1:finchaine)) ! value of the parameter
    select case (trim(param))
       case ('defoutdir')
        defoutdir=trim(value)
       case ('outfps')
        outfps=ctor(value)
       case ('iappend')
        iappend=ctoi(value)
       case ('iresume')
        iresume=ctoi(value)
       case ('imodel')
        imodel=ctoi(value)
       case ('Q_Boro')
        Q_Boro=ctor(value)
       case ('step')
        tpas=ctor(value)
       case ('epstol')
        epstol=ctor(value)
       case ('tmin','t_init')
        t_init=ctor(value)
       case ('hstop')
        hstop=ctor(value)
       case ('mstop')
        mstop=ctor(value)
       case ('tstop')
        tstop=ctor(value)
       case ('vstop')
        vstop=ctor(value)
       case ('iintegrator')
        iintegrator=ctoi(value)
       case ('ibodies')
        ibodies=ctoi(value)
       case ('vm0')
        vm0=ctor(value)
       case ('hm0')
        hm0=ctor(value)
       case ('Om0')
        Om0=ctor(value)
       case ('lm0')
        lm0=ctor(value)
       case ('Ts0')
        Ts0=ctor(value)
       case ('mm0')
        mm0=ctor(value)
        imm=1
       case ('rm0')
        rm0=ctor(value)
        irm=1
       case ('ifile')
        nBODY%ifile=ctoi(value)
       case ('datafile')
        loc%file(5)='../data/'//trim(value)
        print*,'loc%file(5)=',loc%file(5)
       case ('irestrict')
        nBODY%irestrict=ctoi(value)
       case ('ibeg')
        nBODY%ibeg=ctoi(value)
       case ('iend')
        nBODY%iend=ctoi(value)
       case ('imaterial')
        imaterial=ctoi(value)
       case ('ishape')
        ishape=ctoi(value)
       case ('iplanet')
        iplanet=ctoi(value)
       case ('iatmos')
        iatmos=ctoi(value)
       case ('ifra')
        fra%i=ctoi(value)
       case ('num_fra')
        num_fra=ctor(value)
       case ('inum_fra')
        inum_fra=ctoi(value)
       case ('StrengthMod')
        StrengthMod=trim(value)
       case ('Mref')
        call multival(trim(value),tmpMref,tsl_str_intp%n) ! will be saved in tsl_str%Mref if StrengthMod='user'
       case ('Pref')
        call multival(trim(value),tmpPref,tsl_str_intp%n) ! will be saved in tsl_str%Mref if StrengthMod='user'
       case ('coef') ! useless if StrengthMod='interp'
        tsl_str%coef=ctor(value)
       end select
  else if (line(1:1) == '#') then ! if (err == 0 .and. line(1:1) /= '#') then
        cycle
       else
         exit
       end if
 end do

if (iresume==1) iappend=.true.

! set the tensile Strength paramters depending on the method
select case(StrengthMod)
 case('Popova2011','Popova')
  tsl_str%Mref=tsl_strPopova2011(3)
  tsl_str%Pref=tsl_strPopova2011(2)
  tsl_str%coef=tsl_strPopova2011(1)
 case('Ferrier2012','Ferrier')
  tsl_str%Mref=tsl_strFerrier2012(3)
  tsl_str%Pref=tsl_strFerrier2012(2)
  tsl_str%coef=tsl_strFerrier2012(1)
 case('user','USER')
  tsl_str%Mref=tmpMref(1)
  tsl_str%Pref=tmpPref(1)
 !tsl_str%coef is already set
 case('interp')
  allocate(tsl_str_intp%Mref(1:tsl_str_intp%n))
  allocate(tsl_str_intp%Pref(1:tsl_str_intp%n))
  allocate(tsl_str_intp%coef(1:tsl_str_intp%n,3))
  tsl_str_intp%Mref(1:tsl_str_intp%n)=log10(tmpMref(1:tsl_str_intp%n))
  tsl_str_intp%Pref(1:tsl_str_intp%n)=log10(tmpPref(1:tsl_str_intp%n))
 case default
  print*,'*** FATAL ERROR: unknown StrengthMod=',StrengthMod
  stop
end select

if (t_p==0) then
print*,'StrengthMod=',StrengthMod
print*,'tsl_str%Mref=',tsl_str%Mref
print*,'tsl_str%Pref=',tsl_str%Pref
print*,'tsl_str%coef=',tsl_str%coef
print*,'tsl_str_intp%n=',tsl_str_intp%n
if (allocated(tsl_str_intp%Mref)) print*,'tsl_str_intp%Mref(1:tsl_str_intp%n)=',tsl_str_intp%Mref(1:tsl_str_intp%n)
if (allocated(tsl_str_intp%Pref)) print*,'tsl_str_intp%Pref(1:tsl_str_intp%n)=',tsl_str_intp%Pref(1:tsl_str_intp%n)
end if ! end of if (t_p==0) then

! free the memory
deallocate(tmpMref)
deallocate(tmpPref)
! print for checking purpose
if (t_p==0) then
print*,'(user_par) defoutdir=',trim(defoutdir)
print*,'(user_par) outfps=',outfps
print*,'(user_par) iappend=',iappend
print*,'(user_par) imodel=',imodel
print*,'(user_par) tpas=',tpas
print*,'(user_par) epstol=',epstol
print*,'(user_par) t_init=',t_init
print*,'(user_par) hstop=',hstop
print*,'(user_par) mstop=',mstop
print*,'(user_par) tstop=',tstop
print*,'(user_par) vstop=',vstop
print*,'(user_par) iintegrator=',iintegrator
print*,'(user_par) ibodies=',ibodies
print*,'(user_par) vm0=',vm0
print*,'(user_par) hm0=',hm0
print*,'(user_par) Om0=',Om0
print*,'(user_par) lm0=',lm0
print*,'(user_par) Ts0=',Ts0
print*,'(user_par) mm0=',mm0
print*,'(user_par) rm0=',rm0
print*,'(user_par) ifile=',nBODY%ifile
print*,'(user_par) loc%file(5)=',trim(loc%file(5))
print*,'(user_par) nBODY%irestrict=',nBODY%irestrict
print*,'(user_par) nBODY%ibeg=',nBODY%ibeg
print*,'(user_par) nBODY%iend=',nBODY%iend
print*,'(user_par) imaterial=',imaterial
print*,'(user_par) ishape=',ishape
print*,'(user_par) iplanet=',iplanet
print*,'(user_par) iatmos=',iatmos
print*,'(user_par) fra%i=',fra%i
print*,'(user_par) num_fra=',num_fra
print*,'(user_par) inum_fra=',inum_fra
end if ! end of if (t_p==0) then

 close(num%file(0))

 ! ----------------- Additional settings -------------

 !* Number of equations for the Ablation Model
 select case(imodel)
 case(1,2)
   nbeqn = 8_sp
 case(3,4)
   nbeqn = 7_sp
 case default
  print*,'*** FATAL ERROR: imodel should be equal to 1,2,3 or 4.'
  stop
 end select
if (t_p==0) then
 print*,'(user_par) ablation model #=',imodel
 print*,'(user_par) nbeqn=',nbeqn
end if ! end of if (t_p==0) then

 ! ibodies settings
 select case(ibodies)
 case(1) ! One body
  ! imm and irm
  if ((imm ==0) .and. (irm==0)) then
    print*,'*** FATAL ERROR: IMM=0 AND IRM=0: MAKE A DECISION. (CHECK ibodies, mm0 and rm0)'
    stop
  end if
  nBODY%ibeg=1_dp
  nBODY%iend=1_dp
  nBODY%irestrict=0
 end select
 
 ! ifile setting
 select case (nBODY%ifile)
 case(1)
 case(2)
 case(3)
 case default
  print*,'*** FATAL ERROR: ifile should be equal to 1 (=NEO) or 2 (=Brown distribution) or 3 (Mars, Vaubaillon).'
  stop
 end select
 
 select case (ishape)
 case(1)
 case default
  print*,'*** FATAL ERROR: ishape should be equal to 1 (=spherical model).'
  stop
 end select
 
 ! now reads the atmosphere file
 select case (iplanet)
 case(1) ! Earth
  loc%file(6)="../data/Earth_atmos.txt" ! atmosphere file
  print*,'now reading: ',loc%file(6)
  open(unit=num%file(6),file=loc%file(6),iostat=err)
  read(num%file(6), *, iostat=err) atm_n
  allocate(atm_hpdt   (atm_n,3))
  allocate(atm_hpdt_pp(atm_n,2))
  do i = 1, atm_n
   read(num%file(6), *, iostat=err) atm_hpdt(i,1), atm_hpdt(i,2), atm_hpdt(i,3)
  end do
  ! now computes the coefficients that will be used to interpolate the data
  call spline_jma (atm_hpdt(:,1), atm_hpdt(:,2), 1.0E-05, 1.0E-05, atm_hpdt_pp(:,1) )
  call spline_jma (atm_hpdt(:,1), atm_hpdt(:,3), 1.0E-05, 1.0E-05, atm_hpdt_pp(:,2) )
 case(2) ! Mars
  select case(iatmos)
   case(3)
    loc%file(6)="../data/Mars_atmos.txt"
    print*,'now reading: ',loc%file(6)
    open(unit=num%file(6),file=loc%file(6),iostat=err)
    read(num%file(6), *, iostat=err) atm_n
    allocate(atm_hpdt(atm_n,3))
    allocate(atm_hpdt_pp(atm_n,2))
    do i = 1, atm_n
      read(num%file(6), *, iostat=err) atm_hpdt(i,1), atm_hpdt(i,2), atm_hpdt(i,3)
    end do
    do i = 1, atm_n
    end do
    ! now computes the coefficients that will be used to interpolate the data
    call spline_jma (atm_hpdt(:,1), atm_hpdt(:,2), 1.0E-05, 1.0E-05, atm_hpdt_pp(:,1) )
    call spline_jma (atm_hpdt(:,1), atm_hpdt(:,3), 1.0E-05, 1.0E-05, atm_hpdt_pp(:,2) )
   case(4)
    loc%file(6)="../data/MarsAtmosphere0-120km.dat"
    atm_n=366
    allocate(atm_hpdt(atm_n,4))
    allocate(atm_hpdt_pp(atm_n,3))
    open(unit=num%file(6),file=loc%file(6),iostat=err)
    read(num%file(6), '(A47)', iostat=err) comm
    read(num%file(6), '(A52)', iostat=err) comm
    read(num%file(6), '(A46)', iostat=err) comm
    read(num%file(6), '(A90)', iostat=err) comm
    read(num%file(6), '(A69)', iostat=err) comm
    read(num%file(6), '(A69)', iostat=err) comm
    read(num%file(6), '(A69)', iostat=err) comm
    do i = 1, atm_n 
     read(num%file(6), '(4e15.4)', iostat=err) atm_hpdt(i,1), atm_hpdt(i,2), atm_hpdt(i,3), atm_hpdt(i,4)
    end do
    ! now computes the coefficients that will be used to interpolate the data
    allocate(atm_coef_da(atm_n,3))
    allocate(atm_coef_Pa(atm_n,3))
    allocate(atm_coef_Ta(atm_n,3))
    call spline_custom (atm_hpdt(:,1), atm_hpdt(:,2), atm_coef_Pa(:,1), atm_coef_Pa(:,2), atm_coef_Pa(:,3), atm_n)
    call spline_custom (atm_hpdt(:,1), atm_hpdt(:,3), atm_coef_da(:,1), atm_coef_da(:,2), atm_coef_da(:,3), atm_n)
    call spline_custom (atm_hpdt(:,1), atm_hpdt(:,4), atm_coef_Ta(:,1), atm_coef_Ta(:,2), atm_coef_Ta(:,3), atm_n)
  end select
 case(3) ! Venus
  atmos_data_file="../data/Venus_atmos.txt"
  read(num%file(6), *, iostat=err) atm_n
  allocate(atm_hpdt(atm_n,3))
  allocate(atm_hpdt_pp(atm_n,2))
  do i = 1, atm_n  
   read(num%file(6), *, iostat=err) atm_hpdt(i,1), atm_hpdt(i,2), atm_hpdt(i,3)
  end do
  ! now computes the coefficients that will be used to interpolate the data
  call spline_jma (atm_hpdt(:,1), atm_hpdt(:,2), 0.0_sp, 0.0_sp, atm_hpdt_pp(:,1) )
  call spline_jma (atm_hpdt(:,1), atm_hpdt(:,3), 0.0_sp, 0.0_sp, atm_hpdt_pp(:,2) )
 case default
  print*,'*** FATAL ERROR: iplanet should be equal to 1 (=Earth) or 2 =(Mars) or 3 (Venus).'
  stop
 end select
 ! now closes the atmosphere data file
 close(num%file(6))



 select case (iatmos)
 case(1,2,3,4)
 case default
  print*,'*** FATAL ERROR: iatmos should be equal to 1 (=exponential) or 2 (=hybrid) or 3 (extrapolated from tabulated values) or 4 (extrapolated from values provided by LMD).'
  stop
 end select
 
 select case (fra%i)
 case(0,1)
 case default
  print*,'*** FATAL ERROR: fra%i should be equal to 0 or 1.'
  stop
 end select
 
 select case (inum_fra)
 case(0,1)
 case default
  print*,'*** FATAL ERROR: inum_fra should be equal to 0 or 1.'
  stop
 end select
 
 
 end subroutine read_par

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                    DATA CONVERSION INTO S.I. UNITS                     ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine conv_par()

 !FON les variables definies ici sont globales

 !--------------------------------- BEGINNING --------------------------------!

 !* parametres pour l'integration numerique
 !hstop [km]  NO --> [m]   OK
 hstop = hstop * 1000._dp


 end subroutine conv_par


 
END MODULE USER_PAR
