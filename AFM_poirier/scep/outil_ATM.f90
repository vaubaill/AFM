 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE OUTIL_ATM

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                       ~~!
 !~~                    ATMOSPHERIC DENSITY MANAGEMENT                     ~~!
 !~~                                                                       ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE VARGLOBAL
 USE PHYSICS
 USE ATM_EXPO 
 USE ATM_US76

 CONTAINS

 !----------------------------------------------------------------------------!
 !--             Listing of subroutines available in this module            --!
 !----------------------------------------------------------------------------!
 !--                                                                        --!
 !--  atm_planet : atmospheric density computing                            --! 
 !--               (depending model choice of user)                         --!
 !--                                                                        --!
 !----------------------------------------------------------------------------!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                    COMPUTING OF ATMOSPHERIC DENSITY                    ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine atm_planet(hm,da,Pa,Ta)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 real(kind=DP), intent(IN   ) :: hm 
 real(kind=DP), intent(  OUT) :: da
 real(kind=DP), intent(  OUT) :: Pa
 real(kind=DP), intent(  OUT) :: Ta 
 real(kind=DP) :: hm_tmp

 !-------------------------------- BEGINNING! --------------------------------!

 hm_tmp=hm

 select case(iplanet)
 case(1) ! Earth

   select case(iatmos)
   case(1) ! NASA exponential model
      call Earth_expo(hm_tmp,da,Pa,Ta)
      Pa = 1000.0_dp * Pa
   case(2) ! Hybrid : Exp + US 1976 ! DO NOT USE!!!
      if (hm_tmp .gt. 100000._dp) then
        call Earth_expo(hm_tmp,da,Pa,Ta)
      else
       call Earth_us76(hm_tmp/1000._dp,da,Pa,Ta)
       da = da0 * da
       Pa = Pa0 * Pa
       Ta = Ta0 * Ta
      end if
   case(3) ! interpolation from tabulated values
      call splint ( 1, hm, da )     ! [kg/m^3]
      call splint ( 2, hm, Ta )     ! [K]
      Pa = da * 0.1921 * Ta          ! [Pa]
   case default
      stop '*** FATAL ERROR: atmospheric model unkown'
   end select

 case(2) ! Mars

   select case(iatmos)     
   case(1) ! NASA exponential model
      call Mars_expo(hm_tmp,da,Pa,Ta)
   case(3) ! interpolation from tabulated values
      call splint ( 1, hm, da )     ! [kg/m^3]
      call splint ( 2, hm, Ta )     ! [K]
      Pa = da * 0.1921 * Ta          ! [Pa]
   case default
      stop '*** FATAL ERROR: atmospheric model unkown'
   end select


 case(3) ! Venus
   select case(iatmos)
   case(3) ! interpolation from tabulated values
      call splint ( 1, hm, da )     ! [kg/m^3]
      call splint ( 2, hm, Ta )     ! [K]
      Pa = da * 0.1921 * Ta          ! [Pa]
   case default
      stop '*** FATAL ERROR: atmospheric model unkown'
   end select


 end select

 ! test for NaN
 if (isnan(hm)) stop 'hm NaN'
 if (isnan(hm_tmp)) stop 'hm_tmp NaN'
 if (isnan(da)) stop ' da NaN'
 if (isnan(Pa)) stop ' Pa NaN'
 if (isnan(Ta)) stop ' Ta NaN'

 end subroutine atm_planet

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                       ~~!
 !~~                                                                       ~~!
 !~~                                                                       ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE OUTIL_ATM
