 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE OUTIL_AFM

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                 NUMERICAL INTEGRATION GLOBAL MANAGEMENT                ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE ARRAY
 USE PARAM_FILE
 USE MANAGE_FILE
 USE VARGLOBAL
 USE CONVERSION
 USE MATHS
 USE ALGEBRE_LINEAIRE
 USE OUTIL_INT
 USE INPUT

 CONTAINS 

 !----------------------------------------------------------------------------!
 !--           Listing of the subroutines available in this module          --!
 !----------------------------------------------------------------------------!
 !--                                                                        --!
 !--  AFM_control : mean program of the software                            --!
 !--                                                                        --!
 !----------------------------------------------------------------------------!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !#############################################################################!
 !##                                                                         ##!
 !##                 NUMERICAL INTEGRATION GLOBAL MANAGEMENT                 ##!
 !##                                                                         ##!
 !#############################################################################!
 
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !****************************************************************************!
 !** ROL :                                                                  **!
 !** INPUT : 
 !  idbody: body id (number) to consider
 !            see the ibodies keyword in the user/input file. Defatult is 1
 !  time : time [s]
 !  y100: array of initial condition of the body to consider.
 !        Depending on the physical model, it has 5 or 6 columns:
 !        5-columns: mass, velocity, height (altitude), zenith angle, length of path.
 !        6-columns: mass, velocity, height (altitude), zenith angle, length of path, Temperature.
 !        International system units are used.
 !        See the imodel keyword in the user/input file.
 !
 !**OUTPUT:
 !  ncon: number of consumed bodies
 !  npbm: number of abnormal end integrations
 !  nste: number of fragmentations
 !  nsrv: number of surviving bodies
 !  nint: number of saved integration steps for the number of bodies considered
 !  icpt: number of ablation step for the number of bodies considered
 !**                                                                        **!
 !****************************************************************************!

 subroutine AFMcontrol(idbody,time,y100,ncon,npbm,nste,nsrv,nint,icpt)

 !FON Declaration des variables locales
 implicit none
 !DVA Variables I-O
 integer(kind=SP), intent(   IN) :: idbody
 real(kind=DP),    intent(INOUT) :: time
 real(kind=DP), dimension(1:nbeqn), intent( IN) :: y100
 integer(kind=SP), intent(INOUT) :: nsrv,ncon,npbm,nste,nint
 integer(kind=DP), intent(  OUT) :: icpt
 !DVA Variables
 !* Matrix of state vectors of all residues
 real(kind=DP)    :: mm,rm,vm,hm,Om,lm,Xm,Ym,Tm
 real(kind=DP)    :: dmm,drm,dvm,dhm,dOm,dlm,dXm,dYm,dTm
! integer(kind=DP) :: nbfra
 real(kind=DP), dimension(:,:), allocatable ::  ytab 
 real(kind=DP), dimension(:,:), allocatable :: dytab 
 !* Matrix of fragments
 real(kind=DP), dimension(:,:), allocatable ::  yfra
 real(kind=DP), dimension(:,:), allocatable :: dyfra
 !* Fragmentation
 integer(kind=DP) :: nfra
 !* Numerical integration parametrization
 real(kind=DP) :: t,topt,tmp,deltaT,tref,tnow
 character(len=1) :: comm
 !* Iteration
 integer(kind=DP) :: id_fra,j,k,err
 !* Fragmentation conditions
 real(kind=DP) :: Ptsl, Patm
 !* Atmospheric profile
 real(kind=DP), dimension(3) :: atm_par
 !* provisoire
 real(kind=DP) :: mtot ! total mass at hstop
 !* logic
 logical :: savenow,keepgoing
 integer(kind=SP) :: debug
 
 !-------------------------------- BEGINNING! --------------------------------!
 debug=0
 mtot = 0._dp
 rmin = 0._dp
 rmax = 0._dp
 vmin = 0._dp
 vmax = 0._dp
 deltaT=0._dp
 tnow=0._dp
 tref=0._dp
 savenow=.false.
 print*,'AFMcontrol: idbody ',idbody
 print*,'AFMcontrol: y100 ',y100 ! m,v,h,o,l,X,Y[,T]
 print*,'AFMcontrol: nbeqn ',nbeqn
 icpt = 0_dp
 if (iresume .eq. 1) then
  call input_resume(tref,ytab,dytab,nbfra)
  !tref=ytab(0,nbfra)
  iresume=0 ! in order to avoid to read again the last step file the next time AFMcontrol is called by AFM_run
  print*,'AFMcontrol: input_resume ok. nbfra= ',nbfra,' tref=',tref
 else
  print*,'AFMcontrol: maxdim= ',maxdim
  nbfra = 1
  allocate( ytab(1:nbeqn,1:nbfra))
  allocate(dytab(1:nbeqn,1:nbfra))
  !* State vector of the main body 
  ytab(1:nbeqn,1) = y100(1:nbeqn)
 dytab(1:nbeqn,1) = y100(1:nbeqn) * 0.0_dp
 end if
 if (.not. allocated( yfra))   allocate( yfra(1:nbeqn,1:maxdim))
 if (.not. allocated(dyfra))   allocate(dyfra(1:nbeqn,1:maxdim))


 print*,'AFMcontrol: loop over nbfra'
 !* Take in account of ablation/fragmentation phenomenon
 do while (nbfra > 0)
   open(num%file(18),file=loc%file(18),iostat=err) ! opens latest state file
   !* Fragmentation
   if (fra%i .eq. 1) then
     do id_fra = nbfra,1,-1
       if ((ytab(1,id_fra)<mstop) .or. (ytab(2,id_fra)<vstop) .or. (ytab(3,id_fra)<hstop)) cycle
       ! estimate the fragmentation
       call AFM_fragmentation(ytab(1:nbeqn,id_fra),dytab(1:nbeqn,id_fra),nfra,yfra,dyfra,Ptsl,Patm)
       if (nfra .ge. 2) then ! i.e. fragmentation occurred
           nste = nste+1
           hm = ytab(3,id_fra) ! height [m]
           mm = ytab(1,id_fra) ! mass [kg]
           if (ibodies .eq. 1) then
               write(num%file(10),fmt%file(10))     id_fra,Patm,Ptsl,mm,hm/1000._dp,nfra ! ParentId, atm pressure, dyn pressure, number fragments
           else
               write(num%file(10),fmt%file(10)) nBODY%icur,Patm,Ptsl,mm,hm/1000._dp,nfra ! ParentId, atm pressure, dyn pressure, number fragments
           end if
           ! reorganize the data
           call tab_insert(id_fra,nfra, yfra,nbfra, ytab)
           call tab_insert(id_fra,nfra,dyfra,nbfra,dytab)
       end if ! end of if (nfra .ge. 2) then
     end do ! end of do id_fra = 1,nbfra,1
     ! update number of fragmentation
     if (size(ytab,2) .ne. nbfra) nbfra = size(ytab,2)
   end if  ! end of if (fra%i .eq. 1)
   !* End of fragmentation

   ! check if it is time to save
   tnow=time
   deltaT=tnow-tref
    if ((deltaT .ge. 1.0_dp/outfps) .or. (outfps .eq. 9999.0)) then
      tref=tnow
      savenow=.true.
   endif 
   
   
   
   !* Ablation
   do id_fra = nbfra,1,-1
      topt = tpas
      icpt=icpt+1
      !if (t_p==0) print*,'AFMcontrol: before INTcontrol: topt,ytab(1:nbeqn,id_fra),dytab(1:nbeqn,id_fra)=',topt,ytab(1:nbeqn,id_fra),dytab(1:nbeqn,id_fra)
      call INTcontrol(time,topt,ytab(1:nbeqn,id_fra),dytab(1:nbeqn,id_fra),atm_par)
      ! m,v,h,o,l,x,y,[T]
      mm = ytab(1,id_fra) ! [kg]
      rm = (mm/((4._dp/3._dp)*PI*dm))**(1._dp/3._dp) ! [m]
      vm = ytab(2,id_fra) ! [m/s]
      hm = ytab(3,id_fra) ! [m]
      Om = ytab(4,id_fra) ! [rad]
      lm = ytab(5,id_fra) ! [m]
      Xm = ytab(6,id_fra) ! [m]
      Ym = ytab(7,id_fra) ! [m]
      
      dmm = dytab(1,id_fra) ! dm/dt [kg/s]
      drm = (dmm/((4._dp/3._dp)*PI*dm))**(1._dp/3._dp) ! dr/dt [m/s]
      dvm = dytab(2,id_fra) ! dv/dt [m/s/s]
      dhm = dytab(3,id_fra) ! dh/dt [m/s]
      dOm = dytab(4,id_fra) ! do/dt [rad/s]
      dlm = dytab(5,id_fra) ! dl/dt [m/s]
      dXm = dytab(6,id_fra) ! dX/dt [m/s]
      dYm = dytab(7,id_fra) ! dY/dt [m/s]
      
      select case(nbeqn) ! set the Temperature depending on the number of equations
      case(7)
         Tm = 0._dp      ! [K]
        dTm = 0.0_dp     ! [K/s]
      case(8)
         Tm = ytab(8,id_fra)  ! [K]
        dTm = dytab(8,id_fra) ! [K/s]
      end select
      
      !*
      if (mm .le. mstop) then
        if (hm .ge. hstop) then  ! body is consumed in atmosphere
          !* Consumed
          ncon=ncon+1
          print*,'AFMcontrol: consumed body: ',id_fra,' / ',nbfra
          write(num%file(15),fmt%file(15)) id_fra,time,mm,rm,vm/1000._dp,hm/1000._dp,Om*RAD2DEG,lm/1000._dp,Xm,Ym,Tm,dmm,drm,dvm/1000._dp,dhm/1000._dp,dOm*RAD2DEG,dlm/1000._dp,dXm,dYm,dTm
          call tab_remove(id_fra,nbfra, ytab,debug) ! remove particle # id_fra
          call tab_remove(id_fra,nbfra,dytab,debug)
          nbfra = nbfra - 1
          cycle ! go to the next iteration i
        end if
      else
        if (hm .le. hstop) then   ! body is surviving the atmosphere
          !* Surviving      
          nsrv=nsrv+1
          mtot=mtot+mm
          print'("AFMcontrol: surviving body: Parent=",i6," Frag=",i6," m=",e9.2," kg, h=",f4.1," km, v= ",e9.2," km/s, Xm=",e9.2," km, Ym=",e9.2," mtot=",e9.2)',idbody,id_fra,mm,hstop/1000.0_dp,vm/1000.0_dp,Xm,Ym,mtot
          !* min(s)         
          if (rm .lt. rmin) rmin = rm
          if (vm .lt. vmin) vmin = vm
          !* max(s)     
          if (rm .gt. rmax) rmax = rm
          if (vm .gt. vmax) vmax = vm
          write(num%file(14),fmt%file(14)) id_fra,time,mm,rm,vm/1000._dp,hm/1000._dp,Om*RAD2DEG,lm/1000._dp,Xm,Ym,Tm,dmm,drm,dvm/1000._dp,dhm/1000._dp,dOm*RAD2DEG,dlm/1000._dp,dXm,dYm,dTm
          call tab_remove(id_fra,nbfra, ytab,debug) ! remove particle # id_fra
          call tab_remove(id_fra,nbfra,dytab,debug)
          nbfra = nbfra - 1
          cycle ! go to the next iteration i
        end if
      end if     
      !* ---------------

      comm='R' ! default
      !* Abnormal integration cut off
      if ((hm .gt. hmax) .or. (abs(topt) .lt. tstop)) then
        if (hm .gt. hmax) nrbnd=nrbnd+1
        if (hm .gt. hmax) comm='T'
        npbm = npbm+1
        print*,'AFMcontrol: *** ERROR: with particle ',id_fra
        write(num%file(16),fmt%file(16)) id_fra,topt,mm,rm,vm/1000._dp,hm/1000._dp,Om*RAD2DEG,lm/1000._dp,Xm,Ym,Tm,dmm,drm,dvm/1000._dp,dhm/1000._dp,dOm*RAD2DEG,dlm/1000._dp,dXm,dYm,dTm,comm
        call tab_remove(id_fra,nbfra, ytab,debug) ! remove particle # id_fra
        call tab_remove(id_fra,nbfra,dytab,debug)
        nbfra = nbfra - 1
        cycle !* go to the next iteration i
      end if
      
      ! some savings
       !* Steps of numerical integration in STEPS_int.dat and latest state in STEPS_lst.dat
      if ((savenow .eq. .true.) .and. (nbfra > 0)) then
        if ((id_fra .eq. 1) .and. (debug==1)) print'("AFMcontrol: ==== SAUVEGARDE: iBody=",i8," t=",e11.3," m=",e9.2," kg, H=",f8.1," V=",f8.1," X=",f6.2,"  nobj=",i8)',nBODY%icur,tnow,ytab(1,1),ytab(3,1),ytab(2,1),Xm/1000._dp,nbfra
        ! save in STEPS_lst.dat the state of the body: useful for resumed integrations
        write(num%file(18),fmt%file(18)) id_fra,time,mm,rm,vm/1000._dp,hm/1000._dp,Om*RAD2DEG,lm/1000._dp,Xm,Ym,Tm,dmm,drm,dvm/1000._dp,dhm/1000._dp,dOm*RAD2DEG,dlm/1000._dp,dXm,dYm,dTm
        nint = nint+1
        ! save the integration step in STEPS_int.dat
        if (ibodies .eq. 1)  write(num%file(11),fmt%file(11)) id_fra,time,mm,rm,vm/1000._dp,hm/1000._dp,Om*RAD2DEG,lm/1000._dp,Xm,Ym,Tm,dmm,drm,dvm/1000._dp,dhm/1000._dp,dOm*RAD2DEG,dlm/1000._dp,dXm,dYm,dTm
      end if ! end of if (savenow .eq. .true.)
   
   end do ! end of do i = nbfra,1,-1
   savenow=.false.
   !* End of ablation 
   
   close(num%file(18)) ! close latest state file
 
 end do ! end do while (nbfra .ne. 0) 

 deallocate(yfra)
 deallocate(dyfra)
 deallocate(ytab)
 deallocate(dytab)

 end subroutine AFMcontrol

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE OUTIL_AFM
