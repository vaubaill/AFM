!+
MODULE ATM_US76
! ---------------------------------------------------------------------------
! PURPOSE - Compute properties of the U.S. Standard Atmosphere 1976
! AUTHORS - Steven S. Pietrobon.
!           Ralph L. Carmichael, Public Domain Aeronautical Software
!
!     REVISION HISTORY
!   DATE  VERS PERSON  STATEMENT OF CHANGES
! 28Feb95  0.1   RLC   Assembled several old codes
!  1Aug00  0.2   RLC   Copied from old Tables76
! 23Aug00  0.3   RLC   Added NitrogenNumber using QUANC8
! 24Aug00  0.4   RLC   Added KineticTemperatureGradient
! 30Aug00  0.5   RLC   Corrected numerous errors
! 30Dec00  0.6   RLC   Adapted UpperAtmosphere from Pietrobon's Unofficial
!                        Australian Standard Atmosphere
!----------------------------------------------------------------------------
 USE PRECISION
 implicit none

  character(LEN=*),PUBLIC,parameter:: ATM76_VERSION = "0.6 (12 Sept 2000)"
  real(kind=DP),private,parameter:: PI = 3.14159265_dp
  real(kind=DP),parameter:: REARTH = 6356.766_dp               ! radius of the Earth (km)
  real(kind=DP),parameter:: GMR = 34.163195_dp                             ! gas constant
  real(kind=DP),parameter:: GZERO = 9.80665_dp !  accel. of gravity, m/sec^2

  real(kind=DP),parameter:: TZERO = 288.15_dp                ! temperature at sealevel, K
  real(kind=DP),parameter:: PZERO = 101325.0_dp            ! pressure at sealevel, N/sq.m
  real(kind=DP),parameter:: RHOZERO = 1.2250_dp            ! density at sealevel, kg/cu.m
  real(kind=DP),parameter:: RSTAR = 8314.32_dp       ! perfect gas constant, N-m/(kmol-K)
  real(kind=DP),parameter:: ASOUNDZERO = 340.294_dp   ! speed of sound at sealevel, m/sec

  real(kind=DP),parameter:: BETAVISC = 1.458E-6_dp    ! viscosity term, N s/(sq.m sqrt(K)
  real(kind=DP),parameter:: SUTH = 110.4_dp                    ! Sutherland's constant, K

  real(kind=DP),parameter:: MZERO      = 28.9644_dp ! molecular weight of air at sealevel

! TABLE 5 - DEFINITION OF KINETIC TEMPERATURE FROM 86 km to 1000 km
  real(kind=DP),parameter:: Z7 =  86.0_dp,  T7=186.8673_dp
  real(kind=DP),parameter:: z8 =  91.0_dp,  T8=T7
  real(kind=DP),parameter:: Z9 = 110.0_dp,  T9=240.0_dp
  real(kind=DP),parameter:: Z10= 120.0_dp, T10=360.0_dp
  real(kind=DP),parameter:: Z12=1000.0_dp, T12=1000.0_dp


  real(kind=DP),parameter:: FT2METERS = 0.3048_dp               ! mult. ft. to get meters (exact)
  real(kind=DP),parameter:: KELVIN2RANKINE = 1.8_dp             ! mult deg K to get deg R
  real(kind=DP),parameter:: PSF2NSM = 47.880258_dp          ! mult lb/sq.ft to get N/sq.m
  real(kind=DP),parameter:: SCF2KCM = 515.379_dp         ! mult slug/cu.ft to get kg/cu.m

CONTAINS

!+
FUNCTION EvaluateCubic(a,fa,fpa, b,fb,fpb, u) RESULT(fu)
! ---------------------------------------------------------------------------
! PURPOSE - Evaluate a cubic polynomial defined by the function and the
!   1st derivative at two points
  real(kind=DP),INTENT(IN):: u   ! point where function is to be evaluated
  real(kind=DP),INTENT(IN):: a,fa,fpa   ! a, f(a), f'(a)  at first point
  real(kind=DP),INTENT(IN):: b,fb,fpb   ! b, f(b), f'(b)  at second point
  real(kind=DP):: fu                    ! computed value of f(u)

  real(kind=DP):: d,t,p
!----------------------------------------------------------------------------
  d=(fb-fa)/(b-a)
  t=(u-a)/(b-a)
  p=1.0_dp-t

  fu = p*fa + t*fb - p*t*(b-a)*(p*(d-fpa)-t*(d-fpb))
  RETURN
END Function EvaluateCubic   ! ----------------------------------------------

!+
FUNCTION KineticTemperature(z) RESULT(t)
!   -------------------------------------------------------------------------
! PURPOSE - Compute kinetic temperature above 86 km.

  real(kind=DP),INTENT(IN)::  z     ! geometric altitude, km.                        
  real(kind=DP):: t     ! kinetic temperature, K

  real(kind=DP),parameter:: C1 = -76.3232_dp  ! uppercase A in document
  real(kind=DP),parameter:: C2 = 19.9429_dp   ! lowercase a in document
  real(kind=DP),parameter:: C3 = 12.0_dp
  real(kind=DP),parameter:: C4 = 0.01875_dp   ! lambda in document
  real(kind=DP),parameter:: TC = 263.1905_dp

  real(kind=DP):: xx,yy
!----------------------------------------------------------------------------
  IF (z <= Z8) THEN
    t=T7
  ELSE IF (z < Z9) THEN  
    xx=(z-Z8)/C2                        ! from Appendix B, p.223
    yy=SQRT(1.0_dp-xx*xx)
    t=TC+C1*yy
  ELSE IF (z <= Z10) THEN
    t=T9+C3*(z-Z9)
  ELSE
    xx=(REARTH+Z10)/(REARTH+z)
    yy=(T12-T10)*EXP(-C4*(z-Z10)*xx)
    t=T12-yy
  END IF

  RETURN
END Function KineticTemperature   ! -----------------------------------------


!+
SUBROUTINE UpperAtmosphere(alt, sigma, delta, theta)
!   -------------------------------------------------------------------------
! PURPOSE - Compute the properties of the 1976 standard atmosphere from
!   86 km. to 1000 km.

  IMPLICIT NONE
!============================================================================
!     A R G U M E N T S                                                     |
!============================================================================
  real(kind=DP),INTENT(IN)::  alt    ! geometric altitude, km.                        
  real(kind=DP),INTENT(OUT):: sigma  ! density/sea-level standard density              
  real(kind=DP),INTENT(OUT):: delta  ! pressure/sea-level standard pressure           
  real(kind=DP),INTENT(OUT):: theta  ! temperature/sea-level standard temperature
!============================================================================
!     L O C A L   C O N S T A N T S                                         |
!============================================================================

!altitude table (km)
  real(kind=DP),parameter,DIMENSION(23):: Z = (/ &
     86._dp,  93._dp, 100._dp, 107._dp, 114._dp, &
    121._dp, 128._dp, 135._dp, 142._dp, 150._dp, &
    160._dp, 170._dp, 180._dp, 190._dp, 200._dp, &
    250._dp, 300._dp, 400._dp, &
    500._dp, 600._dp, 700._dp, 800._dp, 1000._dp /)

!fit2000
  real(kind=DP),parameter,DIMENSION(SIZE(Z)):: LOGP = (/                     &
  -0.985159_dp,  -2.225531_dp,  -3.441676_dp,  -4.532756_dp,  -5.415458_dp,  &
  -6.057519_dp,  -6.558296_dp,  -6.974194_dp,  -7.333980_dp,  -7.696929_dp,  &
  -8.098581_dp,  -8.458359_dp,  -8.786839_dp,  -9.091047_dp,  -9.375888_dp,  &
 -10.605998_dp, -11.644128_dp, -13.442706_dp, -15.011647_dp, -16.314962_dp,  &
 -17.260408_dp, -17.887938_dp, -18.706524_dp /)

 real(kind=DP),parameter,DIMENSION(SIZE(Z)):: DLOGPDZ = (/                   &
 -11.875633_dp, -13.122514_dp, -14.394597_dp, -15.621816_dp, -16.816216_dp,  &
 -17.739201_dp, -18.449358_dp, -19.024864_dp, -19.511921_dp, -19.992968_dp,  &
 -20.513653_dp, -20.969742_dp, -21.378269_dp, -21.750265_dp, -22.093332_dp,  &
 -23.524549_dp, -24.678196_dp, -26.600296_dp, -28.281895_dp, -29.805302_dp,  &
 -31.114578_dp, -32.108589_dp, -33.268623_dp /)

  real(kind=DP),parameter,DIMENSION(SIZE(Z)):: LOGRHO = (/                   &
  -0.177700_dp,  -0.176950_dp,  -0.167294_dp,  -0.142686_dp,  -0.107868_dp,  &
  -0.079313_dp,  -0.064668_dp,  -0.054876_dp,  -0.048264_dp,  -0.042767_dp,  &
  -0.037847_dp,  -0.034273_dp,  -0.031539_dp,  -0.029378_dp,  -0.027663_dp,  &
  -0.022218_dp,  -0.019561_dp,  -0.016734_dp,  -0.014530_dp,  -0.011315_dp,  &
  -0.007673_dp,  -0.005181_dp,  -0.003500_dp /)

  real(kind=DP),parameter,DIMENSION(SIZE(Z)):: DLOGRHODZ = (/                & 
  -0.177900_dp,  -0.180782_dp,  -0.178528_dp,  -0.176236_dp,  -0.154366_dp,  &
  -0.113750_dp,  -0.090551_dp,  -0.075044_dp,  -0.064657_dp,  -0.056087_dp,  &
  -0.048485_dp,  -0.043005_dp,  -0.038879_dp,  -0.035637_dp,  -0.033094_dp,  &
  -0.025162_dp,  -0.021349_dp,  -0.017682_dp,  -0.016035_dp,  -0.014330_dp,  &
  -0.011626_dp,  -0.008265_dp,  -0.004200_dp /)


!============================================================================
!     L O C A L   V A R I A B L E S                                         |
!============================================================================
  integer(kind=SP) :: i,j,k                                        ! counters
  real(kind=DP):: p,rho
!----------------------------------------------------------------------------

  IF (alt >= Z(SIZE(Z))) THEN          ! trap altitudes greater than 1000 km.
    delta=1E-20_dp
    sigma=1E-21_dp
    theta=1000.0_dp/TZERO
    RETURN
  END IF

  i=1 
  j=SIZE(Z)                                    ! setting up for binary search
  DO
    k=(i+j)/2                                              ! integer division
    IF (alt < Z(k)) THEN
      j=k
    ELSE
      i=k
    END IF   
    IF (j <= i+1) EXIT
  END DO

  p=EXP(EvaluateCubic(Z(i),LOGP(i),DLOGPDZ(i), &
                      Z(i+1),LOGP(i+1),DLOGPDZ(i+1), alt))
  delta=p/PZERO

  rho=EXP(EvaluateCubic(Z(i),LOGRHO(i),DLOGRHODZ(i), &
                      Z(i+1),LOGRHO(i+1),DLOGRHODZ(i+1), alt))
  sigma=rho/RHOZERO

  theta=KineticTemperature(alt)/TZERO
  RETURN
END Subroutine UpperAtmosphere   ! ------------------------------------------

!+
SUBROUTINE LowerAtmosphere(alt, sigma, delta, theta)
!   -------------------------------------------------------------------------
! PURPOSE - Compute the properties of the 1976 standard atmosphere to 86 km.

  IMPLICIT NONE
!============================================================================
!     A R G U M E N T S                                                     |
!============================================================================
  real(kind=DP),INTENT(IN)::  alt    ! geometric altitude, km.                        
  real(kind=DP),INTENT(OUT):: sigma  ! density/sea-level standard density              
  real(kind=DP),INTENT(OUT):: delta  ! pressure/sea-level standard pressure           
  real(kind=DP),INTENT(OUT):: theta  ! temperature/sea-level standard temperature
!============================================================================
!     L O C A L   C O N S T A N T S                                         |
!============================================================================
  real(kind=DP),parameter:: REARTH = 6369.0_dp     ! radius of the Earth (km)
  real(kind=DP),parameter:: GMR = 34.163195_dp                 ! gas constant
  integer(kind=SP),parameter:: NTAB=8  ! number of entries in the defining tables
!============================================================================
!     L O C A L   V A R I A B L E S                                         |
!============================================================================
  integer(kind=SP):: i,j,k                                         ! counters
  real(kind=DP):: h                              ! geopotential altitude (km)
  real(kind=DP):: tgrad, tbase ! temperature gradient and base temp of this layer
  real(kind=DP):: tlocal                                  ! local temperature
  real(kind=DP):: deltah                    ! height above base of this layer
!============================================================================
!     L O C A L   A R R A Y S   ( 1 9 7 6   S T D.  A T M O S P H E R E )   |
!============================================================================
  real(kind=DP),DIMENSION(NTAB),parameter:: htab= &
  (/0.0_dp, 11.0_dp, 20.0_dp, 32.0_dp, 47.0_dp, 51.0_dp, 71.0_dp, 84.852_dp/)
  real(kind=DP),DIMENSION(NTAB),parameter:: ttab= &
(/288.15_dp, 216.65_dp, 216.65_dp, 228.65_dp, 270.65_dp, 270.65_dp, 214.65_dp, 186.946_dp/)
  real(kind=DP),DIMENSION(NTAB),parameter:: ptab= &
 (/1.0_dp, 2.233611E-1_dp, 5.403295E-2_dp, 8.5666784E-3_dp, 1.0945601E-3_dp, &
   6.6063531E-4_dp, 3.9046834E-5_dp, 3.68501E-6_dp/)
  real(kind=DP),DIMENSION(NTAB),parameter:: gtab= &
 (/-6.5_dp, 0.0_dp, 1.0_dp, 2.8_dp, 0.0_dp, -2.8_dp, -2.0_dp, 0.0_dp/)
!----------------------------------------------------------------------------
  h=alt*REARTH/(alt+REARTH)      ! convert geometric to geopotential altitude

  i=1 
  j=NTAB                                       ! setting up for binary search
  DO
    k=(i+j)/2                                              ! integer division
    IF (h < htab(k)) THEN
      j=k
    ELSE
      i=k
    END IF   
    IF (j <= i+1) EXIT
  END DO

  tgrad=gtab(i)                                     ! i will be in 1...NTAB-1
  tbase=ttab(i)
  deltah=h-htab(i)
  tlocal=tbase+tgrad*deltah
  theta=tlocal/ttab(1)                                    ! temperature ratio

  IF (tgrad == 0.0_dp) THEN                                     ! pressure ratio
    delta=ptab(i)*EXP(-GMR*deltah/tbase)
  ELSE
    delta=ptab(i)*(tbase/tlocal)**(GMR/tgrad)
  END IF

  sigma=delta/theta                                           ! density ratio
  RETURN
END Subroutine LowerAtmosphere   ! ------------------------------------------

!+
SUBROUTINE SimpleAtmosphere(alt,sigma,delta,theta)
!   -------------------------------------------------------------------------
! PURPOSE - Compute the characteristics of the atmosphere below 20 km.

! NOTES-Correct to 20 km. Only approximate above there

  IMPLICIT NONE
!============================================================================
!     A R G U M E N T S                                                     |
!============================================================================
  real(kind=DP),INTENT(IN)::  alt    ! geometric altitude, km.
  real(kind=DP),INTENT(OUT):: sigma  ! density/sea-level standard density             
  real(kind=DP),INTENT(OUT):: delta  ! pressure/sea-level standard pressure            
  real(kind=DP),INTENT(OUT):: theta  ! temperature/sea-level standard temperature   
!============================================================================
!     L O C A L   C O N S T A N T S                                         |
!============================================================================
  real(kind=DP),parameter:: REARTH = 6369.0_dp                ! radius of the Earth (km)
  real(kind=DP),parameter:: GMR = 34.163195_dp                            ! gas constant
!============================================================================
!     L O C A L   V A R I A B L E S                                         |
!============================================================================
  real(kind=DP):: h   ! geopotential altitude
!----------------------------------------------------------------------------
  h=alt*REARTH/(alt+REARTH)      ! convert geometric to geopotential altitude

  IF (h < 11.0_dp) THEN
    theta=1.0_dp+(-6.5_dp/288.15_dp)*h                                   ! Troposphere
    delta=theta**(GMR/6.5_dp)
  ELSE
    theta=216.65_dp/288.15_dp                                        ! Stratosphere
    delta=0.2233611_dp*EXP(-GMR*(h-11.0_dp)/216.65_dp)
  END IF

  sigma=delta/theta
  RETURN
END Subroutine SimpleAtmosphere   ! -----------------------------------------

!+
FUNCTION Viscosity(theta) RESULT(visc)
!   -------------------------------------------------------------------------
! PURPOSE - Compute viscosity using Sutherland's formula.
!        Returns viscosity in kg/(meter-sec)

  IMPLICIT NONE
  real(kind=DP),INTENT(IN) :: theta                ! temperature/sea-level temperature  
  real(kind=DP):: visc
  real(kind=DP):: temp                              ! temperature in deg Kelvin
!----------------------------------------------------------------------------
  temp=TZERO*theta
  visc=BETAVISC*Sqrt(temp*temp*temp)/(temp+SUTH)
  RETURN
END Function Viscosity   ! --------------------------------------------

!+
SUBROUTINE Earth_us76(alt,sigma,delta,theta)
!   -------------------------------------------------------------------------
! PURPOSE - Compute the characteristics of the U.S. Standard Atmosphere 1976

  IMPLICIT NONE
  real(kind=DP),INTENT(IN)::  alt    ! geometric altitude, km.
  real(kind=DP),INTENT(OUT):: sigma  ! density/sea-level standard density             
  real(kind=DP),INTENT(OUT):: delta  ! pressure/sea-level standard pressure            
  real(kind=DP),INTENT(OUT):: theta  ! temperature/sea-level standard temperature   
!============================================================================
  IF (alt > 86.0_dp) THEN
    CALL UpperAtmosphere(alt,sigma,delta,theta)
  ELSE
    CALL LowerAtmosphere(alt,sigma,delta,theta)
  END IF
  RETURN
END Subroutine Earth_us76   ! -----------------------------------------------

END MODULE ATM_US76  ! ===============================================

