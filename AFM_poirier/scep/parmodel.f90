MODULE PARMODEL

 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

 USE PRECISION
 implicit none
 
 !- Tensile strength (Ferrier Thesis,p190, 2012)
 !- (1) exponent power
 !- (2) effective strength of tested specimen (N/m2)
 !- (3) effective mass     of tested specimen (  kg)
 real(kind=DP), dimension(1:3), parameter :: tsl_strFerrier2012 = (/ 0.25_dp, 1.E+08_dp, 2.7E+10_dp /) 
 !- Tensile strength from Popova et a. (2015), M&PS, 49, 10, 1525-1550
 !- (1) exponent power
 !- (2) effective strength of tested specimen (N/m2)
 !- (3) effective mass     of tested specimen (  kg)
 real(kind=DP), dimension(1:3), parameter :: tsl_strPopova2011 = (/ 0.25_dp, 30.0E+06_dp, 1.0E-02_dp /) 
 
 
 !- Radius distriubtion of fragments
 !- (1) mean
 !- (2) ecart-type
 real(kind=DP), parameter :: dis_fra = 10._dp

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE PARMODEL
