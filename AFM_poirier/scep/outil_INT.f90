 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE OUTIL_INT

 USE PRECISION
! USE RKF
 USE BS

 CONTAINS

 subroutine INTcontrol(t,topt,y,dy,atm)

 !FON Declaration of local variables
 implicit none
 !DVA Variables I-O
 real(kind=DP), intent(INOUT) :: t,topt ! time, optimized_time_step
 real(kind=DP), dimension(1:nbeqn,1), intent(INOUT) ::  y ! m,v,h,o,l,Y[,T]
 real(kind=DP), dimension(1:nbeqn,1), intent(INOUT) :: dy ! dm/dt,dv/dt,dh/dt,do/dt,dl/dt,dY/dt[,dT/dt]
 real(kind=DP), dimension(1:3), intent(  OUT) :: atm
 !DVA Variables
 real(kind=DP), dimension(1:nbeqn) ::  ytmp
 real(kind=DP), dimension(1:nbeqn) :: dytmp
 
 integer :: k

 !-------------------------------- BEGINNING! --------------------------------!

  ytmp(1:nbeqn) =  y(1:nbeqn,1)
 dytmp(1:nbeqn) = dy(1:nbeqn,1)

 select case(iintegrator)
 case(1) ! Runge-Kutta-Fehlberg 4(5)
   stop '*** FATAL ERROR: DO NOT USE RKF INTEGRATOR '
   !call RKFcontrol(topt,ytmp,atm)
 case(2) ! Bulirsh-Stoer
   call  BScontrol(t,topt,ytmp,dytmp,atm)
 case default
   print*,'*** FATAL ERROR: integrator id ',iintegrator,' is unknown (1 or 2 is possible only)'
 end select

  y(1:nbeqn,1) =  ytmp(1:nbeqn)
 dy(1:nbeqn,1) = dytmp(1:nbeqn)

 end subroutine INTcontrol

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

END MODULE OUTIL_INT
