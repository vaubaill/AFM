SUBROUTINE spline_jma(xint,yint,yp1,ypn,y2)

  USE nrtype
  USE nrutil, ONLY : assert_eq
  USE nr, ONLY : tridag
  USE VARGLOBAL, only : data_points

  IMPLICIT NONE

  INTEGER(I4B) :: n

  REAL(SP), DIMENSION(DATA_POINTS), INTENT(IN) :: xint,yint
  REAL(SP), INTENT(IN) :: yp1,ypn
  REAL(SP), DIMENSION(DATA_POINTS), INTENT(OUT) :: y2
  REAL(SP), DIMENSION(SIZE(XINT)) :: a,b,c,r

 ! IF (diag_ctrl >= 1 ) THEN
 !    WRITE(30,*) 'SPLINE.F90 entered.'
 !    WRITE(30,*) ' '
 ! ENDIF

  n=assert_eq(SIZE(xint),SIZE(yint),SIZE(y2),'spline')

  c(1:n-1)=xint(2:n)-xint(1:n-1)
  r(1:n-1)=6.00*((yint(2:n)-yint(1:n-1))/c(1:n-1))
  r(2:n-1)=r(2:n-1)-r(1:n-2)
  a(2:n-1)=c(1:n-2)
  b(2:n-1)=2.00*(c(2:n-1)+a(2:n-1))
  b(1)=1.0
  b(n)=1.0

  IF (yp1 > 0.99e30) THEN
     r(1)=0.0
     c(1)=0.0
  ELSE
     r(1)=(3.00/(xint(2)-xint(1)))*((yint(2)-yint(1))/(xint(2)-xint(1))-yp1)
     c(1)=0.5
  END IF

  IF (ypn > 0.99e30) THEN
     r(n)=0.0
     a(n)=0.0
  ELSE
     r(n)=(-3.00/(xint(n)-xint(n-1)))*((yint(n)-yint(n-1))/(xint(n)-xint(n-1))-ypn)
     a(n)=0.5
  END IF

  CALL tridag(a(2:n),b(1:n),c(1:n-1),r(1:n),y2(1:n))

 ! IF (diag_ctrl >= 1 ) THEN
 !    WRITE(30,*) 'SPLINE.F90 exited.'
 !    WRITE(30,*) ' '
 ! ENDIF

END SUBROUTINE spline_jma
