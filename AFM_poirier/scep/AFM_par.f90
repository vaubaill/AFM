 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!
 !@@ AUT : Romain DECOSTA                                                   @@!
 !@@ VER : 09/09/2014                                                       @@!
 !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!

MODULE AFM_PAR

 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 !~~                                                                        ~~!
 !~~                  MANAGEMENT OF AFM MODEL'S PARAMETERS                  ~~!
 !~~                                                                        ~~!
 !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

 USE PRECISION
 USE PARAM_FILE
 USE VARGLOBAL

 CONTAINS

 !----------------------------------------------------------------------------!
 !--             Listing of subroutines available in this module            --!
 !----------------------------------------------------------------------------!
 !--                                                                        --!
 !--  read_mod  : reading  of A.F.M. model's parameters                     --!
 !--  check_mod : checking of A.F.M. model's parameters                     --!
 !--                                                                        --!
 !----------------------------------------------------------------------------! 

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 !############################################################################!
 !##                                                                        ##!
 !##                   READING OF A.F.M MODEL'S PARAMETERS                  ##!
 !##                                                                        ##!
 !############################################################################!

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 subroutine read_mod()

 !FON Declaration des variables locales
 implicit none
 !DVA Variables 
 !* Table of parameters
 real(kind=DP), dimension(1:12,1:3) :: material 
 !* File management
 integer(kind=SP)  :: ios
 character(len=90) :: commen
 !* Iteration
 integer(kind=SP)  :: i

 !-------------------------------- BEGINNING! --------------------------------!

 !* Opening file
 open(num%file(1),file=loc%file(1),iostat=ios)
 if (ios .ne. 0) then 
   write(*,'(A21,1x,A40)') 'Error in opening file',loc%file(1)
   stop 
 end if

 do i = 1,3
   read(num%file(1),'(A82)') commen(1:82)
 end do

 !* Reading of the parameters
 do i = 1,12
   read(num%file(1),fmt%file(1)) commen(1:35),material(i,1),material(i,2),material(i,3)
 end do

 close(num%file(1))
 !* Closing file

 !* Variables assignation
 am    = material( 1,imaterial) ! albedo
 cm    = material( 2,imaterial) ! specific heat
 dm    = material( 3,imaterial) ! meteoroid density
 em    = material( 4,imaterial) ! emissivity  
 Tmelt = material( 5,imaterial) ! Tmelt
 Linf  = material( 6,imaterial) ! latent heat (< Tmelt)
 Lsup  = material( 7,imaterial) ! latent heat (> Tmelt)
 mu    = material( 8,imaterial) ! mass unit
 Tc    = material( 9,imaterial) ! thermal conductivity
 A     = material(10,imaterial) ! A coefficient for Pvap 
 B     = material(11,imaterial) ! B coefficient for Pvap 
 psi   = material(12,imaterial) ! condensation coefficient

 end subroutine read_mod

END MODULE AFM_PAR
